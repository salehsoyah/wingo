import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/authController.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/signinScreens/forgotPassword.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:flutter_app/views/signinScreens/otp_received.dart';
import 'package:flutter_app/views/signinScreens/verifyForgotPassword.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class PasswordRecovery extends StatefulWidget {
  @override
  _PasswordRecoveryState createState() => _PasswordRecoveryState();
}

class _PasswordRecoveryState extends State<PasswordRecovery> {
  final _emailController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool loading = false;
  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: SafeArea(
            child: loading == true ? Container(
              height: height,
              child: SpinKitFadingCircle(
                itemBuilder: (BuildContext context, int index) {
                  return DecoratedBox(
                    decoration: BoxDecoration(
                      color: index.isEven ? Colors.yellow : Colors.blue,
                    ),
                  );
                },
              ),
            ) : Container(
              width: double.infinity,
              color: Color.fromRGBO(234, 239, 255, 3),
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Container(
                                margin: EdgeInsets.only(
                                    top: height / 15, left: width / 20),
                                child: Icon(
                                  Icons.close,
                                  size: height / 35,
                                )),
                          ),
                          Spacer()
                        ],
                      ),
                      SizedBox(
                        height: height / 8,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(40),
                              topRight: Radius.circular(40),
                            )),
                        child: Column(
                          children: [
                            SizedBox(height: height / 10),
                            Container(
                              child: Text(
                                'Recovery Password',
                                style: TextStyle(
                                  fontFamily: 'DMSans-Bold',
                                  fontSize: height * 0.035,
                                  color: const Color(0xff172b4d),
                                  letterSpacing: -0.3999999847412109,
                                  height: 1.3333333333333333,
                                ),
                                textHeightBehavior: TextHeightBehavior(
                                    applyHeightToFirstAscent: false),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            SizedBox(
                              height: height / 100,
                            ),
                            Container(
                              child: Text(
                                'Enter your email to recover your password',
                                style: TextStyle(
                                  fontFamily: 'DMSans-Regular',
                                  fontSize: height * 0.023,
                                  color: const Color(0xff7a869a),
                                  letterSpacing: -0.3999999961853027,
                                  height: 1.7142857142857142,
                                ),
                                textHeightBehavior: TextHeightBehavior(
                                    applyHeightToFirstAscent: false),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            SizedBox(
                              height: height / 14,
                            ),
                            Padding(
                              padding: EdgeInsets.all(width / 20),
                              child: Container(
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  color: Color.fromRGBO(244, 245, 247, 3),
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                child: Padding(
                                  padding: EdgeInsets.all(width / 20),
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          Text(
                                            "Email",
                                            style: TextStyle(
                                                color: Color.fromRGBO(
                                                    193, 199, 208, 3),
                                                fontSize: height / 45),
                                          ),
                                        ],
                                      ),
                                      TextFormField(
                                        controller: _emailController,

                                        validator: (value) {
                                          if (value == null || value.isEmpty) {
                                            return 'Please enter email';
                                          }
                                          return null;
                                        },
                                        autofocus: false,
                                        style: TextStyle(
                                            fontSize: height / 45,
                                            color: Colors.black),
                                        decoration: InputDecoration(
                                          hintText: 'exemple@exemple.com',
                                          filled: true,
                                          fillColor:
                                          Color.fromRGBO(244, 245, 247, 3),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                            BorderRadius.circular(15.0),
                                          ),
                                          enabledBorder: UnderlineInputBorder(
                                            borderRadius:
                                            BorderRadius.circular(15.0),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              height: height / 15,
                              width: width / 1.25,
                              margin: EdgeInsets.only(top: height / 7),
                              child: InkWell(
                                onTap: () {},
                                // ignore: deprecated_member_use
                                child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  onPressed: () {
                                    if (_formKey.currentState.validate()) {
                                      setState(() {
                                        loading = true ;
                                      });
                                      forgotPasswordMail(_emailController.text)
                                          .then((result) {
                                        setState(() {
                                          loading = false ;
                                        });
                                        print(result);
                                        if (result['status']['code'] == 402) {
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return DialogScreen(
                                                  color: Colors.red,
                                                  icon: Icons.error_outline,
                                                  title: 'Error',
                                                  message: result['message'],
                                                  message2: '',
                                                  buttonText: 'Close',
                                                );
                                              });
                                        } else if (result['status']['code'] ==
                                            403) {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    VerifyForgotPassword(
                                                      email:
                                                      _emailController.text,
                                                    )),
                                          );
                                        } else if (result['status']['code'] ==
                                            404) {
                                          if ((result['message'])
                                              .containsKey('email')) {
                                            showDialog(
                                                context: context,
                                                builder: (BuildContext context) {
                                                  return DialogScreen(
                                                    color: Colors.red,
                                                    icon: Icons.error_outline,
                                                    title: 'Error',
                                                    message: result['message']
                                                    ['email'][0],
                                                    message2: '',
                                                    buttonText: 'Close',
                                                  );
                                                });
                                          }
                                        }
                                      });
                                    }

                                  },
                                  color: Colors.amber,
                                  textColor: Colors.white,
                                  child: Text("Send code".toUpperCase(),
                                      style: TextStyle(
                                          fontSize: 14, color: Colors.white)),
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Positioned(
                    top: height / 6.3,
                    child: Container(
                      width: height / 7.5,
                      child: Image(
                        image: AssetImage("assets/images/profilepic.png"),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
