import 'dart:convert';
import 'dart:io';
import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/authController.dart';
import 'package:flutter_app/controllers/userController.dart';
import 'package:flutter_app/models/device.dart';
import 'package:flutter_app/views/dashboard/dashboard.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/dialog/privacy.dart';
import 'package:flutter_app/views/dialog/terms.dart';
import 'package:flutter_app/views/dialog/updateLogin.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:flutter_app/views/signinScreens/otp_received.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CreateAccount extends StatefulWidget {
  @override
  _CreateAccountState createState() => _CreateAccountState();
}

class _CreateAccountState extends State<CreateAccount> {
  final _firstNameController = TextEditingController();
  final _phoneController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  bool checkboxValue = false;
  String fcm_token;
  final _firebaseMessaging = FirebaseMessaging.instance;
  final _formKey = GlobalKey<FormState>();
  bool loading = false;
  void initState() {
    _firebaseMessaging
        .getToken(
            vapidKey:
                'AAAAoHLdbe8:APA91bEHKX9Cu8zZYksRfW5D3OnA_sWwrSpLeVW4nTkS0qQuosDumKjHyjBPUxjpyGVlSIOjtxorrYsOxn6DCntuEDQBfk1tI_SKQw0cbsDX-fUVJ6XPBBp_iTwNVtfyWNvatjR1A9hO')
        .then((String token) async {
      assert(token != null);
      setState(() {
        fcm_token = token;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: loading == true ? Container(
            height: height,
            child: SpinKitFadingCircle(
              itemBuilder: (BuildContext context, int index) {
                return DecoratedBox(
                  decoration: BoxDecoration(
                    color: index.isEven ? Colors.yellow : Colors.blue,
                  ),
                );
              },
            ),
          ) : SafeArea(
            child: Container(
              width: width,
              color: Color.fromRGBO(234, 239, 255, 3),
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => Login()),
                              );
                            },
                            child: Container(
                                margin: EdgeInsets.only(
                                    top: height / 15, left: width / 20),
                                child: Icon(
                                  Icons.close,
                                  size: height / 35,
                                )),
                          ),
                          Row(
                            children: [
                              Container(
                                  margin: EdgeInsets.only(
                                    top: height / 15,
                                  ),
                                  child: Text(
                                    "Already have an account? ",
                                    style: TextStyle(fontSize: height / 45),
                                  )),
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Login()),
                                  );
                                },
                                child: Container(
                                    margin: EdgeInsets.only(
                                        top: height / 15, right: width / 20),
                                    child: Text(
                                      "Sign in",
                                      style: TextStyle(
                                          color: Colors.amberAccent,
                                          fontSize: height / 45),
                                    )),
                              ),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(
                        height: height / 8,
                      ),
                      Container(
                        width: width,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(40),
                              topRight: Radius.circular(40),
                            )),
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: width / 6),
                              child: Text(
                                'Getting Started',
                                style: TextStyle(
                                  fontFamily: 'DMSans-Bold',
                                  fontSize: height / 30,
                                  color: const Color(0xff172b4d),
                                  letterSpacing: -0.3999999847412109,
                                  height: 1.3333333333333333,
                                ),
                                textHeightBehavior: TextHeightBehavior(
                                    applyHeightToFirstAscent: false),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: height / 70),
                              child: Text(
                                'Create an account to continue!',
                                style: TextStyle(
                                  fontFamily: 'DMSans-Regular',
                                  fontSize: height / 45,
                                  color: const Color(0xff7a869a),
                                  letterSpacing: -0.3999999961853027,
                                  height: 1.7142857142857142,
                                ),
                                textHeightBehavior: TextHeightBehavior(
                                    applyHeightToFirstAscent: false),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            SizedBox(
                              height: height / 14,
                            ),
                            Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(left: width * 0.09),
                                  child: Text(
                                    "Full Name",
                                    style: TextStyle(
                                        color: Color.fromRGBO(193, 199, 208, 3),
                                        fontSize: height / 45),
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              width: width / 1.1,
                              margin: EdgeInsets.fromLTRB(
                                  width * 0.09, 0, width * 0.09, 0),
                              child: TextFormField(
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter full name';
                                  }
                                  return null;
                                },
                                controller: _firstNameController,
                                autofocus: false,
                                style: TextStyle(
                                    fontSize: height / 45, color: Colors.black),
                                decoration: InputDecoration(
                                  prefixIcon: Icon(
                                    Icons.account_circle,
                                    size: height / 25,
                                  ),
                                  suffixIcon: Icon(
                                    Icons.done,
                                    color: Colors.amber,
                                    size: height / 25,
                                  ),
                                  border: InputBorder.none,
                                  hintText: 'Foulen ben foulen',
                                  filled: true,
                                  fillColor: Color.fromRGBO(244, 245, 247, 3),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  enabledBorder: UnderlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: height / 30,
                            ),
                            Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(left: width * 0.09),
                                  child: Text(
                                    "Email",
                                    style: TextStyle(
                                        color: Color.fromRGBO(193, 199, 208, 3),
                                        fontSize: height / 45),
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              width: width / 1.1,
                              margin: EdgeInsets.fromLTRB(
                                  width * 0.09, 0, width * 0.09, 0),
                              child: TextFormField(
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter email address';
                                  }
                                  return null;
                                },
                                controller: _emailController,
                                autofocus: false,
                                style: TextStyle(
                                    fontSize: height / 45, color: Colors.black),
                                decoration: InputDecoration(
                                  prefixIcon: Icon(
                                    Icons.email_rounded,
                                    size: height / 25,
                                  ),
                                  suffixIcon: Icon(
                                    Icons.done,
                                    color: Colors.amber,
                                    size: height / 25,
                                  ),
                                  border: InputBorder.none,
                                  hintText: 'Exemple@exemple',
                                  filled: true,
                                  fillColor: Color.fromRGBO(244, 245, 247, 3),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  enabledBorder: UnderlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: height / 30,
                            ),
                            Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(left: width * 0.09),
                                  child: Text(
                                    "Phone Number",
                                    style: TextStyle(
                                        color: Color.fromRGBO(193, 199, 208, 3),
                                        fontSize: height / 45),
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              width: width / 1.1,
                              margin: EdgeInsets.fromLTRB(
                                  width * 0.09, 0, width * 0.09, 0),
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter phone';
                                  }else if(value.length > 8 || value.length < 8){
                                    return 'Phone should have 8 characters';
                                  }
                                  return null;
                                },
                                controller: _phoneController,
                                autofocus: false,
                                style: TextStyle(
                                    fontSize: height / 45, color: Colors.black),
                                decoration: InputDecoration(
                                  prefixIcon: Icon(
                                    Icons.phone_android,
                                    size: height / 25,
                                  ),
                                  suffixIcon: Icon(
                                    Icons.done,
                                    color: Colors.amber,
                                    size: height / 25,
                                  ),
                                  border: InputBorder.none,
                                  hintText: '21 111 111',
                                  filled: true,
                                  fillColor: Color.fromRGBO(244, 245, 247, 3),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  enabledBorder: UnderlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: height / 30,
                            ),
                            Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(left: width * 0.09),
                                  child: Text(
                                    "Password",
                                    style: TextStyle(
                                        color: Color.fromRGBO(193, 199, 208, 3),
                                        fontSize: height / 45),
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              width: width / 1.1,
                              margin: EdgeInsets.fromLTRB(
                                  width * 0.09, 0, width * 0.09, 0),
                              child: TextFormField(
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter password';
                                  }else if(value.length < 8){
                                    return 'Phone should have at least 8 characters';
                                  }
                                  return null;
                                },
                                controller: _passwordController,
                                obscureText: true,
                                autofocus: false,
                                style: TextStyle(
                                    fontSize: height / 45, color: Colors.black),
                                decoration: InputDecoration(
                                  prefixIcon: Icon(
                                    Icons.admin_panel_settings_sharp,
                                    size: height / 25,
                                  ),
                                  suffixIcon: Icon(
                                    Icons.done,
                                    color: Colors.amber,
                                    size: height / 25,
                                  ),
                                  border: InputBorder.none,
                                  hintText: '••••••••••••• ',
                                  filled: true,
                                  fillColor: Color.fromRGBO(244, 245, 247, 3),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  enabledBorder: UnderlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              width: width,
                              margin: EdgeInsets.only(left: width * 0.09),
                              child: Row(
                                children: [
                                  Checkbox(
                                    value: checkboxValue,
                                    onChanged: (val) {
                                      setState(() {
                                        checkboxValue = val;
                                      });
                                      print(checkboxValue);
                                    },
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Container(
                                            margin: EdgeInsets.only(top: width / 50),
                                            child: Text(
                                              "By creating an account, you agree to our ",
                                              style: TextStyle(fontSize: height / 50),
                                            )),
                                        GestureDetector(
                                          onTap: (){
                                            getTerms().then((terms){
                                              print(terms);
                                              showDialog(
                                                  context: context,
                                                  builder:
                                                      (BuildContext context) {
                                                    return Terms(terms: terms['data']['terms']);
                                                  });
                                            });
                                          },
                                          child: Container(
                                            child: Text(
                                              "Term and Conditions",
                                              style: TextStyle(
                                                  color: Colors.amber, fontSize: height / 50),
                                            ),
                                          ),
                                        ),

                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              height: height / 15,
                              width: width / 1.25,
                              margin: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.width / 25,),
                              child: InkWell(
                                onTap: () {},
                                // ignore: deprecated_member_use
                                child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  onPressed: () async {

                                    if(_formKey.currentState.validate()){
                                      setState(() {
                                        loading = true;
                                      });
                                      if (checkboxValue == true) {
                                        _getDeviceInfo().then((device) {
                                          registration(
                                            _firstNameController.text,
                                            _emailController.text,
                                            _phoneController.text,
                                            _passwordController.text,
                                            device.getUuid(),
                                            device.getPlatform(),
                                            device.getOs_version(),
                                            fcm_token,
                                            device.getModel(),
                                          ).then((result) {
                                            setState(() {
                                              loading = false;
                                            });
                                            print(result);
                                            if (result['status']['code'] == 403) {
                                              print(result['status']['code']);
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        OTPReceived(
                                                          email:
                                                          _emailController.text,
                                                          phone: result['data']
                                                          ['phone'],
                                                        )),
                                              );
                                            } else if (result['status']['code'] ==
                                                404) {
                                              String messageBody = "";
                                              if (result['message']
                                                  .containsKey('first_name')) {
                                                messageBody += result['message']
                                                ['first_name'][0] +
                                                    "\n";
                                              }
                                              if (result['message']
                                                  .containsKey('email')) {
                                                messageBody += result['message']
                                                ['email'][0] +
                                                    "\n";
                                              }
                                              if (result['message']
                                                  .containsKey('phone')) {
                                                messageBody += result['message']
                                                ['phone'][0] +
                                                    "\n";
                                              }
                                              if (result['message']
                                                  .containsKey('password')) {
                                                messageBody += result['message']
                                                ['password'][0] +
                                                    "\n";
                                              }
                                              showDialog(
                                                  context: context,
                                                  builder: (BuildContext context) {
                                                    return DialogScreen(
                                                      color: Colors.red,
                                                      icon: Icons.error_outline,
                                                      title: 'Error',
                                                      message: messageBody,
                                                      message2: '',
                                                      buttonText: 'Close',
                                                    );
                                                  });
                                            } else if (result['status']['code'] ==
                                                402) {
                                              showDialog(
                                                  context: context,
                                                  builder: (BuildContext context) {
                                                    return DialogScreen(
                                                      color: Colors.red,
                                                      icon: Icons.error_outline,
                                                      title: 'Error',
                                                      message: result['message'],
                                                      message2: '',
                                                      buttonText: 'Close',
                                                    );
                                                  });
                                            }
                                          });
                                        });
                                      } else {
                                        showDialog(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return DialogScreen(
                                                color: Colors.red,
                                                icon: Icons.error_outline,
                                                title: 'Error',
                                                message:
                                                'Accept our terms and conditions',
                                                message2: '',
                                                buttonText: 'Close',
                                              );
                                            });
                                      }
                                    }
                                  },
                                  color: Colors.amber,
                                  textColor: Colors.white,
                                  child: Text("Sign up".toUpperCase(),
                                      style: TextStyle(
                                          fontSize: height / 46,
                                          color: Colors.white)),
                                ),
                              ),
                            ),
                            // Container(
                            //   height: height / 15,
                            //   width: width / 1.25,
                            //   margin: EdgeInsets.only(
                            //     top: height / 55
                            //   ),
                            //   child: InkWell(
                            //     onTap: () {},
                            //     // ignore: deprecated_member_use
                            //     child: RaisedButton(
                            //       shape: RoundedRectangleBorder(
                            //         borderRadius: BorderRadius.circular(15.0),
                            //       ),
                            //       onPressed: () {
                            //
                            //       },
                            //       color: Colors.blueAccent,
                            //       textColor: Colors.white,
                            //       child: Row(
                            //         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            //         children: [
                            //           Icon(Icons.facebook_outlined),
                            //           Text("Sign up with Facebook".toUpperCase(),
                            //               style: TextStyle(
                            //                   fontSize: height / 46, color: Colors.white)),
                            //         ],
                            //       ),
                            //     ),
                            //   ),
                            // ),
                            Container(
                              height: height / 15,
                              width: width / 1.25,
                              margin: EdgeInsets.only(
                                top: height / 55, bottom: height / 55
                              ),
                              child: InkWell(
                                onTap: () {},
                                // ignore: deprecated_member_use
                                child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  onPressed: () async{
                                    _getDeviceInfo().then((device) {
                                      authGoogle().then((result) {
                                        social_registration_login(
                                            'google',
                                            result.email,
                                            result.displayName,
                                            result.displayName,
                                            device.getUuid(),
                                            device.getPlatform(),
                                            device.os_version,
                                            fcm_token,
                                            device.getModel())
                                            .then((loginResult) async {
                                          print(loginResult);
                                          if(loginResult['status']['code'] == 203){
                                            showDialog(
                                                context: context,
                                                builder: (BuildContext context) {
                                                  return UpdateLogin(
                                                    gender: 1,
                                                    birthday: DateTime(1994, 5, 31),
                                                    email: loginResult['data']
                                                    ['user']['email'],
                                                    last_name: loginResult['data']
                                                    ['user']['last_name'],
                                                    first_name: loginResult['data']
                                                    ['user']['first_name'],
                                                  );
                                                });
                                          }else if(loginResult['status']['code'] == 200){
                                            SharedPreferences preferences =
                                            await SharedPreferences
                                                .getInstance();

                                            preferences.setString(
                                                'idUser',
                                                (loginResult['data']['user']['hashed_id'])
                                                    .toString());

                                            preferences.setString(
                                                'userName',
                                                loginResult['data']['user']['first_name']
                                                    .toString());

                                            preferences.setBool('connected', true);

                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) => Dashboard()),
                                            );
                                          }

                                        });
                                      });
                                    });

                                  },
                                  color: Colors.red,
                                  textColor: Colors.white,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Icon(FontAwesome.google),
                                      Text("Sign up with Google".toUpperCase(),
                                          style: TextStyle(
                                              fontSize: height / 46, color: Colors.white)),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Positioned(
                    top: height / 6.3,
                    child: Container(
                      width: height / 7.5,
                      child: Image.asset(
                        "assets/images/profilepic.png",
                        fit: BoxFit.fill,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<Device> _getDeviceInfo() async {
    var deviceInfo = DeviceInfoPlugin();

    if (Platform.isIOS) {
      var iosDeviceInfo = await deviceInfo.iosInfo;
      String uuid = iosDeviceInfo.identifierForVendor;
      String platform = "IOS";
      int os_version = int.parse(iosDeviceInfo.systemVersion);
      String model = iosDeviceInfo.utsname.machine;

      Device device = Device(uuid, platform, os_version, model, fcm_token);
      return device;
    } else {
      var androidDeviceInfo = await deviceInfo.androidInfo;
      String uuid = androidDeviceInfo.androidId;
      String platform = "Android";
      int os_version = androidDeviceInfo.version.sdkInt;
      String model = androidDeviceInfo.model;

      Device device = Device(uuid, platform, os_version, model, fcm_token);

      return device;
    }
  }
}
