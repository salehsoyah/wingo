import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/controllers/authController.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/signinScreens/forgotPassword.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class VerifyForgotPassword extends StatefulWidget {
  @override
  final String email;
  final int phone;

  VerifyForgotPassword({Key key, @required this.email, @required this.phone})
      : super(key: key);

  _VerifyForgotPasswordState createState() => _VerifyForgotPasswordState();
}

class _VerifyForgotPasswordState extends State<VerifyForgotPassword> {
  final _tokenNb1 = TextEditingController();
  final _tokenNb2 = TextEditingController();
  final _tokenNb3 = TextEditingController();
  final _tokenNb4 = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  final focus1 = FocusNode();
  final focus2 = FocusNode();
  final focus3 = FocusNode();
  bool loading = false;



  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: SafeArea(
            child: loading == true ? Container(
              height: height,
              child: SpinKitFadingCircle(
                itemBuilder: (BuildContext context, int index) {
                  return DecoratedBox(
                    decoration: BoxDecoration(
                      color: index.isEven
                          ? Colors.yellow
                          : Colors.blue,
                    ),
                  );
                },
              ),
            ) : Container(
              width: double.infinity,
              color: Color.fromRGBO(234, 239, 255, 3),
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Container(
                                margin: EdgeInsets.only(
                                    top: height / 15, left: width / 20),
                                child: Icon(
                                  Icons.close,
                                  size: height / 35,
                                )),
                          ),
                          Spacer()
                        ],
                      ),
                      SizedBox(
                        height: height / 8,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(40),
                              topRight: Radius.circular(40),
                            )),
                        child: Column(
                          children: [
                            SizedBox(height: height / 10),
                            Container(
                              child: Text(
                                'Verify your identity',
                                style: TextStyle(
                                  fontFamily: 'DMSans-Bold',
                                  fontSize: height * 0.035,
                                  color: const Color(0xff172b4d),
                                  letterSpacing: -0.3999999847412109,
                                  height: 1.3333333333333333,
                                ),
                                textHeightBehavior: TextHeightBehavior(
                                    applyHeightToFirstAscent: false),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            SizedBox(
                              height: height / 100,
                            ),
                            Container(
                              child: Text(
                                'We have just sent a code ${widget.email}!',
                                style: TextStyle(
                                  fontFamily: 'DMSans-Regular',
                                  fontSize: height * 0.023,
                                  color: const Color(0xff7a869a),
                                  letterSpacing: -0.3999999961853027,
                                  height: 1.7142857142857142,
                                ),
                                textHeightBehavior: TextHeightBehavior(
                                    applyHeightToFirstAscent: false),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: height / 15),
                                  child: SizedBox(
                                    width: height / 10,
                                    height: height / 10,
                                    child: Container(
                                        decoration: BoxDecoration(
                                          color: Color(0xFFF6F5FA),
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(15),
                                          ),
                                          boxShadow: <BoxShadow>[
                                            BoxShadow(
                                                color: Colors.black26,
                                                blurRadius: 5.0,
                                                spreadRadius: 1,
                                                offset: Offset(0.0, 0.75))
                                          ],
                                        ),
                                        child: Center(
                                          child: Container(
                                            child: TextFormField(
                                              onFieldSubmitted: (v) {
                                                FocusScope.of(context)
                                                    .requestFocus(focus1);
                                              },
                                              textInputAction:
                                              TextInputAction.next,
                                              validator: (value) {
                                                if (value == null ||
                                                    value.isEmpty) {
                                                  return '   Required';
                                                }
                                                return null;
                                              },
                                              keyboardType:
                                              TextInputType.number,
                                              textAlign: TextAlign.center,
                                              inputFormatters: [
                                                LengthLimitingTextInputFormatter(
                                                    1)
                                              ],
                                              controller: _tokenNb1,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: height / 45),
                                              decoration: new InputDecoration(
                                                border: InputBorder.none,
                                                focusedBorder: InputBorder.none,
                                                enabledBorder: InputBorder.none,
                                                errorBorder: InputBorder.none,
                                                disabledBorder:
                                                InputBorder.none,
                                              ),
                                            ),
                                          ),
                                        )),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: height / 15),
                                  child: SizedBox(
                                    width: height / 10,
                                    height: height / 10,
                                    child: Container(
                                        decoration: BoxDecoration(
                                          color: Color(0xFFF6F5FA),
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(15),
                                          ),
                                          boxShadow: <BoxShadow>[
                                            BoxShadow(
                                                color: Colors.black26,
                                                blurRadius: 5.0,
                                                spreadRadius: 1,
                                                offset: Offset(0.0, 0.75))
                                          ],
                                        ),
                                        child: Center(
                                          child: Container(
                                            child: TextFormField(
                                              focusNode: focus1,
                                              onFieldSubmitted: (v){
                                                FocusScope.of(context).requestFocus(focus2);
                                              },
                                              textInputAction: TextInputAction.next,
                                              validator: (value) {
                                                if (value == null ||
                                                    value.isEmpty) {
                                                  return '   Required';
                                                }
                                                return null;
                                              },
                                              textAlign: TextAlign.center,
                                              inputFormatters: [
                                                LengthLimitingTextInputFormatter(
                                                    1)
                                              ],
                                              keyboardType:
                                              TextInputType.number,
                                              controller: _tokenNb2,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: height / 45),
                                              decoration: new InputDecoration(
                                                border: InputBorder.none,
                                                focusedBorder: InputBorder.none,
                                                enabledBorder: InputBorder.none,
                                                errorBorder: InputBorder.none,
                                                disabledBorder:
                                                InputBorder.none,
                                              ),
                                            ),
                                          ),
                                        )),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: height / 15),
                                  child: SizedBox(
                                    width: height / 10,
                                    height: height / 10,
                                    child: Container(
                                        decoration: BoxDecoration(
                                          color: Color(0xFFF6F5FA),
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(15),
                                          ),
                                          boxShadow: <BoxShadow>[
                                            BoxShadow(
                                                color: Colors.black26,
                                                blurRadius: 5.0,
                                                spreadRadius: 1,
                                                offset: Offset(0.0, 0.75))
                                          ],
                                        ),
                                        child: Center(
                                          child: Container(
                                            child: TextFormField(
                                              focusNode: focus2,
                                              onFieldSubmitted: (v){
                                                FocusScope.of(context).requestFocus(focus3);
                                              },
                                              textInputAction: TextInputAction.next,
                                              validator: (value) {
                                                if (value == null ||
                                                    value.isEmpty) {
                                                  return '   Required';
                                                }
                                                return null;
                                              },
                                              textAlign: TextAlign.center,
                                              inputFormatters: [
                                                LengthLimitingTextInputFormatter(
                                                    1)
                                              ],
                                              keyboardType:
                                              TextInputType.number,
                                              controller: _tokenNb3,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: height / 45),
                                              decoration: new InputDecoration(
                                                border: InputBorder.none,
                                                focusedBorder: InputBorder.none,
                                                enabledBorder: InputBorder.none,
                                                errorBorder: InputBorder.none,
                                                disabledBorder:
                                                InputBorder.none,
                                              ),
                                            ),
                                          ),
                                        )),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: height / 15),
                                  child: SizedBox(
                                    width: height / 10,
                                    height: height / 10,
                                    child: Container(
                                        decoration: BoxDecoration(
                                          color: Color(0xFFF6F5FA),
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(15),
                                          ),
                                          boxShadow: <BoxShadow>[
                                            BoxShadow(
                                                color: Colors.black26,
                                                blurRadius: 5.0,
                                                spreadRadius: 1,
                                                offset: Offset(0.0, 0.75))
                                          ],
                                        ),
                                        child: Center(
                                          child: Container(
                                            child: TextFormField(
                                              focusNode: focus3,
                                              validator: (value) {
                                                if (value == null ||
                                                    value.isEmpty) {
                                                  return '   Required';
                                                }
                                                return null;
                                              },
                                              textAlign: TextAlign.center,
                                              inputFormatters: [
                                                LengthLimitingTextInputFormatter(
                                                    1)
                                              ],
                                              keyboardType:
                                              TextInputType.number,
                                              controller: _tokenNb4,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: height / 45),
                                              decoration: new InputDecoration(
                                                border: InputBorder.none,
                                                focusedBorder: InputBorder.none,
                                                enabledBorder: InputBorder.none,
                                                errorBorder: InputBorder.none,
                                                disabledBorder:
                                                InputBorder.none,
                                              ),
                                            ),
                                          ),
                                        )),
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(top: height / 40),
                                    child: Text(
                                      ' I didnt receive code.',
                                      style: TextStyle(
                                        fontFamily: 'DMSans-Regular',
                                        fontSize: height / 50,
                                        color: const Color(0xff7a869a),
                                        letterSpacing: -0.3999999961853027,
                                        height: 1.7142857142857142,
                                      ),
                                      textHeightBehavior: TextHeightBehavior(
                                          applyHeightToFirstAscent: false),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        loading = true;
                                      });
                                      forgotPasswordMail(widget.email).then((result) {
                                        setState(() {
                                          loading = false;
                                        });
                                        showDialog(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return DialogScreen(
                                                color: Colors.amber,
                                                icon: Icons.check_circle,
                                                title: 'Success',
                                                message:
                                                'Password verification mail sent',
                                                message2: '',
                                                buttonText: 'Close',
                                              );
                                            });
                                      });
                                    },
                                    child: Container(
                                      margin: EdgeInsets.only(top: height / 40),
                                      child: Text(
                                        'Resend Code',
                                        style: TextStyle(
                                          fontFamily: 'DMSans-Regular',
                                          fontSize: height / 50,
                                          color: Colors.amber,
                                          letterSpacing: -0.3999999961853027,
                                          height: 1.7142857142857142,
                                        ),
                                        textHeightBehavior: TextHeightBehavior(
                                            applyHeightToFirstAscent: false),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              height: height / 15,
                              width: width / 1.25,
                              margin: EdgeInsets.only(top: height / 7),
                              child: InkWell(
                                onTap: () {},
                                // ignore: deprecated_member_use
                                child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  onPressed: () {
                                    if (_formKey.currentState.validate()) {
                                      if ((_tokenNb1.text == "") ||
                                          (_tokenNb2.text == "") ||
                                          (_tokenNb3.text == "") ||
                                          (_tokenNb4.text == "")) {
                                        showDialog(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return DialogScreen(
                                                color: Colors.red,
                                                icon: Icons.error_outline,
                                                title: 'Error',
                                                message:
                                                'Enter code to verify your identity',
                                                message2: '',
                                                buttonText: 'Close',
                                              );
                                            });
                                      } else {
                                        print(_tokenNb1.text);
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ForgotPassword(
                                                    token: _tokenNb1.text +
                                                        _tokenNb2.text +
                                                        _tokenNb3.text +
                                                        _tokenNb4.text,
                                                    email: widget.email,
                                                  )),
                                        );
                                      }
                                    }
                                  },
                                  color: Colors.amber,
                                  textColor: Colors.white,
                                  child: Text("Next".toUpperCase(),
                                      style: TextStyle(
                                          fontSize: height / 45,
                                          color: Colors.white)),
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Positioned(
                    top: height / 6.3,
                    child: Container(
                      width: height / 7.5,
                      child: Image(
                        image: AssetImage("assets/images/code.png"),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
