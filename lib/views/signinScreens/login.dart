import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/authController.dart';
import 'package:flutter_app/controllers/localAuthController.dart';
import 'package:flutter_app/models/device.dart';
import 'package:flutter_app/views/dashboard/dashboard.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/dialog/updateLogin.dart';
import 'package:flutter_app/views/signinScreens/createaccount.dart';
import 'package:flutter_app/views/signinScreens/forgotPassword.dart';
import 'package:flutter_app/views/signinScreens/opt_received_social.dart';
import 'package:flutter_app/views/signinScreens/otp_received.dart';
import 'package:flutter_app/views/signinScreens/passwordrecovery.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:local_auth/local_auth.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  final _firebaseMessaging = FirebaseMessaging.instance;
  String fcm_token;
  bool loading = false;

  bool _isLoggedIn = false;
  Map _userObj = {};
  final _formKey = GlobalKey<FormState>();

  getUserId() async {
    SharedPreferences preferences =
        await SharedPreferences
        .getInstance();

    return preferences.getString('idUser');
  }

  String userId;

  void initState() {

    getUserId().then((idUser){
      setState(() {
        userId = idUser;
      });
    });

    _firebaseMessaging.getToken().then((String token) async {
      assert(token != null);
      setState(() {
        fcm_token = token;
      });
    });


    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return new WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: Color.fromRGBO(234, 239, 255, 3),
        resizeToAvoidBottomInset: true,
        body: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: SafeArea(
              child: loading == true ? Container(
                height: height,
                child: SpinKitFadingCircle(
                  itemBuilder: (BuildContext context, int index) {
                    return DecoratedBox(
                      decoration: BoxDecoration(
                        color: index.isEven ? Colors.yellow : Colors.blue,
                      ),
                    );
                  },
                ),
              ) : Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Container(
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                                margin: EdgeInsets.only(
                                    top: height / 15, left: width / 20),
                                child: Icon(
                                  Icons.close,
                                  size: height / 35,
                                )),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => PasswordRecovery()),
                                );
                              },
                              child: Container(
                                  margin: EdgeInsets.only(
                                      top: height / 15, right: width / 20),
                                  child: Text(
                                    "Forgot your credentials?",
                                    style: TextStyle(fontSize: height / 45),
                                  )),
                            ),
                          ],
                        ), //header
                        SizedBox(
                          height: height / 8,
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(40),
                                topRight: Radius.circular(40),
                              )),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              SizedBox(height: height / 10),
                              Container(
                                child: Text(
                                  'Let’s Sign You In',
                                  style: TextStyle(
                                    fontFamily: 'DMSans-Bold',
                                    fontSize: height * 0.035,
                                    color: const Color(0xff172b4d),
                                  ),
                                  textHeightBehavior: TextHeightBehavior(
                                      applyHeightToFirstAscent: false),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              SizedBox(
                                height: height / 100,
                              ),
                              Container(
                                child: Text(
                                  'Welcome back, you’ve been missed!',
                                  style: TextStyle(
                                    fontFamily: 'DMSans-Regular',
                                    fontSize: height * 0.023,
                                    color: const Color(0xff7a869a),
                                    letterSpacing: -0.3999999961853027,
                                    height: 1.7142857142857142,
                                  ),
                                  textHeightBehavior: TextHeightBehavior(
                                      applyHeightToFirstAscent: false),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              SizedBox(
                                height: height / 14,
                              ),
                              Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(left: width * 0.09),
                                    child: Text(
                                      "Phone Number",
                                      style: TextStyle(
                                          color: Color.fromRGBO(193, 199, 208, 3),
                                          fontSize: height / 45),
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                width: width / 1.1,
                                margin: EdgeInsets.fromLTRB(width * 0.08, 0, 30, 0),
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Please enter phone';
                                    }else if(value.length > 8 || value.length < 8){
                                      return 'Phone should have 8 charachters';
                                    }
                                    return null;
                                  },
                                  keyboardType: TextInputType.number,
                                  controller: _emailController,
                                  autofocus: false,
                                  style: TextStyle(
                                    fontSize: height / 45,
                                    color: Colors.black,
                                  ),
                                  decoration: InputDecoration(
                                    prefixIcon: Icon(
                                      Icons.phone_android,
                                      size: height / 25,
                                    ),
                                    suffixIcon: Icon(
                                      Icons.done,
                                      color: Colors.amber,
                                      size: height / 25,
                                    ),
                                    border: InputBorder.none,
                                    hintText: '21 111 111',
                                    filled: true,
                                    fillColor: Color.fromRGBO(244, 245, 247, 3),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: height / 30,
                              ),
                              Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(left: width / 12),
                                    child: Text(
                                      "Password",
                                      style: TextStyle(
                                          color: Color.fromRGBO(193, 199, 208, 3),
                                          fontSize: height / 45),
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                width: width / 1.1,
                                margin: EdgeInsets.fromLTRB(width * 0.08, 0, 30, 0),
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Please enter password';
                                    }
                                    return null;
                                  },
                                  controller: _passwordController,
                                  obscureText: true,
                                  autofocus: false,
                                  style: TextStyle(
                                      fontSize: height / 45, color: Colors.black),
                                  decoration: InputDecoration(
                                    prefixIcon: Icon(
                                      Icons.admin_panel_settings_sharp,
                                      size: height / 25,
                                    ),
                                    border: InputBorder.none,
                                    hintText: '••••••••••••• ',
                                    filled: true,
                                    fillColor: Color.fromRGBO(244, 245, 247, 3),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: height * 0.02,
                              ),
                              userId != null ? Container(
                                child: InkWell(
                                    onTap: () async {
                                      final isAuthenticated = await LocalAuthApi.authenticate();

                                      if (isAuthenticated) {
                                        SharedPreferences preferences =
                                        await SharedPreferences
                                            .getInstance();
                                        preferences.setBool('connected', true);
                                        Navigator.of(context).pushReplacement(
                                          MaterialPageRoute(builder: (context) => Dashboard()),
                                        );
                                      }
                                    },
                                  child: Image(
                                    width: width / 5.3,
                                    image: AssetImage("assets/images/finger.png",),
                                  ),
                                ),
                              ) : Container(),
                              SizedBox(
                                height: height * 0.02,
                              ),
                              Container(
                                height: height / 15,
                                width: width / 1.25,
                                child: InkWell(
                                  onTap: () {},
                                  // ignore: deprecated_member_use
                                  child: RaisedButton(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                    ),
                                    onPressed: () {
                                     if(_formKey.currentState.validate()){
                                       setState(() {
                                         loading = true;
                                       });
                                       _getDeviceInfo().then((device) {
                                         signIn(
                                           _emailController.text,
                                           _passwordController.text,
                                           device.getUuid(),
                                           device.getPlatform(),
                                           device.getOs_version(),
                                           fcm_token,
                                           device.getModel(),
                                         ).then((result) async {
                                           setState(() {
                                             loading = false;
                                           });

                                           print(result);
                                           if (result['status']['code'] == 401) {
                                             showDialog(
                                                 context: context,
                                                 builder: (BuildContext context) {
                                                   return DialogScreen(
                                                     color: Colors.red,
                                                     icon: Icons.error_outline,
                                                     title: 'Error',
                                                     message: result['message'],
                                                     message2:
                                                     'Please enter a valid informations',
                                                     buttonText: 'Close',
                                                   );
                                                 });
                                           } else if (result['status']['code'] ==
                                               403) {
                                             Navigator.push(
                                               context,
                                               MaterialPageRoute(
                                                   builder: (context) => OTPReceivedSocial(
                                                     email:
                                                     result['data']
                                                     ['phone']['email'],
                                                     phone: result['data']
                                                     ['phone']['phone'].toString(), name: result['data']['phone']['first_name'], id: result['data']['phone']['hashed_id'],
                                                   )),
                                             );
                                             showDialog(
                                                 context: context,
                                                 builder: (BuildContext context) {
                                                   return DialogScreen(
                                                     color: Colors.red,
                                                     icon: Icons.error_outline,
                                                     title: 'Error',
                                                     message: result['message'],
                                                     message2: '',
                                                     buttonText: 'Close',
                                                   );
                                                 });
                                           } else if (result['status']['code'] ==
                                               200) {
                                             print(result['device']);
                                             SharedPreferences preferences =
                                             await SharedPreferences
                                                 .getInstance();

                                             preferences.setString(
                                                 'idUser',
                                                 (result['user']['hashed_id'])
                                                     .toString());

                                             preferences.setString(
                                                 'userName',
                                                 result['user']['first_name']
                                                     .toString());

                                             preferences.setBool('connected', true);


                                             Navigator.push(
                                               context,
                                               MaterialPageRoute(
                                                   builder: (context) =>
                                                       Dashboard()),
                                             );
                                           }
                                         });
                                       });
                                     }
                                    },
                                    color: Colors.amber,
                                    textColor: Colors.white,
                                    child: Text("Sign in".toUpperCase(),
                                        style: TextStyle(
                                            fontSize: height / 46,
                                            color: Colors.white)),
                                  ),
                                ),
                              ),

                              // Container(
                              //   height: height / 15,
                              //   width: width / 1.25,
                              //   margin: EdgeInsets.only(
                              //     top: height / 55,
                              //   ),
                              //   child: InkWell(
                              //     onTap: () {},
                              //     // ignore: deprecated_member_use
                              //     child: RaisedButton(
                              //       shape: RoundedRectangleBorder(
                              //         borderRadius: BorderRadius.circular(15.0),
                              //       ),
                              //       onPressed: () async {
                              //         print("ddd");
                              //         final isAuthenticated = await LocalAuthApi.authenticate();
                              //
                              //         if (isAuthenticated) {
                              //           Navigator.of(context).pushReplacement(
                              //             MaterialPageRoute(builder: (context) => Dashboard()),
                              //           );
                              //         }
                              //       },
                              //       color: Colors.blueAccent,
                              //       textColor: Colors.white,
                              //       child: Row(
                              //         mainAxisAlignment:
                              //         MainAxisAlignment.spaceEvenly,
                              //         children: [
                              //           Icon(Icons.facebook_outlined),
                              //           Text("Sign in with biometrics".toUpperCase(),
                              //               style: TextStyle(
                              //                   fontSize: height / 46,
                              //                   color: Colors.white)),
                              //         ],
                              //       ),
                              //     ),
                              //   ),
                              // ),
                              Container(
                                height: height / 15,
                                width: width / 1.25,
                                margin: EdgeInsets.only(
                                  top: height / 55,
                                ),
                                child: InkWell(
                                  onTap: () {},
                                  // ignore: deprecated_member_use
                                  child: RaisedButton(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                    ),
                                    onPressed: () {

                                      _getDeviceInfo().then((device) {
                                        authGoogle().then((result) {
                                          social_registration_login(
                                                  'google',
                                                  result.email,
                                                  result.displayName,
                                                  result.displayName,
                                                  device.getUuid(),
                                                  device.getPlatform(),
                                                  device.os_version,
                                                  fcm_token,
                                                  device.getModel())
                                              .then((loginResult) async {
                                            print(loginResult);
                                            if(loginResult['status']['code'] == 203){
                                              showDialog(
                                                  context: context,
                                                  builder: (BuildContext context) {
                                                    return UpdateLogin(
                                                      gender: 1,
                                                      birthday: DateTime(1994, 5, 31),
                                                      email: loginResult['data']
                                                      ['user']['email'],
                                                      last_name: loginResult['data']
                                                      ['user']['last_name'],
                                                      first_name: loginResult['data']
                                                      ['user']['first_name'],
                                                    );
                                                  });
                                            }else if(loginResult['status']['code'] == 200){
                                              SharedPreferences preferences =
                                                  await SharedPreferences
                                                  .getInstance();

                                              preferences.setString(
                                                  'idUser',
                                                  (loginResult['data']['user']['hashed_id'])
                                                      .toString());

                                              preferences.setString(
                                                  'idSession',
                                                  (loginResult['data']['user']['session_id'])
                                                      .toString());

                                              preferences.setString(
                                                  'userName',
                                                  loginResult['data']['user']['first_name']
                                                      .toString());

                                              preferences.setBool('connected', true);

                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) => Dashboard()),
                                              );
                                            }

                                          });
                                        });
                                      });
                                    },
                                    color: Colors.red,
                                    textColor: Colors.white,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Icon(FontAwesome.google),
                                        Text("Sign in with Google".toUpperCase(),
                                            style: TextStyle(
                                                fontSize: height / 46,
                                                color: Colors.white)),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                height: height / 15,
                                width: width / 1.25,
                                margin: EdgeInsets.only(
                                  top: height / 55,
                                ),
                                child: InkWell(
                                  onTap: () {},
                                  // ignore: deprecated_member_use
                                  child: RaisedButton(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                    ),
                                    onPressed: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => CreateAccount()),
                                      );
                                    },
                                    color: Color.fromRGBO(244, 245, 247, 3),
                                    textColor: Colors.white,
                                    child: Text("Create an account",
                                        style: TextStyle(
                                            fontSize: height / 46,
                                            color: const Color(0xff7a869a))),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 100,
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    top: height / 6.3,
                    child: Container(
                      width: height / 7.5,
                      child: Image.asset(
                        "assets/images/profilepic.png",
                        fit: BoxFit.fill,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<Device> _getDeviceInfo() async {
    var deviceInfo = DeviceInfoPlugin();

    if (Platform.isIOS) {
      var iosDeviceInfo = await deviceInfo.iosInfo;
      String uuid = iosDeviceInfo.identifierForVendor;
      String platform = "IOS";
      int os_version = int.parse(iosDeviceInfo.systemVersion);
      String model = iosDeviceInfo.utsname.machine;

      Device device = Device(uuid, platform, os_version, model, fcm_token);
      return device;
    } else {
      var androidDeviceInfo = await deviceInfo.androidInfo;
      String uuid = androidDeviceInfo.androidId;
      String platform = "Android";
      int os_version = androidDeviceInfo.version.sdkInt;
      String model = androidDeviceInfo.model;

      Device device = Device(uuid, platform, os_version, model, fcm_token);

      return device;
    }
  }

  Widget buildText(String text, bool checked) => Container(
        margin: EdgeInsets.symmetric(vertical: 8),
        child: Row(
          children: [
            checked
                ? Icon(Icons.check, color: Colors.green, size: 24)
                : Icon(Icons.close, color: Colors.red, size: 24),
            const SizedBox(width: 12),
            Text(text, style: TextStyle(fontSize: 24)),
          ],
        ),
      );

  Widget buildAvailability(BuildContext context) => buildButton(
        text: 'Check Availability',
        icon: Icons.event_available,
        onClicked: () async {
          final isAvailable = await LocalAuthApi.hasBiometrics();
          final biometrics = await LocalAuthApi.getBiometrics();

          final hasFingerprint = biometrics.contains(BiometricType.fingerprint);

          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: Text('Availability'),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  buildText('Biometrics', isAvailable),
                  buildText('Fingerprint', hasFingerprint),
                ],
              ),
            ),
          );
        },
      );

  Widget buildAuthenticate(BuildContext context) => buildButton(
        text: 'Authenticate',
        icon: Icons.lock_open,
        onClicked: () async {
          final isAuthenticated = await LocalAuthApi.authenticate();

          if (isAuthenticated) {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => Dashboard()),
            );
          }
        },
      );

  Widget buildButton({
    @required String text,
    @required IconData icon,
    @required VoidCallback onClicked,
  }) =>
      ElevatedButton.icon(
        style: ElevatedButton.styleFrom(
          minimumSize: Size.fromHeight(50),
        ),
        icon: Icon(icon, size: 26),
        label: Text(
          text,
          style: TextStyle(fontSize: 20),
        ),
        onPressed: onClicked,
      );
}
