import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/authController.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/dialog/transaction.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:flutter_app/views/signinScreens/verifyForgotPassword.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();

  final String token;
  final String email;

  ForgotPassword({Key key, @required this.token, this.email}) : super(key: key);
}

class _ForgotPasswordState extends State<ForgotPassword> {
  final _newPassword = TextEditingController();
  final _newPasswordConfirm = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  bool loading = false;

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: SafeArea(
            child: loading == true
                ? Container(
                    height: height,
                    child: SpinKitFadingCircle(
                      itemBuilder: (BuildContext context, int index) {
                        return DecoratedBox(
                          decoration: BoxDecoration(
                            color: index.isEven ? Colors.yellow : Colors.blue,
                          ),
                        );
                      },
                    ),
                  )
                : Container(
                    width: double.infinity,
                    color: Color.fromRGBO(234, 239, 255, 3),
                    child: Stack(
                      alignment: Alignment.bottomCenter,
                      children: [
                        Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: Container(
                                      margin: EdgeInsets.only(
                                          top: height / 15, left: width / 20),
                                      child: Icon(
                                        Icons.close,
                                        size: height / 35,
                                      )),
                                ),
                                Spacer()
                              ],
                            ),
                            SizedBox(
                              height: height / 8,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(40),
                                    topRight: Radius.circular(40),
                                  )),
                              child: Column(
                                children: [
                                  SizedBox(height: height / 10),
                                  Container(
                                    child: Text(
                                      'Reset your password',
                                      style: TextStyle(
                                        fontFamily: 'DMSans-Bold',
                                        fontSize: height * 0.035,
                                        color: const Color(0xff172b4d),
                                        letterSpacing: -0.3999999847412109,
                                        height: 1.3333333333333333,
                                      ),
                                      textHeightBehavior: TextHeightBehavior(
                                          applyHeightToFirstAscent: false),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                  SizedBox(
                                    height: height / 100,
                                  ),
                                  Container(
                                    child: Text(
                                      'At least 8 characters, with uppercase and lowercase letters!',
                                      style: TextStyle(
                                        fontFamily: 'DMSans-Regular',
                                        fontSize: height * 0.023,
                                        color: const Color(0xff7a869a),
                                        letterSpacing: -0.3999999961853027,
                                        height: 1.7142857142857142,
                                      ),
                                      textHeightBehavior: TextHeightBehavior(
                                          applyHeightToFirstAscent: false),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                  SizedBox(
                                    height: height / 14,
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        margin:
                                            EdgeInsets.only(left: width * 0.09),
                                        child: Text(
                                          "New Password",
                                          style: TextStyle(
                                              color: Color.fromRGBO(
                                                  193, 199, 208, 3),
                                              fontSize: height / 45),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Container(
                                    width: width / 1.1,
                                    height: height / 13,
                                    margin: EdgeInsets.fromLTRB(
                                        width * 0.08, 0, width * 0.08, 0),
                                    child: TextFormField(
                                      validator: (value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Please enter new password';
                                        }
                                        return null;
                                      },
                                      controller: _newPassword,
                                      obscureText: true,
                                      autofocus: false,
                                      style: TextStyle(
                                          fontSize: height / 45,
                                          color: Colors.black),
                                      decoration: InputDecoration(
                                        prefixIcon: Icon(
                                          Icons.admin_panel_settings_sharp,
                                          size: height / 25,
                                        ),
                                        border: InputBorder.none,
                                        hintText: '••••••••••••• ',
                                        filled: true,
                                        fillColor:
                                            Color.fromRGBO(244, 245, 247, 3),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                        ),
                                        enabledBorder: UnderlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: height / 30,
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        margin:
                                            EdgeInsets.only(left: width / 12),
                                        child: Text(
                                          "Confirm Password",
                                          style: TextStyle(
                                              color: Color.fromRGBO(
                                                  193, 199, 208, 3),
                                              fontSize: height / 45),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Container(
                                    margin: EdgeInsets.fromLTRB(
                                        width * 0.08, 0, 30, 0),
                                    child: TextFormField(
                                      validator: (value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Please enter new password';
                                        }
                                        return null;
                                      },
                                      controller: _newPasswordConfirm,
                                      obscureText: true,
                                      autofocus: false,
                                      style: TextStyle(
                                          fontSize: height / 45,
                                          color: Colors.black),
                                      decoration: InputDecoration(
                                        prefixIcon: Icon(
                                            Icons.admin_panel_settings_sharp),
                                        border: InputBorder.none,
                                        hintText: '••••••••••••• ',
                                        filled: true,
                                        fillColor:
                                            Color.fromRGBO(244, 245, 247, 3),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                        ),
                                        enabledBorder: UnderlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    height: height / 15,
                                    width: width / 1.25,
                                    margin: EdgeInsets.only(top: height / 7),
                                    child: InkWell(
                                      onTap: () {},
                                      // ignore: deprecated_member_use
                                      child: RaisedButton(
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                        ),
                                        onPressed: () {
                                          print(widget.token);

                                          if (_formKey.currentState
                                              .validate()) {
                                            setState(() {
                                              loading = true;
                                            });
                                            forgetPassword(
                                                    widget.token,
                                                    _newPassword.text,
                                                    _newPasswordConfirm.text)
                                                .then((result) async {
                                              setState(() {
                                                loading = false;
                                              });
                                              if (result['status']['code'] ==
                                                  402) {
                                                showDialog(
                                                    context: context,
                                                    builder:
                                                        (BuildContext context) {
                                                      return DialogScreen(
                                                        color: Colors.red,
                                                        icon:
                                                            Icons.error_outline,
                                                        title: 'Error',
                                                        message:
                                                            result['message'],
                                                        message2: '',
                                                        buttonText: 'Close',
                                                      );
                                                    });
                                              } else if (result['status']
                                                      ['code'] ==
                                                  403) {
                                                showDialog(
                                                    context: context,
                                                    builder:
                                                        (BuildContext context) {
                                                      return DialogScreen(
                                                        color: Colors.red,
                                                        icon:
                                                            Icons.error_outline,
                                                        title: 'Error',
                                                        message:
                                                            result['message'],
                                                        message2: '',
                                                        buttonText: 'Close',
                                                      );
                                                    });
                                              } else if (result['status']
                                                      ['code'] ==
                                                  404) {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          VerifyForgotPassword(
                                                            email: widget.email,
                                                          )),
                                                );
                                                showDialog(
                                                    context: context,
                                                    builder:
                                                        (BuildContext context) {
                                                      return DialogScreen(
                                                        color: Colors.red,
                                                        icon:
                                                            Icons.error_outline,
                                                        title: 'Error',
                                                        message:
                                                            result['message'],
                                                        message2: '',
                                                        buttonText: 'Close',
                                                      );
                                                    });
                                              } else if (result['status']
                                                      ['code'] ==
                                                  200) {
                                                showDialog(
                                                    context: context,
                                                    builder:
                                                        (BuildContext context) {
                                                      return Transaction(
                                                        icon: Icons.done,
                                                        message:
                                                            result['message'],
                                                        title: 'Success',
                                                        type: 1,
                                                      );
                                                    });
                                              }
                                            });
                                          }
                                        },
                                        color: Colors.amber,
                                        textColor: Colors.white,
                                        child: Text("Sign in".toUpperCase(),
                                            style: TextStyle(
                                                fontSize: height / 46,
                                                color: Colors.white)),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        Positioned(
                          top: height / 6.3,
                          child: Container(
                            width: height / 7.5,
                            child: Image(
                              image: AssetImage("assets/images/code.png"),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
          ),
        ),
      ),
    );
  }
}
