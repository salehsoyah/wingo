import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/userController.dart';
import 'package:flutter_app/views/dashboard/customBottomNavigationBar.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/shops/createShop.dart';
import 'package:flutter_app/views/shops/editShop.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:flutter_app/views/user/profile.dart';
import 'package:flutter_app/views/user/verifieLevel.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Notifications extends StatefulWidget {
  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  int _selectedItem;
  String level = '';
  int lvlStatus = 0;
  String pending = '';
  int currentLvlId = 0;
  int canCreateShop = 0;
  String nextLevel;

  bool haveShop = false;
  int shopStatus;
  String shopName;

  bool wingo = false;
  bool transfer = false;
  bool withdrawal = false;
  bool levelNotif = false;
  bool support = false;
  bool deposit = false;
  bool all = false;

  bool loading = false;

  @override
  void initState() {
    getUserInfo().then((result) async {
      if(result['status']['code'] == 1005){
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.remove('connected');
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    Login()));
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return DialogScreen(
                color: Colors.amber,
                icon: Icons.pending_actions,
                title: 'Session',
                message: result['status']['message'],
                message2: '',
                buttonText: 'Close',
              );
            });
      }
      // print(jsonDecode(result['data']['user']['notifications']));
      else if(result['status']['code'] == 200){
        if (jsonDecode(result['data']['user']['notifications'])['wingo'] == 1) {
          setState(() {
            wingo = true;
          });
        }
        if (jsonDecode(result['data']['user']['notifications'])['support'] ==
            1) {
          setState(() {
            support = true;
          });
        }
        if (jsonDecode(result['data']['user']['notifications'])['deposit'] ==
            1) {
          setState(() {
            deposit = true;
          });
        }
        if (jsonDecode(result['data']['user']['notifications'])['transfer'] ==
            1) {
          setState(() {
            transfer = true;
          });
        }
        if (jsonDecode(result['data']['user']['notifications'])['level'] == 1) {
          setState(() {
            levelNotif = true;
          });
        }
        if (jsonDecode(result['data']['user']['notifications'])['withdrawal'] ==
            1) {
          setState(() {
            withdrawal = true;
          });
        }
        if (jsonDecode(result['data']['user']['notifications'])['all'] == 1) {
          setState(() {
            all = true;
          });
        }
        setState(() {
          nextLevel = result['data']['nextLevel']['hashed_id'];

          canCreateShop = result['data']['user']['level']['can_create_shop'];
        });
        setState(() {
          currentLvlId = result['data']['user']['level']['id'];
        });

        if (result['data']['shop'] != null) {
          setState(() {
            haveShop = true;
            shopStatus = result['data']['shop']['status'];
            shopName = result['data']['shop']['title'];
          });
        }

        setState(() {
          level = result['data']['user']['level']['title'];
        });
      }
    });

    getUserLevelRequest().then((result) {
      if (result['status']['code'] == 200) {
        // print(result);
        setState(() {
          pending = result['data']['request']['level']['title'];
        });
      }
    });

    super.initState();
  }

  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return DefaultTabController(
        length: 1,
        child: Scaffold(
          backgroundColor: Color.fromRGBO(234, 239, 255, 3),
          appBar: AppBar(
            elevation: 0.0,
            automaticallyImplyLeading: false,
            iconTheme: IconThemeData(color: Colors.grey[600]),
            backgroundColor: Colors.grey[200],
            foregroundColor: Colors.grey[200],
            title: Text(
              'Notifications',
              style: TextStyle(color: Colors.grey[800]),
            ),
            centerTitle: true,
          ),
          bottomNavigationBar: CustomBottomNavigationBar(
            iconList: [
              Icons.apps,
              Icons.money_outlined,
              Icons.qr_code_scanner_sharp,
              Icons.wallet_membership,
              Icons.account_circle_rounded,
            ],
            onChange: (val) {
              setState(() {
                _selectedItem = 4;
              });
            },
            defaultSelectedIndex: 4,
          ),
          body: SingleChildScrollView(
            child: SafeArea(
              child: loading == true ? Container(
                height: height,
                child: SpinKitFadingCircle(
                  itemBuilder: (BuildContext context, int index) {
                    return DecoratedBox(
                      decoration: BoxDecoration(
                        color: index.isEven ? Colors.yellow : Colors.blue,
                      ),
                    );
                  },
                ),
              ) : Column(
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Container(
                      width: 380,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Container(
                                  width: width / 1.20,
                                  child: Row(
                                    children: [
                                      Checkbox(
                                        value: withdrawal,
                                        onChanged: (val) {
                                          setState(() {
                                            withdrawal = val;
                                          });
                                          print(withdrawal);
                                        },
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                              margin: EdgeInsets.only(
                                                  top: width / 50),
                                              child: Text(
                                                "I want to get notified when a withdrawal\nrequest is approved or rejected",
                                                style: TextStyle(
                                                  fontSize: height / 55,
                                                ),
                                                textAlign: TextAlign.justify,
                                              ))
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Divider(
                            height: 20,
                            thickness: 0.03,
                            color: Colors.black,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Container(
                                  width: width / 1.15,
                                  child: Row(
                                    children: [
                                      Checkbox(
                                        value: deposit,
                                        onChanged: (val) {
                                          setState(() {
                                            deposit = val;
                                          });
                                          print(deposit);
                                        },
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                              margin: EdgeInsets.only(
                                                  top: width / 50),
                                              child: Text(
                                                "I want to get notified when a deposit \nis made",
                                                style: TextStyle(
                                                  fontSize: height / 55,
                                                ),
                                                textAlign: TextAlign.justify,
                                              ))
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Divider(
                            height: 20,
                            thickness: 0.03,
                            color: Colors.black,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Container(
                                  width: width / 1.15,
                                  child: Row(
                                    children: [
                                      Checkbox(
                                        value: transfer,
                                        onChanged: (val) {
                                          setState(() {
                                            transfer = val;
                                          });
                                          print(transfer);
                                        },
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                              margin: EdgeInsets.only(
                                                  top: width / 50),
                                              child: Text(
                                                "I want to get notified when I receive \na transfer or payment",
                                                style: TextStyle(
                                                  fontSize: height / 55,
                                                ),
                                                textAlign: TextAlign.justify,
                                              ))
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Divider(
                            height: 20,
                            thickness: 0.03,
                            color: Colors.black,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Container(
                                  width: width / 1.20,
                                  child: Row(
                                    children: [
                                      Checkbox(
                                        value: levelNotif,
                                        onChanged: (val) {
                                          setState(() {
                                            levelNotif = val;
                                          });
                                          print(wingo);
                                        },
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                              margin: EdgeInsets.only(
                                                  top: width / 50),
                                              child: Text(
                                                "I want to get notified about my level \nstatus",
                                                style: TextStyle(
                                                  fontSize: height / 55,
                                                ),
                                                textAlign: TextAlign.justify,
                                              ))
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Divider(
                            height: 20,
                            thickness: 0.03,
                            color: Colors.black,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Container(
                                  width: width / 1.20,
                                  child: Row(
                                    children: [
                                      Checkbox(
                                        value: support,
                                        onChanged: (val) {
                                          setState(() {
                                            support = val;
                                          });
                                          print(support);
                                        },
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                              margin: EdgeInsets.only(
                                                  top: width / 50),
                                              child: Text(
                                                "I want to get notified when a support \nticket is answered",
                                                style: TextStyle(
                                                  fontSize: height / 55,
                                                ),
                                                textAlign: TextAlign.justify,
                                              ))
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Divider(
                            height: 20,
                            thickness: 0.03,
                            color: Colors.black,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Container(
                                  width: width / 1.20,
                                  child: Row(
                                    children: [
                                      Checkbox(
                                        value: all,
                                        onChanged: (val) {
                                          setState(() {
                                            all = val;
                                          });
                                          print(all);
                                        },
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                              margin: EdgeInsets.only(
                                                  top: width / 50),
                                              child: Text(
                                                "I want to receive all notifications",
                                                style: TextStyle(
                                                  fontSize: height / 55,
                                                ),
                                                textAlign: TextAlign.justify,
                                              ))
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Container(
                      width: 380,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Container(
                                  width: width / 1.20,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Checkbox(
                                        value: wingo,
                                        onChanged: (val) {
                                          setState(() {
                                            wingo = val;
                                          });
                                          print(wingo);
                                        },
                                      ),
                                      Container(
                                          margin:
                                              EdgeInsets.only(top: width / 50),
                                          child: Text(
                                            "I want to receive wingo updates and \nnews",
                                            style: TextStyle(
                                              fontSize: height / 55,
                                            ),
                                            textAlign: TextAlign.justify,
                                          )),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    height: height / 15,
                    width: width / 1.15,
                    child: InkWell(
                      onTap: () {},
                      // ignore: deprecated_member_use
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        onPressed: () async {
                          setState(() {
                            loading = true;
                          });
                          SharedPreferences preferences =
                              await SharedPreferences.getInstance();
                          String userId = preferences.getString('idUser');
                          String id = userId;
                          updateNotifications(
                                  id,
                                  wingo == true ? 1 : 0,
                                  support == true ? 1 : 0,
                                  levelNotif == true ? 1 : 0,
                                  transfer == true ? 1 : 0,
                                  deposit == true ? 1 : 0,
                                  withdrawal == true ? 1 : 0,
                                  all == true ? 1 : 0)
                              .then((result) async {
                            setState(() {
                              loading = false;
                            });

                            if(result['status']['code'] == 200){
                              showDialog(
                                  context: context,
                                  builder:
                                      (BuildContext
                                  context) {
                                    return DialogScreen(
                                      color: Colors
                                          .amber,
                                      icon:
                                      Icons.done,
                                      title:
                                      'Success',
                                      message:
                                      'Notifications Updated',
                                      message2: '',
                                      buttonText:
                                      'Close',
                                    );
                                  });
                            }else if(result['status']['code'] == 1005){
                              SharedPreferences preferences = await SharedPreferences.getInstance();
                              preferences.remove('connected');
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          Login()));
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return DialogScreen(
                                      color: Colors.amber,
                                      icon: Icons.pending_actions,
                                      title: 'Session',
                                      message: result['status']['message'],
                                      message2: '',
                                      buttonText: 'Close',
                                    );
                                  });
                            }
                            else{
                              showDialog(
                                  context: context,
                                  builder:
                                      (BuildContext
                                  context) {
                                    return DialogScreen(
                                      color: Colors
                                          .red,
                                      icon:
                                      Icons.error_outline,
                                      title:
                                      'Error',
                                      message:
                                      result['message'],
                                      message2: '',
                                      buttonText:
                                      'Close',
                                    );
                                  });
                            }
                            print(result);
                          });
                        },
                        color: Colors.amber,
                        textColor: Colors.white,
                        child: Text("Validate".toUpperCase(),
                            style: TextStyle(
                                fontSize: height / 46, color: Colors.white)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
