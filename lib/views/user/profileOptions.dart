import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/userController.dart';
import 'package:flutter_app/views/dashboard/customBottomNavigationBar.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/dialog/language.dart';
import 'package:flutter_app/views/dialog/privacy.dart';
import 'package:flutter_app/views/dialog/qrCode.dart';
import 'package:flutter_app/views/dialog/terms.dart';
import 'package:flutter_app/views/shops/createShop.dart';
import 'package:flutter_app/views/shops/editShop.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:flutter_app/views/user/notifications.dart';
import 'package:flutter_app/views/user/profile.dart';
import 'package:flutter_app/views/user/verification.dart';
import 'package:flutter_app/views/user/verifieLevel.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileOptions extends StatefulWidget {
  @override
  _ProfileOptionsState createState() => _ProfileOptionsState();
}

class _ProfileOptionsState extends State<ProfileOptions> {
  int _selectedItem;
  String level = '';
  int lvlStatus = 0;
  String pending = '';
  int currentLvlId = 0;
  int canCreateShop = 0;
  String nextLevel;
  String qrCode;
  bool haveShop = false;
  int shopStatus;
  String shopName;
  List<String> idDocsRefused = [];
  List<String> idDocs = [];
  List<String> allDocs = [];
  int levelDocumentLength = 0;
  @override
  void initState() {
    getUserLevelRequest().then((value) async {
      print(value);
      if(value['status']['code'] == 1005){
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.remove('connected');
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    Login()));
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return DialogScreen(
                color: Colors.amber,
                icon: Icons.pending_actions,
                title: 'Session',
                message: value['status']['message'],
                message2: '',
                buttonText: 'Close',
              );
            });

      }

      // print(value['data']['request']['documents']);
      if(value['status']['code'] == 200){
        if (value['data']['request']['documents'].length > 0) {
          for (var u in value['data']['request']['documents']) {

            if (u['status'] != 2) {
              setState(() {
                idDocs.add(u['document_hashed_id']);
              });
            }
          }

          for (var u in value['data']['request']['documents']) {
            if (u['status'] == 2 && !idDocs.contains(u['document_hashed_id'])) {
              setState(() {
                idDocsRefused.add(u['document_hashed_id']);
              });
            }
            // print(u['hashed_id']);
          }
        }
      }
      print(idDocs);
      print(idDocsRefused);
    });

    getUserInfo().then((result) async {
      if(result['status']['code'] == 1005){
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.remove('connected');
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    Login()));
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return DialogScreen(
                color: Colors.amber,
                icon: Icons.pending_actions,
                title: 'Session',
                message: result['status']['message'],
                message2: '',
                buttonText: 'Close',
              );
            });

      }else if(result['status']['code'] == 200){
        print(result['data']['user']['hashed_id']);
        getLevelDocuments(result['data']['nextLevel']['hashed_id']).then((documentslevel){
          print('level document length = ' + documentslevel.length.toString());
          setState(() {
            levelDocumentLength = documentslevel.length;
          });
        });
        setState(() {
          nextLevel = result['data']['nextLevel']['hashed_id'];
          qrCode = result['data']['user']['qr_code'];

          canCreateShop = result['data']['user']['level']['can_create_shop'];
        });
        setState(() {
          currentLvlId = result['data']['user']['level']['id'];
        });

        if (result['data']['shop'] != null) {
          setState(() {
            haveShop = true;
            shopStatus = result['data']['shop']['status'];
            shopName = result['data']['shop']['title'];
          });
        }

        setState(() {
          level = result['data']['user']['level']['title'];
        });
      }

    });

    getUserLevelRequest().then((result) async {
      if(result['status']['code'] == 1005){
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.remove('connected');
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    Login()));
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return DialogScreen(
                color: Colors.amber,
                icon: Icons.sms_failed_outlined,
                title: 'Session',
                message: result['status']['message'],
                message2: '',
                buttonText: 'Close',
              );
            });

      }
      else if (result['status']['code'] == 200) {
        // print(result);
        setState(() {
          pending = result['data']['request']['level']['title'];
          print(result['data']['request']['level']['title']);
        });
      }
    });



    super.initState();
  }

  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return DefaultTabController(
        length: 1,
        child: Scaffold(
          backgroundColor: Color.fromRGBO(234, 239, 255, 3),
          appBar: AppBar(
            elevation: 0.0,
            automaticallyImplyLeading: false,
            iconTheme: IconThemeData(color: Colors.grey[600]),
            backgroundColor: Colors.grey[200],
            foregroundColor: Colors.grey[200],
            title: Text(
              'Settings',
              style: TextStyle(color: Colors.grey[800]),
            ),
            centerTitle: true,
          ),
          bottomNavigationBar: CustomBottomNavigationBar(
            iconList: [
              Icons.apps,
              Icons.money_outlined,
              Icons.qr_code_scanner_sharp,
              Icons.wallet_membership,
              Icons.account_circle_rounded,
            ],
            onChange: (val) {
              setState(() {
                _selectedItem = 4;
              });
            },
            defaultSelectedIndex: 4,
          ),
          body: SingleChildScrollView(
            child: SafeArea(
              child: Column(
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Container(
                      width: 380,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              child: InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Profile()));
                                },
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        SizedBox(
                                          width: 15,
                                        ),
                                        Icon(
                                          Icons.account_circle,
                                          color: Colors.grey[400],
                                        ),
                                        SizedBox(
                                          width: 30,
                                        ),
                                        Text(
                                          'Profile Settings',
                                          style: TextStyle(
                                            color: Colors.grey[700],
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                        SizedBox(
                                          width: 15,
                                        ),
                                      ],
                                    ),
                                    Icon(
                                      Icons.arrow_forward_ios,
                                      color: Colors.grey[400],
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Divider(
                            height: 20,
                            thickness: 0.03,
                            color: Colors.black,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              child: InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => Notifications(),
                                      ));
                                },
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        SizedBox(
                                          width: 15,
                                        ),
                                        Icon(
                                          Icons.notifications_active_rounded,
                                          color: Colors.grey[400],
                                        ),
                                        SizedBox(
                                          width: 30,
                                        ),
                                        Text(
                                          'Notifications',
                                          style: TextStyle(
                                            color: Colors.grey[700],
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                        SizedBox(
                                          width: 15,
                                        ),
                                      ],
                                    ),
                                    Icon(
                                      Icons.arrow_forward_ios,
                                      color: Colors.grey[400],
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Divider(
                            height: 20,
                            thickness: 0.03,
                            color: Colors.black,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    SizedBox(
                                      width: 15,
                                    ),
                                    Icon(
                                      Icons.elevator,
                                      color: Colors.grey[400],
                                    ),
                                    SizedBox(
                                      width: 30,
                                    ),
                                    Text(
                                      level,
                                      style: TextStyle(
                                        color: Colors.grey[700],
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 15,
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    pending != ''
                                        ? GestureDetector(
                                            onTap: () {
                                              print("all docs => " + allDocs.length.toString());
                                              print("done docs => " + idDocs.length.toString());

                                              if(levelDocumentLength > idDocs.length){
                                                getUserLevelRequest()
                                                    .then((value) {
                                                  if (value['status']['code'] ==
                                                      200) {
                                                    // print(value['data']
                                                    //     ['request']['id']);
                                                    Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                          builder: (context) =>
                                                              VerifieLevel(
                                                                lvlRequestId: value[
                                                                'data']
                                                                ['request']
                                                                ['hashed_id'],
                                                                level: nextLevel,
                                                              ),
                                                        ));
                                                  }
                                                });
                                              }
                                              else{
                                                showDialog(
                                                    context: context,
                                                    builder: (BuildContext context) {
                                                      return DialogScreen(
                                                        color: Colors.amber,
                                                        icon: Icons.pending_actions,
                                                        title: 'Request',
                                                        message: 'Level upgrade request is pending approval',
                                                        message2: '',
                                                        buttonText: 'Close',
                                                      );
                                                    });
                                              }
                                            },
                                            child: Container(
                                                margin:
                                                    EdgeInsets.only(left: 2),
                                                decoration: BoxDecoration(
                                                  color: Colors.amber,
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                ),
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.all(6.0),
                                                  child: Text(
                                                    '$pending Pending',
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ),
                                                )),
                                          )
                                        : Container(
                                            margin: EdgeInsets.only(left: 2),
                                            child: InkWell(
                                              onTap: () {},
                                              // ignore: deprecated_member_use
                                              child: RaisedButton(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          15.0),
                                                ),
                                                onPressed: () {
                                                  addLevelRequest(nextLevel)
                                                      .then((result) {
                                                    // print(result);
                                                    getUserLevelRequest()
                                                        .then((value) async {
                                                      if (value['status']
                                                              ['code'] ==
                                                          200) {
                                                        // print(value['data']['request']['id']);
                                                        Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder: (context) =>
                                                                  VerifieLevel(
                                                                lvlRequestId: value[
                                                                            'data']
                                                                        [
                                                                        'request']
                                                                    [
                                                                    'hashed_id'],
                                                                level:
                                                                    nextLevel,
                                                              ),
                                                            ));
                                                      }else if(value['status']['code'] == 1005){
                                                        SharedPreferences preferences = await SharedPreferences.getInstance();
                                                        preferences.remove('connected');
                                                        Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                                builder: (context) =>
                                                                    Login()));
                                                        showDialog(
                                                            context: context,
                                                            builder: (BuildContext context) {
                                                              return DialogScreen(
                                                                color: Colors.amber,
                                                                icon: Icons.pending_actions,
                                                                title: 'Session',
                                                                message: value['status']['message'],
                                                                message2: '',
                                                                buttonText: 'Close',
                                                              );
                                                            });
                                                      }
                                                    });
                                                  });
                                                },
                                                color: Colors.amber,
                                                textColor: Colors.white,
                                                child: Text(
                                                    "Validate next level",
                                                    style: TextStyle(
                                                        fontSize: height / 70,
                                                        color: Colors.white)),
                                              ),
                                            ),
                                          ),
                                    Icon(
                                      Icons.arrow_forward_ios,
                                      color: Colors.grey[400],
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                          Divider(
                            height: 20,
                            thickness: 0.03,
                            color: Colors.black,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              child: InkWell(
                                onTap: (){
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                           Verifications(),
                                      ));
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        SizedBox(
                                          width: 15,
                                        ),
                                        Icon(
                                          Icons.domain_verification,
                                          color: Colors.grey[400],
                                        ),
                                        SizedBox(
                                          width: 30,
                                        ),
                                        Text(
                                          'Verifications',
                                          style: TextStyle(
                                            color: Colors.grey[700],
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                        SizedBox(
                                          width: 15,
                                        ),
                                      ],
                                    ),
                                    Icon(
                                      Icons.arrow_forward_ios,
                                      color: Colors.grey[400],
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Container(
                      width: 380,
                      height: 70,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20)),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                SizedBox(
                                  width: 15,
                                ),
                                Icon(
                                  Icons.shopping_cart_rounded,
                                  color: Colors.grey[400],
                                ),
                                SizedBox(
                                  width: 30,
                                ),
                                Text(
                                  'Shop',
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.grey[700]),
                                ),
                                SizedBox(
                                  width: 15,
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                canCreateShop != 0 && haveShop == false
                                    ? GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    CreateShop(),
                                              ));
                                        },
                                        child: Container(
                                            margin: EdgeInsets.only(left: 2),
                                            decoration: BoxDecoration(
                                              color: Colors.lightBlue,
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                            ),
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Text(
                                                'Create Shop',
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                            )),
                                      )
                                    : canCreateShop == 0 && haveShop == false
                                        ? Container()
                                        : haveShop == true && shopStatus == 0
                                            ? Container(
                                                margin:
                                                    EdgeInsets.only(left: 2),
                                                decoration: BoxDecoration(
                                                  color: Colors.amber,
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                ),
                                                child: InkWell(
                                                  onTap: (){
                                                    showDialog(
                                                        context: context,
                                                        builder: (BuildContext context) {
                                                          return DialogScreen(
                                                            color: Colors.amber,
                                                            icon: Icons.pending_actions,
                                                            title: 'Request',
                                                            message: 'You will receive a notification once approved or rejected',
                                                            message2: '',
                                                            buttonText: 'Close',
                                                          );
                                                        });
                                                  },
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(8.0),
                                                    child: Text(
                                                      'Pending',
                                                      style: TextStyle(
                                                          color: Colors.white),
                                                    ),
                                                  ),
                                                ))
                                            : GestureDetector(
                                                onTap: () {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                        builder: (context) =>
                                                            EditShop(),
                                                      ));
                                                },
                                                child: Container(
                                                    margin: EdgeInsets.only(
                                                        left: 2),
                                                    decoration: BoxDecoration(
                                                      color: Colors.green,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              20),
                                                    ),
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              8.0),
                                                      child: Text(
                                                        shopName,
                                                        style: TextStyle(
                                                            color:
                                                                Colors.white),
                                                      ),
                                                    )),
                                              ),
                                Icon(
                                  Icons.arrow_forward_ios,
                                  color: Colors.grey[400],
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Container(
                      width: 380,
                      height: 70,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20)),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          child: InkWell(
                            onTap: () {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return Language();
                                  });
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    SizedBox(
                                      width: 15,
                                    ),
                                    Icon(
                                      Icons.language,
                                      color: Colors.grey[400],
                                    ),
                                    SizedBox(
                                      width: 30,
                                    ),
                                    Text(
                                      'Language',
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.grey[700]),
                                    ),
                                    SizedBox(
                                      width: 15,
                                    ),
                                  ],
                                ),
                                Icon(
                                  Icons.arrow_forward_ios,
                                  color: Colors.grey[400],
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Container(
                      width: 380,
                      height: 70,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20)),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          child: InkWell(
                            onTap: () {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return QrCode(data: qrCode,);
                                  });
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    SizedBox(
                                      width: 15,
                                    ),
                                    Icon(
                                      Icons.qr_code_scanner_sharp,
                                      color: Colors.grey[400],
                                    ),
                                    SizedBox(
                                      width: 30,
                                    ),
                                    Text(
                                      'My QR Code',
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.grey[700]),
                                    ),
                                    SizedBox(
                                      width: 15,
                                    ),
                                  ],
                                ),
                                Icon(
                                  Icons.arrow_forward_ios,
                                  color: Colors.grey[400],
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Container(
                      width: 380,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              child: InkWell(
                                onTap: () {
                                  getTerms().then((terms) async {
                                    if(terms['status']['code'] == 200){
                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return Terms(
                                                terms: terms['data']['terms']);
                                          });
                                    }else if(terms['status']['code'] == 1005){
                                      SharedPreferences preferences = await SharedPreferences.getInstance();
                                      preferences.remove('connected');
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  Login()));
                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return DialogScreen(
                                              color: Colors.amber,
                                              icon: Icons.pending_actions,
                                              title: 'Session',
                                              message: terms['status']['message'],
                                              message2: '',
                                              buttonText: 'Close',
                                            );
                                          });
                                    }
                                    print(terms);
                                  });
                                },
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        SizedBox(
                                          width: 15,
                                        ),
                                        Icon(
                                          Icons.text_snippet,
                                          color: Colors.grey[400],
                                        ),
                                        SizedBox(
                                          width: 30,
                                        ),
                                        Text(
                                          'Terms and conditions',
                                          style: TextStyle(
                                            color: Colors.grey[700],
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                        SizedBox(
                                          width: 15,
                                        ),
                                      ],
                                    ),
                                    Icon(
                                      Icons.arrow_forward_ios,
                                      color: Colors.grey[400],
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Divider(
                            height: 20,
                            thickness: 0.03,
                            color: Colors.black,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              child: InkWell(
                                onTap: () {
                                  getPrivacy().then((terms) async {
                                    if(terms['status']['code'] == 200){
                                      print(terms);
                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return Privacy(
                                                terms: terms['data']['privacy']);
                                          });
                                    }else if(terms['status']['code'] == 1005){
                                      SharedPreferences preferences = await SharedPreferences.getInstance();
                                      preferences.remove('connected');
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  Login()));
                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return DialogScreen(
                                              color: Colors.amber,
                                              icon: Icons.pending_actions,
                                              title: 'Session',
                                              message: terms['status']['message'],
                                              message2: '',
                                              buttonText: 'Close',
                                            );
                                          });
                                    }

                                  });
                                },
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        SizedBox(
                                          width: 15,
                                        ),
                                        Icon(
                                          Icons.notifications_active_rounded,
                                          color: Colors.grey[400],
                                        ),
                                        SizedBox(
                                          width: 30,
                                        ),
                                        Text(
                                          'Privacy and policy',
                                          style: TextStyle(
                                            color: Colors.grey[700],
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                        SizedBox(
                                          width: 15,
                                        ),
                                      ],
                                    ),
                                    Icon(
                                      Icons.arrow_forward_ios,
                                      color: Colors.grey[400],
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    height: height / 15,
                    width: width / 1.15,
                    child: InkWell(
                      onTap: () {},
                      // ignore: deprecated_member_use
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        onPressed: () async {
                          SharedPreferences preferences =
                              await SharedPreferences
                              .getInstance();
                          preferences.remove('connected');
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    Login(),
                              ));
                        },
                        color: Colors.amber,
                        textColor: Colors.white,
                        child: Text("Disconnect".toUpperCase(),
                            style: TextStyle(
                                fontSize: height / 46, color: Colors.white)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: height / 15,
                    width: width / 1.15,
                    child: InkWell(
                      onTap: () {},
                      // ignore: deprecated_member_use
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        onPressed: () async {
                          SharedPreferences preferences =
                              await SharedPreferences.getInstance();
                          String userId = preferences.getString('idUser');
                          String id = userId;
                          deleteUser(id).then((result) async {
                            SharedPreferences preferences =
                                await SharedPreferences
                                .getInstance();
                            preferences.remove('connected');
                            print(result);
                            preferences.remove('idUser');
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => Login(),
                                ));
                          });
                        },
                        color: Colors.red,
                        textColor: Colors.white,
                        child: Text("Delete my Account".toUpperCase(),
                            style: TextStyle(
                                fontSize: height / 46, color: Colors.white)),
                      ),
                    ),
                  ),
                  SizedBox(height: 10,)
                ],
              ),
            ),
          ),
        ));
  }
}
