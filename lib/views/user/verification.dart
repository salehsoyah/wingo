import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/authController.dart';
import 'package:flutter_app/controllers/userController.dart';
import 'package:flutter_app/views/dashboard/customBottomNavigationBar.dart';
import 'package:flutter_app/views/dashboard/dashboard.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/shops/createShop.dart';
import 'package:flutter_app/views/shops/editShop.dart';
import 'package:flutter_app/views/signinScreens/createaccount.dart';
import 'package:flutter_app/views/signinScreens/forgotPassword.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:flutter_app/views/signinScreens/otp_received.dart';
import 'package:flutter_app/views/signinScreens/passwordrecovery.dart';
import 'package:flutter_app/views/user/verifieLevel.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Verifications extends StatefulWidget {
  @override
  _VerificationsState createState() => _VerificationsState();
}

class _VerificationsState extends State<Verifications> {
  final _emailController = TextEditingController();
  final _nameController = TextEditingController();
  final _phoneController = TextEditingController();
  final _passwordController = TextEditingController();

  Future userInfo;
  int _selectedItem;

  @override
  void initState() {
    getUserInfo().then((user) async {
      if(user['status']['code'] == 200){
        setState((){
          userInfo = getUserInfo();
        });
      }else if(user['status']['code'] == 1005){
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.remove('connected');
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    Login()));
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return DialogScreen(
                color: Colors.amber,
                icon: Icons.pending_actions,
                title: 'Session',
                message: user['status']['message'],
                message2: '',
                buttonText: 'Close',
              );
            });
      }

    });

    super.initState();
  }

  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      bottomNavigationBar: CustomBottomNavigationBar(
        iconList: [
          Icons.apps,
          Icons.money_outlined,
          Icons.qr_code_scanner_sharp,
          Icons.wallet_membership,
          Icons.account_circle_rounded,
        ],
        onChange: (val) {
          setState(() {
            _selectedItem = 4;
          });
        },
        defaultSelectedIndex: 4,
      ),
      backgroundColor: Color.fromRGBO(234, 239, 255, 3),
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: [
              FutureBuilder(
                future: userInfo,
                builder:
                    (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                  if (!snapshot.hasData) {
                    return Container(
                      height: height,
                      child: SpinKitFadingCircle(
                        itemBuilder: (BuildContext context, int index) {
                          return DecoratedBox(
                            decoration: BoxDecoration(
                              color: index.isEven ? Colors.yellow : Colors.blue,
                            ),
                          );
                        },
                      ),
                    );
                  } else {
                    return Stack(
                      alignment: Alignment.bottomCenter,
                      children: [
                        Container(
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.pop(context);
                                    },
                                    child: Container(
                                        margin: EdgeInsets.only(
                                            top: height / 15, left: width / 20),
                                        child: Icon(
                                          Icons.close,
                                          size: height / 35,
                                        )),
                                  ),
                                ],
                              ), //header
                              SizedBox(
                                height: height / 20,
                              ),
                              Container(
                                width: width,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(40),
                                      topRight: Radius.circular(40),
                                    )),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    SizedBox(height: height / 10),
                                    Container(
                                      child: Text(
                                        'Verifications',
                                        style: TextStyle(
                                          fontFamily: 'DMSans-Bold',
                                          fontSize: height * 0.035,
                                          color: const Color(0xff172b4d),
                                        ),
                                        textHeightBehavior: TextHeightBehavior(
                                            applyHeightToFirstAscent: false),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    SizedBox(
                                      height: height / 100,
                                    ),
                                    Container(
                                      child: Text(
                                        'Consult phone and email verifications',
                                        style: TextStyle(
                                          fontFamily: 'DMSans-Regular',
                                          fontSize: height * 0.023,
                                          color: const Color(0xff7a869a),
                                          letterSpacing: -0.3999999961853027,
                                          height: 1.7142857142857142,
                                        ),
                                        textHeightBehavior: TextHeightBehavior(
                                            applyHeightToFirstAscent: false),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    SizedBox(
                                      height: height / 14,
                                    ),
                                    Container(
                                      width: width / 1.1,
                                      height: height / 4,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(25),
                                          color: Colors.greenAccent),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Icon(Icons.phone_android),
                                                SizedBox(
                                                  width: width / 50,
                                                ),
                                                Text(
                                                  "Phone status",
                                                  style: TextStyle(
                                                      fontSize: height / 30),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: height / 50,
                                            ),
                                            Text(
                                              snapshot.data['data']['user']
                                                          ['phone_status'] ==
                                                      1
                                                  ? 'Valid'
                                                  : 'Pending',
                                              style: TextStyle(
                                                  fontSize: height / 35),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: height / 50,
                                    ),
                                    Container(
                                      width: width / 1.1,
                                      height: height / 4,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(25),
                                          color: snapshot.data['data']['user']
                                                      ['email_status'] ==
                                                  1
                                              ? Colors.greenAccent
                                              : Colors.amber),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Icon(Icons.mail),
                                                SizedBox(
                                                  width: width / 50,
                                                ),
                                                Text(
                                                  "Email status",
                                                  style: TextStyle(
                                                      fontSize: height / 30),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: height / 50,
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Text(
                                                  snapshot.data['data']['user'][
                                                              'email_status'] ==
                                                          1
                                                      ? 'Valid'
                                                      : 'Pending validation /',
                                                  style: TextStyle(
                                                      fontSize: height / 35),
                                                ),
                                                SizedBox(width: width / 50,),
                                                snapshot.data['data']['user'][
                                                'email_status'] ==
                                                    0 ? GestureDetector(
                                                  onTap: () {
                                                    resendVerificationMail(snapshot.data['data']['user']['email']).then((result){
                                                      if(result['status']['code'] == 200){
                                                        showDialog(
                                                            context: context,
                                                            builder: (BuildContext context) {
                                                              return DialogScreen(
                                                                color: Colors.amber,
                                                                icon: Icons.check_circle,
                                                                title: 'Success',
                                                                message: 'Email verification mail sent',
                                                                message2 :'',
                                                                buttonText: 'Close',
                                                              );
                                                            });
                                                      }else{
                                                        showDialog(
                                                            context: context,
                                                            builder: (BuildContext context) {
                                                              return DialogScreen(
                                                                color: Colors.red,
                                                                icon: Icons.check_circle,
                                                                title: 'Error',
                                                                message: result['message'],
                                                                message2 :'',
                                                                buttonText: 'Close',
                                                              );
                                                            });
                                                      }
                                                    });
                                                  },
                                                  child: Container(
                                                    child: Text(
                                                      'Resend Code',
                                                      style: TextStyle(
                                                        fontFamily: 'DMSans-Regular',
                                                        fontSize: height / 45,
                                                        color: Colors.white,
                                                        letterSpacing: -0.3999999961853027,
                                                        height: 1.7142857142857142,
                                                      ),
                                                      textHeightBehavior: TextHeightBehavior(
                                                          applyHeightToFirstAscent: false),
                                                      textAlign: TextAlign.center,
                                                    ),
                                                  ),
                                                ) : Container(),

                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: height / 50,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
