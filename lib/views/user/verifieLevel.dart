import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/authController.dart';
import 'package:flutter_app/controllers/userController.dart';
import 'package:flutter_app/models/UploadedDocument.dart';
import 'package:flutter_app/views/dashboard/customBottomNavigationBar.dart';
import 'package:flutter_app/views/dashboard/dashboard.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/signinScreens/createaccount.dart';
import 'package:flutter_app/views/signinScreens/forgotPassword.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:flutter_app/views/signinScreens/otp_received.dart';
import 'package:flutter_app/views/signinScreens/passwordrecovery.dart';
import 'package:flutter_app/views/user/profile.dart';
import 'package:flutter_app/views/user/profileOptions.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class VerifieLevel extends StatefulWidget {
  @override
  _VerifieLevelState createState() => _VerifieLevelState();
  String lvlRequestId;
  String level;

  VerifieLevel({Key key, @required this.lvlRequestId, @required this.level})
      : super(key: key);
}

class _VerifieLevelState extends State<VerifieLevel> {
  final _emailController = TextEditingController();
  final _nameController = TextEditingController();
  final _phoneController = TextEditingController();

  int _selectedItem;

  File imageFile;
  String photo = "";
  File _image;
  final picker = ImagePicker();
  List<String> idDocsRefused = [];
  List<String> idDocs = [];
  List<String> doneDocs = [];
  List<UploadDocument> uploadedDocuments = [];
  bool loading = false;
  int documentNb = 0;

  Future getImage() async {
    // ignore: deprecated_member_use
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      imageFile = File(pickedFile.path);
    });
  }

  static Future image2Base64(String path) async {
    File file = new File(path);
    List<int> imageBytes = await file.readAsBytes();
    return base64Encode(imageBytes);
  }

  Future levelDocuments;

  @override
  void initState() {
    getUserInfo().then((user) async {
      if(user['status']['code'] == 200){
        setState((){
          levelDocuments = getLevelDocuments(widget.level);
        });

        getUserLevelRequest().then((value) {
          // print(value['data']['request']['documents']);
          if (value['data']['request']['documents'].length > 0) {
            for (var u in value['data']['request']['documents']) {
              if (u['status'] != 2) {
                setState(() {
                  idDocs.add(u['document_hashed_id']);
                });
              }
            }

            for (var u in value['data']['request']['documents']) {
              if (u['status'] == 2 && !idDocs.contains(u['document_hashed_id'])) {
                setState(() {
                  idDocsRefused.add(u['document_hashed_id']);
                });
              }
              // print(u['hashed_id']);
            }
          }
          print(idDocs);
          print(idDocsRefused);
        });
      }else if(user['status']['code'] == 1005){
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.remove('connected');
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    Login()));
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return DialogScreen(
                color: Colors.amber,
                icon: Icons.pending_actions,
                title: 'Session',
                message: user['status']['message'],
                message2: '',
                buttonText: 'Close',
              );
            });
      }
    });
    super.initState();
  }

  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      bottomNavigationBar: CustomBottomNavigationBar(
        iconList: [
          Icons.apps,
          Icons.money_outlined,
          Icons.qr_code_scanner_sharp,
          Icons.wallet_membership,
          Icons.account_circle_rounded,
        ],
        onChange: (val) {
          setState(() {
            _selectedItem = 4;
          });
        },
        defaultSelectedIndex: 4,
      ),
      backgroundColor: Color.fromRGBO(234, 239, 255, 3),
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: SafeArea(
          child: loading == false
              ? Stack(
                  alignment: Alignment.center,
                  children: [
                    Column(
                      children: [
                        Container(
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                      margin: EdgeInsets.only(
                                          top: height / 15, left: width / 20),
                                      child: Icon(
                                        Icons.close,
                                        size: height / 35,
                                      )),
                                ],
                              ), //header
                              SizedBox(
                                height: height / 14,
                              ),
                              Container(
                                width: width,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(40),
                                      topRight: Radius.circular(40),
                                    )),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    SizedBox(height: height / 10),
                                    Container(
                                      child: Text(
                                        'Verify Level',
                                        style: TextStyle(
                                          fontFamily: 'DMSans-Bold',
                                          fontSize: height * 0.035,
                                          color: const Color(0xff172b4d),
                                        ),
                                        textHeightBehavior: TextHeightBehavior(
                                            applyHeightToFirstAscent: false),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    SizedBox(
                                      height: height / 100,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 13.0, right: 13.0),
                                      child: Container(
                                        child: Text(
                                          'Please upload requested documents to proceed with your inquiry!',
                                          style: TextStyle(
                                            fontFamily: 'DMSans-Regular',
                                            fontSize: height * 0.023,
                                            color: const Color(0xff7a869a),
                                            letterSpacing: -0.3999999961853027,
                                            height: 1.7142857142857142,
                                          ),
                                          textHeightBehavior:
                                              TextHeightBehavior(
                                                  applyHeightToFirstAscent:
                                                      false),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: height / 10,
                                    ),
                                    FutureBuilder(
                                      future: levelDocuments,
                                      builder: (BuildContext context,
                                          AsyncSnapshot<dynamic> snapshot) {
                                        if (snapshot.connectionState ==
                                            ConnectionState.done) {
                                          if (snapshot.hasData &&
                                              snapshot.data.length > 0) {
                                            return ListView.builder(
                                              scrollDirection: Axis.vertical,
                                              shrinkWrap: true,
                                              itemCount: snapshot.data.length,
                                              itemBuilder: (context, index) {
                                                return Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 25,
                                                          right: 15.0,
                                                          bottom: 10),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Row(
                                                        children: [
                                                          Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      right:
                                                                          10.0),
                                                              child: Container(
                                                                width:
                                                                    width / 2.2,
                                                                decoration:
                                                                    BoxDecoration(
                                                                  color: Colors
                                                                      .amber,
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              20),
                                                                ),
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .all(
                                                                          8.0),
                                                                  child: Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .spaceBetween,
                                                                    children: [
                                                                      Text(
                                                                        (index + 1).toString() +
                                                                            " - " +
                                                                            snapshot.data[index].title,
                                                                        style: TextStyle(
                                                                            fontSize:
                                                                                15,
                                                                            color:
                                                                                Colors.white),
                                                                      ),
                                                                      SizedBox(
                                                                        width:
                                                                            50,
                                                                      ),
                                                                      (idDocs.contains(snapshot.data[index].id) &&
                                                                              !idDocsRefused.contains(snapshot.data[index].id))
                                                                          ? Container()
                                                                          : InkWell(
                                                                              onTap: () async {
                                                                                getImage().then((file) {
                                                                                  if (imageFile != null) {
                                                                                    image2Base64(imageFile.path).then((value) async {
                                                                                      UploadDocument uploadDocument = UploadDocument(snapshot.data[index].id, "data:image/png;base64,$value");

                                                                                      uploadedDocuments.add(uploadDocument);
                                                                                      setState(() {
                                                                                        doneDocs.add(uploadDocument.id);
                                                                                      });
                                                                                      print(uploadDocument);
                                                                                      print(uploadedDocuments);

                                                                                      // print(widget.lvlRequestId);
                                                                                    });
                                                                                  }
                                                                                });
                                                                                // ignore: deprecated_member_use
                                                                              },
                                                                              child: doneDocs.contains(snapshot.data[index].id) ? Container() : Icon(Icons.edit)),
                                                                    ],
                                                                  ),
                                                                ),
                                                              )),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                );
                                              },
                                            );
                                          } else {
                                            return Text('No documents');
                                          }
                                        } else {
                                          return Center(
                                              child:
                                                  CircularProgressIndicator());
                                        }
                                      },
                                    ),
                                    SizedBox(
                                      height: height / 5.2,
                                    ),
                                    Container(
                                      height: height / 15,
                                      width: width / 1.15,
                                      child: InkWell(
                                        onTap: () {},
                                        // ignore: deprecated_member_use
                                        child: RaisedButton(
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                          ),
                                          onPressed: () async {
                                            print(uploadedDocuments);
                                            setState(() {
                                              loading = true;
                                            });
                                            SharedPreferences preferences = await SharedPreferences.getInstance();
                                            String userId = preferences.getString('idUser');
                                            String id = userId;
                                            for(var u in uploadedDocuments){
                                              uploadVerification(widget.lvlRequestId, u.image, id, u.id).then((document) async {
                                                setState(() {
                                                  loading = false;
                                                });
                                                print(document);
                                                if(document['status']['code'] != 200){
                                                  showDialog(
                                                      context: context,
                                                      builder: (BuildContext context) {
                                                        return DialogScreen(
                                                          color: Colors.red,
                                                          icon: Icons.error_outline,
                                                          title: 'Error',
                                                          message: 'Error uploading document',
                                                          message2: '',
                                                          buttonText: 'Close',
                                                        );
                                                      });
                                                }
                                                else if(document['status']['code'] == 1005){
                                                  SharedPreferences preferences = await SharedPreferences.getInstance();
                                                  preferences.remove('connected');
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              Login()));
                                                  showDialog(
                                                      context: context,
                                                      builder: (BuildContext context) {
                                                        return DialogScreen(
                                                          color: Colors.amber,
                                                          icon: Icons.pending_actions,
                                                          title: 'Session',
                                                          message: document['status']['message'],
                                                          message2: '',
                                                          buttonText: 'Close',
                                                        );
                                                      });
                                                }
                                                else{
                                                  documentNb = documentNb + 1;
                                                }
                                                print(documentNb);
                                                print(uploadedDocuments.length);
                                                if(documentNb == uploadedDocuments.length){
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                        builder: (context) =>
                                                            ProfileOptions(),
                                                      ));
                                                }
                                              });
                                            }
                                          },
                                          color: Colors.amber,
                                          textColor: Colors.white,
                                          child: Text(
                                              "Upload documents".toUpperCase(),
                                              style: TextStyle(
                                                  fontSize: height / 46,
                                                  color: Colors.white)),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: height / 10,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Positioned(
                      top: height / 10.1,
                      child: Container(
                        width: height / 7.5,
                        child: Image.asset(
                          "assets/images/profilepic.png",
                          fit: BoxFit.fill,
                        ),
                      ),
                    )
                  ],
                )
              : Container(
                  height: height,
                  child: SpinKitFadingCircle(
                    itemBuilder: (BuildContext context, int index) {
                      return DecoratedBox(
                        decoration: BoxDecoration(
                          color: index.isEven ? Colors.yellow : Colors.blue,
                        ),
                      );
                    },
                  ),
                ),
        ),
      ),
    );
  }
}
