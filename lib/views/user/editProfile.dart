import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/authController.dart';
import 'package:flutter_app/urls/urls.dart';
import 'package:get/get_connect/http/src/response/response.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';


class SettingUi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Settings",
      home: EditProfile(),
    );
  }
}

class EditProfile extends StatefulWidget {
  final List<String> data;

  EditProfile({List<String> data}) : data = data ?? <String>[];
  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  bool showPassword = false;
  final _phoneController = TextEditingController();
  final _passwordController = TextEditingController();
  final _firstNameController = TextEditingController();
  final _billingAdressController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _emailController = TextEditingController();

  File imageFile;
  String photo = "";

  File _image;
  final picker = ImagePicker();

  Future getImage() async {
    // ignore: deprecated_member_use
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      imageFile = File(pickedFile.path);
    });
  }

  static Future image2Base64(String path) async {
    File file = new File(path);
    List<int> imageBytes = await file.readAsBytes();
    return base64Encode(imageBytes);
  }

  @override
  void initState() {
    getUserDetail('nada@nada').then((result) {
      setState(() {
        _phoneController.text = result['data']['user']['phone'].toString();
        if(result['data']['user']['photo'] != null){
          setState(() {
            photo = result['data']['user']['photo'];
          });
        }
        _firstNameController.text = result['data']['user']['first_name'];
        _lastNameController.text = result['data']['user']['last_name'];
        _passwordController.text = result['data']['user']['passsword'];
        _billingAdressController.text =
            result['data']['user']['billing_address'];
        _emailController.text = result['data']['user']['email'];
      });
      //print(result);
    });

    print("ala");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        elevation: 1,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.lightBlue,
          ),
          onPressed: () {},
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.settings,
              color: Colors.lightBlue,
            ),
            onPressed: () {},
          ),
        ],
      ),
      body: Container(
        padding: EdgeInsets.only(left: 15, top: 20, right: 15),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(40),
              topRight: Radius.circular(40),
            )),
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: ListView(
            children: [
              Text(
                "Edit Profile",
                style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.w500,
                    color: Colors.lightBlue),
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                child: Stack(
                  children: [
                    Container(
                      width: 130,
                      height: 130,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                              width: 4,
                              color: Theme.of(context).scaffoldBackgroundColor),
                          boxShadow: [
                            BoxShadow(
                                spreadRadius: 2,
                                blurRadius: 10,
                                color: Colors.black.withOpacity(0.1),
                                offset: Offset(0, 10)),
                          ],
                      ),
                      child: InkWell(
                        onTap: () async {
                          // ignore: deprecated_member_use
                          getImage();
                          if (imageFile != null) {
                            image2Base64(imageFile.path).then((value) {
                              log("data:image/png;base64,$value");
                              uploadLogo("data:image/png;base64,$value")
                                  .then((result) {
                                print(result);
                                print("photoo");
                                setState(() {
                                  photo = result['data']['photoPath'];

                                });
                              });
                            });
                            // uploadLogo(img64).then((result){
                            //   print(result);
                            // });
                          }
                        },
                        child: CircleAvatar(
                          backgroundColor: Colors.black,
                          radius: 40.0,
                          child: CircleAvatar(
                            radius: 38.0,
                            child: ClipOval(
                              child: (imageFile != null)
                                  ? Image.file(imageFile)
                                  : Image(image: AssetImage('assets/images/profilepic.png'),)
                            ),
                            backgroundColor: Colors.white,
                          ),
                        ),
                        /*child: Text(
                          "Choose your company logo",
                          style: TextStyle(fontSize: 13),
                        ),*/
                      ),
                    ),

                    /*Container(
                      width: 130,
                      height: 130,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                              width: 4,
                              color: Theme.of(context).scaffoldBackgroundColor),
                          boxShadow: [
                            BoxShadow(
                                spreadRadius: 2,
                                blurRadius: 10,
                                color: Colors.black.withOpacity(0.1),
                                offset: Offset(0, 10)),
                          ],
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage("assets/images/profilepic.png"),
                          )),
                    ),*/
                    Positioned(
                        bottom: 0,
                        right: 0,
                        child: Container(
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                  width: 4,
                                  color: Theme.of(context)
                                      .scaffoldBackgroundColor),
                              color: Colors.white),
                          child: Icon(
                            Icons.edit,
                            color: Colors.lightBlue,
                          ),
                        ))
                  ],
                ),

              ),

              SizedBox(
                height: 28,
              ),
              /*TextFormField(
                controller: _firstNameController,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.only(bottom: 5),
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  hintText: '',
                  hintStyle: TextStyle(
                      fontSize: 16, fontWeight: FontWeight.bold,
                      color: Colors.black
                  ),
                ),
              ),*/
              Padding(
                padding: const EdgeInsets.only(bottom: 35.0),
                child: TextField(
                  controller: _firstNameController,
                  decoration: InputDecoration(
                    suffixIcon: IconButton(
                      icon: Icon(Icons.drive_file_rename_outline),
                      color: Colors.lightBlue,
                    ),
                    contentPadding: EdgeInsets.only(bottom: 5),
                    labelText: 'first name',
                    floatingLabelBehavior: FloatingLabelBehavior.always,
                    hintText: _firstNameController.text,
                    hintStyle: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 35.0),
                child: TextField(
                  controller: _lastNameController,
                  decoration: InputDecoration(
                    suffixIcon: IconButton(
                      icon: Icon(Icons.drive_file_rename_outline),
                      color: Colors.lightBlue,
                    ),
                    contentPadding: EdgeInsets.only(bottom: 5),
                    labelText: 'Last name',
                    floatingLabelBehavior: FloatingLabelBehavior.always,
                    hintText: _lastNameController.text,
                    hintStyle: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 35.0),
                child: TextField(
                  controller: _phoneController,
                  decoration: InputDecoration(
                    suffixIcon: IconButton(
                      icon: Icon(Icons.phone),
                      color: Colors.lightBlue,
                    ),
                    contentPadding: EdgeInsets.only(bottom: 5),
                    labelText: 'Phone',
                    floatingLabelBehavior: FloatingLabelBehavior.always,
                    hintText: _phoneController.text,
                    hintStyle: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 35.0),
                child: TextField(
                  controller: _billingAdressController,
                  decoration: InputDecoration(
                    suffixIcon: IconButton(
                      icon: Icon(Icons.home_filled),
                      color: Colors.lightBlue,
                    ),
                    contentPadding: EdgeInsets.only(bottom: 5),
                    labelText: 'Billing address',
                    floatingLabelBehavior: FloatingLabelBehavior.always,
                    hintText: _billingAdressController.text,
                    hintStyle: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 35.0),
                child: TextField(
                  controller: _emailController,
                  decoration: InputDecoration(
                    suffixIcon: IconButton(
                      icon: Icon(Icons.email_outlined),
                      color: Colors.lightBlue,
                    ),
                    contentPadding: EdgeInsets.only(bottom: 5),
                    labelText: 'Email ',
                    floatingLabelBehavior: FloatingLabelBehavior.always,
                    hintText: _emailController.text,
                    hintStyle: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                ),
              ),

              //buildTextField("Password", _passwordController.text, true),
              SizedBox(
                height: MediaQuery.of(context).size.height / 28,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  RaisedButton(
                    color: Colors.white,
                    padding: EdgeInsets.symmetric(horizontal: 50),
                    elevation: 2,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    onPressed: () {},
                    child: Text(
                      "Cancel",
                      style: TextStyle(
                          fontSize: 14,
                          letterSpacing: 2.0,
                          color: Colors.black),
                    ),
                  ),
                  RaisedButton(
                    color: Colors.white,
                    padding: EdgeInsets.symmetric(horizontal: 50),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    elevation: 2,
                    onPressed: () async {

                    },
                    child: Text(
                      "Save",
                      style: TextStyle(
                          fontSize: 14,
                          letterSpacing: 2.0,
                          color: Colors.lightBlue),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  /* Widget buildTextField(String labelText, String placeholder,
      bool isPasswordField) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 35.0),
      child: TextField(
      controller: _passwordController,
        obscureText: isPasswordField ? showPassword : false,
        decoration: InputDecoration(
          suffixIcon: isPasswordField ? IconButton(
            onPressed: () {
              setState(() {
                showPassword = !showPassword;
              });
            },
            icon: Icon(Icons.remove_red_eye),
            color: Colors.lightBlue,
          ) : null,
          contentPadding: EdgeInsets.only(bottom: 5),
          labelText: labelText,
          floatingLabelBehavior: FloatingLabelBehavior.always,
          hintText: placeholder,
          hintStyle: TextStyle(
              fontSize: 16, fontWeight: FontWeight.bold,
              color: Colors.black
          ),
        ),
      ),
    );
  }*/
}
