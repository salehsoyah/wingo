import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/authController.dart';
import 'package:flutter_app/controllers/userController.dart';
import 'package:flutter_app/views/dashboard/customBottomNavigationBar.dart';
import 'package:flutter_app/views/dashboard/dashboard.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/shops/createShop.dart';
import 'package:flutter_app/views/shops/editShop.dart';
import 'package:flutter_app/views/signinScreens/createaccount.dart';
import 'package:flutter_app/views/signinScreens/forgotPassword.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:flutter_app/views/signinScreens/otp_received.dart';
import 'package:flutter_app/views/signinScreens/passwordrecovery.dart';
import 'package:flutter_app/views/user/verifieLevel.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final _emailController = TextEditingController();
  final _nameController = TextEditingController();
  String phone;
  final _passwordController = TextEditingController();
  final _phoneController = TextEditingController();
  String level = '';
  int lvlStatus = 0;
  int _selectedItem;
  String pending = '';
  int currentLvlId = 0;
  int canCreateShop = 0;
  String nextLevel;
  DateTime selectedDate = DateTime.now();
  DateTime birthday;
  String dropdownValue = 'Male';
  bool loading = false;

  final _formKey = GlobalKey<FormState>();
  Future userInfo;

  @override
  void initState() {

    getUserInfo().then((result) async {
      print(result['data']);

      if(result['status']['code'] == 200){
        userInfo = getUserInfo();

        setState(() {
          _nameController.text = result['data']['user']['first_name'];
          nextLevel = result['data']['nextLevel']['hashed_id'];
          _phoneController.text = result['data']['user']['phone'].toString();
          _emailController.text = result['data']['user']['email'];
          if(result['data']['user']['birthday'] != null){
            birthday = DateTime.parse(result['data']['user']['birthday']);
          }
          canCreateShop = result['data']['user']['level']['can_create_shop'];
        });
        setState(() {
          currentLvlId = result['data']['user']['level']['id'];
        });

        setState(() {
          level = result['data']['user']['level']['title'];
        });
      }else if(result['status']['code'] == 1005){
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.remove('connected');
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    Login()));
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return DialogScreen(
                color: Colors.amber,
                icon: Icons.pending_actions,
                title: 'Session',
                message: result['status']['message'],
                message2: '',
                buttonText: 'Close',
              );
            });
      }
    });

    getUserLevelRequest().then((result) async {
      if (result['status']['code'] == 200) {
        // print(result);
        setState(() {
          pending = result['data']['request']['level']['title'];
        });
      }else if(result['status']['code'] == 1005){
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.remove('connected');
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    Login()));
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return DialogScreen(
                color: Colors.amber,
                icon: Icons.pending_actions,
                title: 'Session',
                message: result['status']['message'],
                message2: '',
                buttonText: 'Close',
              );
            });
      }
    });

    super.initState();
  }

  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      bottomNavigationBar: CustomBottomNavigationBar(
        iconList: [
          Icons.apps,
          Icons.money_outlined,
          Icons.qr_code_scanner_sharp,
          Icons.wallet_membership,
          Icons.account_circle_rounded,
        ],
        onChange: (val) {
          setState(() {
            _selectedItem = 4;
          });
        },
        defaultSelectedIndex: 4,
      ),
      backgroundColor: Color.fromRGBO(234, 239, 255, 3),
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: [
              FutureBuilder(
                future: userInfo,
                builder:
                    (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                  if (!snapshot.hasData) {
                    return Container(
                      height: height,
                      child: SpinKitFadingCircle(
                        itemBuilder: (BuildContext context, int index) {
                          return DecoratedBox(
                            decoration: BoxDecoration(
                              color: index.isEven ? Colors.yellow : Colors.blue,
                            ),
                          );
                        },
                      ),
                    );
                  } else {
                    return loading == false
                        ? Form(
                            key: _formKey,
                            child: Stack(
                              alignment: Alignment.bottomCenter,
                              children: [
                                Container(
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              Navigator.pop(context);
                                            },
                                            child: Container(
                                                margin: EdgeInsets.only(
                                                    top: height / 15,
                                                    left: width / 20),
                                                child: Icon(
                                                  Icons.close,
                                                  size: height / 35,
                                                )),
                                          ),
                                          GestureDetector(
                                            onTap: () async {
                                              SharedPreferences preferences =
                                                  await SharedPreferences
                                                      .getInstance();
                                              preferences.remove('connected');
                                              Navigator.pop(context);
                                              Navigator.pushReplacement<void,
                                                  void>(
                                                context,
                                                MaterialPageRoute<void>(
                                                  builder:
                                                      (BuildContext context) =>
                                                          Login(),
                                                ),
                                              );
                                            },
                                            child: Container(
                                                margin: EdgeInsets.only(
                                                    top: height / 15,
                                                    right: width / 20),
                                                child: Icon(
                                                  Icons.login_outlined,
                                                  size: height / 35,
                                                )),
                                          ),
                                        ],
                                      ), //header
                                      SizedBox(
                                        height: height / 8,
                                      ),
                                      Container(
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(40),
                                              topRight: Radius.circular(40),
                                            )),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            SizedBox(height: height / 10),
                                            Container(
                                              child: Text(
                                                'Profile',
                                                style: TextStyle(
                                                  fontFamily: 'DMSans-Bold',
                                                  fontSize: height * 0.035,
                                                  color:
                                                      const Color(0xff172b4d),
                                                ),
                                                textHeightBehavior:
                                                    TextHeightBehavior(
                                                        applyHeightToFirstAscent:
                                                            false),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                            SizedBox(
                                              height: height / 100,
                                            ),
                                            Container(
                                              child: Text(
                                                'Consult and edit your profile',
                                                style: TextStyle(
                                                  fontFamily: 'DMSans-Regular',
                                                  fontSize: height * 0.023,
                                                  color:
                                                      const Color(0xff7a869a),
                                                  letterSpacing:
                                                      -0.3999999961853027,
                                                  height: 1.7142857142857142,
                                                ),
                                                textHeightBehavior:
                                                    TextHeightBehavior(
                                                        applyHeightToFirstAscent:
                                                            false),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                            SizedBox(
                                              height: height / 14,
                                            ),
                                            Row(
                                              children: [
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      left: width * 0.08),
                                                  child: Text(
                                                    "Full Name",
                                                    style: TextStyle(
                                                        color: Color.fromRGBO(
                                                            193, 199, 208, 3),
                                                        fontSize: height / 45),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Container(
                                              width: width / 1.1,
                                              margin: EdgeInsets.fromLTRB(
                                                  width * 0.08, 0, width * 0.08, 0),
                                              child: TextFormField(
                                                validator: (value) {
                                                  if (value == null ||
                                                      value.isEmpty) {
                                                    return 'Please enter name';
                                                  }
                                                  return null;
                                                },
                                                controller: _nameController,
                                                autofocus: false,
                                                style: TextStyle(
                                                  fontSize: height / 45,
                                                  color: Colors.black,
                                                ),
                                                decoration: InputDecoration(
                                                  prefixIcon: Icon(
                                                    Icons
                                                        .drive_file_rename_outline,
                                                    size: height / 25,
                                                  ),
                                                  suffixIcon: Icon(
                                                    Icons.done,
                                                    color: Colors.amber,
                                                    size: height / 25,
                                                  ),
                                                  border: InputBorder.none,
                                                  filled: true,
                                                  fillColor: Color.fromRGBO(
                                                      244, 245, 247, 3),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15.0),
                                                  ),
                                                  enabledBorder:
                                                      UnderlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15.0),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              height: height / 30,
                                            ),
                                            Row(
                                              children: [
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      left: width * 0.08),
                                                  child: Text(
                                                    "Phone",
                                                    style: TextStyle(
                                                        color: Color.fromRGBO(
                                                            193, 199, 208, 3),
                                                        fontSize: height / 45),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Container(
                                              width: width / 1.1,
                                              margin: EdgeInsets.fromLTRB(
                                                  width * 0.08, 0, width * 0.08, 0),
                                              child: TextField(
                                                controller: _phoneController,
                                                enableInteractiveSelection: false,
                                                readOnly: true,
                                                autofocus: false,
                                                style: TextStyle(
                                                  fontSize: height / 45,
                                                  color: Colors.black,
                                                ),
                                                decoration: InputDecoration(
                                                  prefixIcon: Icon(
                                                    Icons
                                                        .phone_android,
                                                    size: height / 25,
                                                  ),
                                                  suffixIcon: Icon(
                                                    Icons.done,
                                                    color: Colors.amber,
                                                    size: height / 25,
                                                  ),
                                                  border: InputBorder.none,
                                                  filled: true,
                                                  fillColor: Color.fromRGBO(
                                                      244, 245, 247, 3),

                                                  enabledBorder:
                                                  UnderlineInputBorder(
                                                    borderRadius:
                                                    BorderRadius.circular(
                                                        15.0),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              height: height / 35,
                                            ),
                                            Row(
                                              children: [
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      left: width * 0.08),
                                                  child: Text(
                                                    "Email",
                                                    style: TextStyle(
                                                        color: Color.fromRGBO(
                                                            193, 199, 208, 3),
                                                        fontSize: height / 45),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Container(
                                              width: width / 1.1,
                                              margin: EdgeInsets.fromLTRB(
                                                  width * 0.08, 0, width * 0.08, 0),
                                              child: TextFormField(
                                                validator: (value) {
                                                  if (value == null ||
                                                      value.isEmpty) {
                                                    return 'Please enter email';
                                                  }
                                                  return null;
                                                },
                                                controller: _emailController,
                                                obscureText: false,
                                                autofocus: false,
                                                style: TextStyle(
                                                    fontSize: height / 45,
                                                    color: Colors.black),
                                                decoration: InputDecoration(
                                                  prefixIcon: Icon(
                                                    Icons.alternate_email,
                                                    size: height / 25,
                                                  ),
                                                  border: InputBorder.none,
                                                  filled: true,
                                                  fillColor: Color.fromRGBO(
                                                      244, 245, 247, 3),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15.0),
                                                  ),
                                                  enabledBorder:
                                                      UnderlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15.0),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              height: height / 30,
                                            ),
                                            Row(
                                              children: [
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      left: width * 0.08),
                                                  child: Text(
                                                    "Birthday",
                                                    style: TextStyle(
                                                        color: Color.fromRGBO(
                                                            193, 199, 208, 3),
                                                        fontSize: height / 45),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Container(
                                              width: width / 1.1,
                                              margin: EdgeInsets.fromLTRB(
                                                  width * 0.08, 0, width * 0.08, 0),
                                              child: RaisedButton(
                                                onPressed: () =>
                                                    _selectDate(context),
                                                child: Text(birthday == null
                                                    ? ' '
                                                    : birthday.toString().substring(
                                                        0, 10)),
                                              ),
                                            ),
                                            SizedBox(
                                              height: height / 30,
                                            ),
                                            Row(
                                              children: [
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      left: width * 0.08),
                                                  child: Text(
                                                    "Gender",
                                                    style: TextStyle(
                                                        color: Color.fromRGBO(
                                                            193, 199, 208, 3),
                                                        fontSize: height / 45),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Container(
                                              width: width / 1.1,
                                              margin: EdgeInsets.fromLTRB(
                                                  width * 0.08, 0, width * 0.08, 0),
                                              child: DropdownButtonFormField<
                                                  String>(
                                                style: TextStyle(
                                                    fontSize: 13,
                                                    color: Colors.black),
                                                decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20),
                                                  ),
                                                  enabledBorder:
                                                      UnderlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20),
                                                  ),
                                                  errorBorder: InputBorder.none,
                                                  disabledBorder:
                                                      InputBorder.none,
                                                  filled: true,
                                                  hintStyle: TextStyle(
                                                      color: Colors.grey[800]),
                                                  hintText: "Name",
                                                ),
                                                value: dropdownValue,
                                                items: <String>[
                                                  'Male',
                                                  'Female',
                                                ].map<DropdownMenuItem<String>>(
                                                    (String value) {
                                                  return DropdownMenuItem<
                                                      String>(
                                                    value: value,
                                                    child: Text(value),
                                                  );
                                                }).toList(),
                                                onChanged: (String newValue) {
                                                  setState(() {
                                                    dropdownValue = newValue;
                                                  });
                                                },
                                              ),
                                            ),
                                            SizedBox(
                                              height: height / 30,
                                            ),
                                            Row(
                                              children: [
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      left: width * 0.08),
                                                  child: Text(
                                                    "Password",
                                                    style: TextStyle(
                                                        color: Color.fromRGBO(
                                                            193, 199, 208, 3),
                                                        fontSize: height / 45),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Container(
                                              width: width / 1.1,
                                              margin: EdgeInsets.fromLTRB(
                                                  width * 0.08, 0, width * 0.08, 0),
                                              child: TextFormField(
                                                validator: (value) {
                                                  if (value.isNotEmpty && value.length < 8) {
                                                    return 'Please should have 8 characters';
                                                  }
                                                  return null;
                                                },
                                                controller: _passwordController,
                                                obscureText: true,
                                                autofocus: false,
                                                style: TextStyle(
                                                    fontSize: height / 45,
                                                    color: Colors.black),
                                                decoration: InputDecoration(
                                                  prefixIcon: Icon(
                                                    Icons.security,
                                                    size: height / 25,
                                                  ),
                                                  border: InputBorder.none,
                                                  filled: true,
                                                  fillColor: Color.fromRGBO(
                                                      244, 245, 247, 3),
                                                  focusedBorder:
                                                  OutlineInputBorder(
                                                    borderRadius:
                                                    BorderRadius.circular(
                                                        15.0),
                                                  ),
                                                  enabledBorder:
                                                  UnderlineInputBorder(
                                                    borderRadius:
                                                    BorderRadius.circular(
                                                        15.0),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              height: height * 0.05,
                                            ),
                                            Container(
                                              height: height / 15,
                                              width: width / 1.25,
                                              child: InkWell(
                                                onTap: () {},
                                                // ignore: deprecated_member_use
                                                child: RaisedButton(
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15.0),
                                                  ),
                                                  onPressed: () {
                                                    if (_formKey.currentState
                                                        .validate()) {
                                                      setState(() {
                                                        loading = true;
                                                      });
                                                      updateUser(
                                                              _nameController
                                                                  .text,
                                                              _nameController
                                                                  .text,
                                                              _emailController
                                                                  .text,
                                                              _phoneController.text,
                                                          birthday == null ? DateTime.parse('2000-01-01') : birthday,
                                                              dropdownValue ==
                                                                      'Male'
                                                                  ? 1
                                                                  : 2, _passwordController.text)
                                                          .then((result) async {
                                                            print(result);
                                                        setState(() {
                                                          loading = false;
                                                        });
                                                       if(result['status']['code'] == 200){
                                                         showDialog(
                                                             context: context,
                                                             builder:
                                                                 (BuildContext
                                                             context) {
                                                               return DialogScreen(
                                                                 color: Colors
                                                                     .amber,
                                                                 icon:
                                                                 Icons.done,
                                                                 title:
                                                                 'Success',
                                                                 message:
                                                                 'Profile Updated',
                                                                 message2: '',
                                                                 buttonText:
                                                                 'Close',
                                                               );
                                                             });
                                                       }else if(result['status']['code'] == 1005){
                                                         SharedPreferences preferences = await SharedPreferences.getInstance();
                                                         preferences.remove('connected');
                                                         Navigator.push(
                                                             context,
                                                             MaterialPageRoute(
                                                                 builder: (context) =>
                                                                     Login()));
                                                         showDialog(
                                                             context: context,
                                                             builder: (BuildContext context) {
                                                               return DialogScreen(
                                                                 color: Colors.amber,
                                                                 icon: Icons.pending_actions,
                                                                 title: 'Session',
                                                                 message: result['status']['message'],
                                                                 message2: '',
                                                                 buttonText: 'Close',
                                                               );
                                                             });
                                                       }
                                                       else{
                                                         showDialog(
                                                             context: context,
                                                             builder:
                                                                 (BuildContext
                                                             context) {
                                                               return DialogScreen(
                                                                 color: Colors
                                                                     .red,
                                                                 icon:
                                                                 Icons.error_outline,
                                                                 title:
                                                                 'Error',
                                                                 message:
                                                                 result['status']['message'],
                                                                 message2: '',
                                                                 buttonText:
                                                                 'Close',
                                                               );
                                                             });
                                                       }
                                                      });
                                                    }
                                                  },
                                                  color: Colors.amber,
                                                  textColor: Colors.white,
                                                  child: Text(
                                                      "Edit".toUpperCase(),
                                                      style: TextStyle(
                                                          fontSize: height / 46,
                                                          color: Colors.white)),
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Positioned(
                                  top: height / 6.3,
                                  child: Container(
                                    width: height / 7.5,
                                    child: Image.asset(
                                      "assets/images/profilepic.png",
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          )
                        : Container(
                            height: height,
                            child: SpinKitFadingCircle(
                              itemBuilder: (BuildContext context, int index) {
                                return DecoratedBox(
                                  decoration: BoxDecoration(
                                    color: index.isEven
                                        ? Colors.yellow
                                        : Colors.blue,
                                  ),
                                );
                              },
                            ),
                          );
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate != null ? selectedDate : DateTime.now(),
        firstDate: DateTime(1990, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate) print(picked);
    setState(() {
      selectedDate = picked;
      if(birthday != selectedDate && selectedDate != null){
        setState(() {
          birthday = selectedDate;
        });
      }
    });
  }
}
