import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/controllers/linkCardController.dart';
import 'package:flutter_app/controllers/transferController.dart';
import 'package:flutter_app/controllers/userController.dart';
import 'package:flutter_app/models/card.dart';
import 'package:flutter_app/views/dashboard/dashboard.dart';
import 'package:flutter_app/views/dialog/checkcredentials.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/dialog/transaction.dart';
import 'package:flutter_app/views/dialog/transfertSuccessDialog.dart';
import 'package:flutter_app/views/linkCard/linkCard.dart';
import 'package:flutter_app/views/linkCard/linkedCard.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:flutter_app/views/transfert/transferValidation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WithdrawCard extends StatefulWidget {
  double amount;

  WithdrawCard({Key key, @required this.amount}) : super(key: key);

  @override
  _WithdrawCardState createState() => _WithdrawCardState();
}

class _WithdrawCardState extends State<WithdrawCard> {
  final _nameController = TextEditingController();
  final _ribController = TextEditingController();
  final _bankController = TextEditingController();

  String _selected;
  List<BankCard> cards;

  List<BankCard> _myJson = [];
  final _formKey = GlobalKey<FormState>();

  void initState() {

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: SafeArea(
            child: Container(
              width: double.infinity,
              color: Color.fromRGBO(234, 239, 255, 3),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).pop();

                        },
                        child: Container(
                            margin: EdgeInsets.only(
                                top: height / 15, left: width / 20),
                            child: Icon(
                              Icons.close,
                              size: height / 35,
                            )),
                      ),
                    ],
                  ), //header


                  Container(
                    margin: EdgeInsets.only(top: height / 14),
                    width: width,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25),
                          topRight: Radius.circular(25),
                        )),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 30,
                        ),
                        // Row(
                        //   crossAxisAlignment: CrossAxisAlignment.center,
                        //   children: [
                        //     Container(
                        //       margin: EdgeInsets.only(left: 10),
                        //       width: width * 0.80,
                        //       child: DropdownButtonHideUnderline(
                        //           child: ButtonTheme(
                        //             alignedDropdown: true,
                        //             child: DropdownButton(
                        //               hint: Text(
                        //                 'Select bank',
                        //                 style: TextStyle(fontSize: height / 47),
                        //               ),
                        //               value: _selected,
                        //               onChanged: (newValue) {
                        //                 setState(() {
                        //                   _selected = newValue;
                        //                   print(_selected);
                        //                 });
                        //               },
                        //               items: _myJson.map((bankItem) {
                        //                 return DropdownMenuItem(
                        //                   value:
                        //                   bankItem.card_number.toString(),
                        //                   child: Row(
                        //                     children: [
                        //                       Image.asset(
                        //                           'assets/images/' +
                        //                               bankItem.bank_name
                        //                                   .toString() +
                        //                               '.png',
                        //                           width: 25),
                        //                       Column(
                        //                         crossAxisAlignment:
                        //                         CrossAxisAlignment.start,
                        //                         children: [
                        //                           Container(
                        //                             margin: EdgeInsets.only(
                        //                                 left: 10),
                        //                             child: Text(
                        //                               (bankItem.bank_name)
                        //                                   .toString(),
                        //                               style: TextStyle(
                        //                                   fontSize: 16,
                        //                                   fontWeight:
                        //                                   FontWeight.w500),
                        //                             ),
                        //                           ),
                        //                           Container(
                        //                             margin: EdgeInsets.only(
                        //                                 left: 10),
                        //                             child: Text(
                        //                               '****' +
                        //                                   (bankItem.card_number)
                        //                                       .toString()
                        //                                       .substring(
                        //                                       16, 20),
                        //                               style: TextStyle(
                        //                                   color: Colors.grey,
                        //                                   fontSize: 14,
                        //                                   fontWeight:
                        //                                   FontWeight.w500),
                        //                             ),
                        //                           ),
                        //                         ],
                        //                       )
                        //                     ],
                        //                   ),
                        //                 );
                        //               }).toList(),
                        //             ),
                        //           )),
                        //     ),
                        //   ],
                        // ),
                        // Divider(
                        //     height: 20,
                        //     thickness: 2,
                        //     indent: 20,
                        //     endIndent: 20),
                        SizedBox(
                          height: 15,
                        ),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Container(
                            margin: EdgeInsets.only(left: width / 17),
                            child: Text(
                              "On which bank account should we send you your requested withdrawal ?",
                              style: TextStyle(
                                  fontSize: height / 47,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        ),

                        Container(
                          margin: EdgeInsets.only(
                              left: width / 17,
                              right: width / 17,
                              top: height / 30),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Bank",
                                style: TextStyle(
                                    color: Color.fromRGBO(193, 199, 208, 3),
                                    fontSize: height / 45),
                              ),
                              Container(
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Please enter bank name';
                                    }
                                    return null;
                                  },
                                  controller: _bankController,
                                  autofocus: false,
                                  style: TextStyle(
                                      fontSize: height / 50,
                                      color: Colors.black),
                                  decoration: InputDecoration(
                                    prefixIcon: Icon(
                                      Icons.account_circle_rounded,
                                      size: height / 25,
                                    ),
                                    border: InputBorder.none,
                                    hintText: 'Biat',
                                    filled: true,
                                    fillColor: Color.fromRGBO(244, 245, 247, 3),
                                    contentPadding: const EdgeInsets.only(
                                        left: 20.0, bottom: 6.0, top: 17.0),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                              left: width / 17,
                              right: width / 17,
                              top: height / 30),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "RIB",
                                style: TextStyle(
                                    color: Color.fromRGBO(193, 199, 208, 3),
                                    fontSize: height / 45),
                              ),
                              Container(
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Please enter RIB';
                                    }else if(value.length < 20 || value.length > 20){
                                      return 'RIB should have 20 digits';
                                    }
                                    return null;
                                  },
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(20)
                                  ],
                                  keyboardType: TextInputType.number,
                                  controller: _ribController,
                                  autofocus: false,
                                  style: TextStyle(
                                      fontSize: height / 50,
                                      color: Colors.black),
                                  decoration: InputDecoration(
                                    prefixIcon: Icon(
                                      Icons.credit_card_sharp,
                                      size: height / 25,
                                    ),
                                    suffixIcon: Icon(
                                      Icons.done,
                                      color: Colors.amber,
                                    ),
                                    border: InputBorder.none,
                                    hintText: '4509 2097 9956 6997 0000',
                                    filled: true,
                                    fillColor: Color.fromRGBO(244, 245, 247, 3),
                                    contentPadding: const EdgeInsets.only(
                                        left: 20.0, bottom: 6.0, top: 17.0),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),

                        SizedBox(
                          height: height / 3,
                        ),
                        Container(
                          height: height / 13.5,
                          width: width / 1.25,
                          child: InkWell(
                            onTap: () {},
                            // ignore: deprecated_member_use
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              onPressed: () async {
                                SharedPreferences preferences =
                                    await SharedPreferences.getInstance();
                                if (_formKey.currentState.validate()) {
                                  showDialog(
                                      context: context,
                                      builder: (context) {
                                        return CheckCredentials();
                                      }).then((checkCode) {
                                    print(checkCode);
                                    if (checkCode == 200) {
                                      storeWithdrawal(
                                              preferences.getString('idUser'),
                                              widget.amount,
                                              _ribController.text,
                                              _bankController.text)
                                          .then((result) async {
                                        if (result['status']['code'] == 200) {
                                          print(result);
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return Transaction(
                                                    icon: Icons
                                                        .arrow_circle_up_outlined,
                                                    title: 'Success',
                                                    message:
                                                        'Your withdrawal request have been successfully sent. You will be notified once approved or rejected by our agents.');
                                              });
                                        }
                                        if(result['status']['code'] == 1005){
                                          SharedPreferences preferences = await SharedPreferences.getInstance();
                                          preferences.remove('connected');
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      Login()));
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return DialogScreen(
                                                  color: Colors.amber,
                                                  icon: Icons.pending_actions,
                                                  title: 'Session',
                                                  message: result['status']['message'],
                                                  message2: '',
                                                  buttonText: 'Close',
                                                );
                                              });
                                        }
                                        else {
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return DialogScreen(
                                                  color: Colors.red,
                                                  icon: Icons.error_outline,
                                                  title: 'Error',
                                                  message: result['message'],
                                                  message2: '',
                                                  buttonText: 'Close',
                                                );
                                              });
                                        }
                                      });
                                    } else if(checkCode != null && checkCode != 200){
                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return DialogScreen(
                                              color: Colors.red,
                                              icon: Icons.error_outline,
                                              title: 'Error',
                                              message:
                                                  'Unauthorized , Invalid credentials',
                                              message2: '',
                                              buttonText: 'Close',
                                            );
                                          });
                                    }
                                  });
                                }

                                // if(_cardNbController.text.length != 20){
                                //   showDialog(
                                //       context: context,
                                //       builder: (BuildContext context) {
                                //         return DialogScreen(
                                //           color: Colors.red,
                                //           icon: Icons.error_outline,
                                //           title: 'Error',
                                //           message: 'Please enter a valid card number',
                                //           message2: '',
                                //           buttonText: 'Close',
                                //         );
                                //       });
                                // }
                                // if(_nameController.text != "" && _cardNbController.text != "" && _cardNbController.text.length == 20) {
                                // Navigator.push(
                                //   context,
                                //   MaterialPageRoute(
                                //       builder: (context) =>
                                //           TransferValidation(
                                //             cardNb: _selected,
                                //             name: _nameController.text,
                                //             amount: widget.amount,
                                //           )),
                                // );
                                // else{
                                //   showDialog(
                                //       context: context,
                                //       builder: (BuildContext context) {
                                //         return DialogScreen(
                                //           color: Colors.red,
                                //           icon: Icons.error_outline,
                                //           title: 'Error',
                                //           message: 'Please enter all informations',
                                //           message2: '',
                                //           buttonText: 'Close',
                                //         );
                                //       });
                                // }
                              },
                              color: Colors.amber,
                              textColor: Colors.white,
                              child: Text("next".toUpperCase(),
                                  style: TextStyle(
                                      fontSize: height / 47,
                                      color: Colors.white)),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
