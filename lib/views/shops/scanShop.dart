import 'dart:ui';

import 'package:barcode_scan_fix/barcode_scan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/authController.dart';
import 'package:flutter_app/controllers/shopController.dart';
import 'package:flutter_app/controllers/userController.dart';
import 'package:flutter_app/views/dashboard/customBottomNavigationBar.dart';
import 'package:flutter_app/views/dashboard/dashboard.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:flutter_app/views/transfert/transferShop.dart';
import 'package:flutter_app/views/transfert/transferUserQr.dart';
import 'package:flutter_app/views/transfert/transfert.dart';

// import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ScanShop extends StatefulWidget {
  @override
  _ScanShopState createState() => _ScanShopState();
}

class _ScanShopState extends State<ScanShop> {
  @override
  int _selectedItem;

  String shopTitle = "";
  String shopId = "";
  String userTitle = "";
  String userId = "";
  String fromName = "";
  String toPhone;

  String balance = '0';

  _scan() async {
    return await BarcodeScanner.scan().then((value) async {
      print(value);
      SharedPreferences preferences = await SharedPreferences.getInstance();
      getQrCode(value, preferences.getString('idUser')).then((shop) {
        if (shop['status']['code'] != 200) {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return DialogScreen(
                  color: Colors.red,
                  icon: Icons.error_outline,
                  title: 'Error',
                  message: shop['message'],
                  message2: '',
                  buttonText: 'Close',
                );
              });
        } else {
          if (shop['data']['shop'] != null) {
            setState(() {
              shopTitle = shop['data']['shop']['title'];
              shopId = shop['data']['shop']['hashed_id'];
              userTitle = '';
            });
          } else if (shop['data']['to'] != null) {
            setState(() {
              userTitle = shop['data']['to']['first_name'];
              userId = shop['data']['to']['hashed_id'];
              toPhone = shop['data']['to']['phone'].toString();
              shopTitle = '';
            });
          }
        }
      });
    });
  }

  void initState() {
    getUserInfo().then((user) async {
      if(user['status']['code'] == 200){
        print(user);
        setState(() {
          fromName = user['data']['user']['first_name'];
          balance = user['data']['user']['balance'].toString();
        });
      }else if(user['status']['code'] == 1005){
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.remove('connected');
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    Login()));
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return DialogScreen(
                color: Colors.amber,
                icon: Icons.pending_actions,
                title: 'Session',
                message: user['status']['message'],
                message2: '',
                buttonText: 'Close',
              );
            });
      }

    });
    super.initState();
  }

  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      bottomNavigationBar: CustomBottomNavigationBar(
        iconList: [
          Icons.apps,
          Icons.money_outlined,
          Icons.qr_code_scanner_sharp,
          Icons.wallet_membership,
          Icons.account_circle_rounded,
        ],
        onChange: (val) {
          setState(() {
            _selectedItem = 2;
          });
        },
        defaultSelectedIndex: 2,
      ),
      backgroundColor: Color.fromRGBO(234, 239, 255, 3),
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: SafeArea(
          child: Stack(
            alignment: Alignment.center,
            children: [
              Column(
                children: [
                  Container(
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: height / 7,
                        ),
                        Container(
                          width: width,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(40),
                                topRight: Radius.circular(40),
                              )),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              SizedBox(height: height / 9),
                              Container(
                                child: Text(
                                  'Scan QR Code',
                                  style: TextStyle(
                                    fontFamily: 'DMSans-Bold',
                                    fontSize: height * 0.035,
                                    color: const Color(0xff172b4d),
                                  ),
                                  textHeightBehavior: TextHeightBehavior(
                                      applyHeightToFirstAscent: false),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              SizedBox(
                                height: height / 30,
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    width: 310,
                                    height: 310,
                                    decoration: BoxDecoration(
                                        color: Color.fromRGBO(234, 239, 255, 3),
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    child: Padding(
                                      padding: const EdgeInsets.all(75.0),
                                      child: Image(
                                          image: AssetImage(
                                              'assets/images/qrcode.png')),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Container(
                                    width: 310,
                                    height: 180,
                                    decoration: BoxDecoration(
                                        color: Color.fromRGBO(234, 239, 255, 3),
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              balance,
                                              style: TextStyle(fontSize: 24),
                                            ),
                                            Text(
                                              'TND',
                                              style: TextStyle(fontFeatures: [
                                                FontFeature.enable('sups'),
                                              ], fontSize: 11),
                                            )
                                          ],
                                        ),
                                        Text(
                                          'Wingo Balance',
                                          style: TextStyle(fontSize: 17),
                                        ),
                                        Container(
                                          padding: EdgeInsets.all(20.0),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.stretch,
                                            children: <Widget>[
                                              shopTitle != ''
                                                  ? FlatButton(
                                                      color: Colors.lightBlue,
                                                      onPressed: () {
                                                        Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                              builder: (context) =>
                                                                  TransferShop(
                                                                      shopId:
                                                                          shopId,
                                                                      shopName:
                                                                          shopTitle)),
                                                        );
                                                      },
                                                      child: Text(
                                                        "Pay " + shopTitle,
                                                        style: TextStyle(
                                                            color:
                                                                Colors.white),
                                                      ))
                                                  : userTitle != ''
                                                      ? FlatButton(
                                                          color:
                                                              Colors.lightBlue,
                                                          onPressed: () {
                                                            Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder:
                                                                      (context) =>
                                                                          TransferUserQr(
                                                                            fromName:
                                                                                fromName,
                                                                            toName:
                                                                                userTitle,
                                                                            toPhone:
                                                                                toPhone,
                                                                          )),
                                                            );
                                                          },
                                                          child: Text(
                                                            "Pay " + userTitle,
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white),
                                                          ))
                                                      : Container(),
                                              SizedBox(
                                                height: 10.0,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: height / 14,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Positioned(
                top: height / 12.3,
                child: GestureDetector(
                  onTap: () async {
                    _scan();
                  },
                  child: Container(
                    width: height / 7.5,
                    child: CircleAvatar(
                      radius: 45.0,
                      child: Icon(
                        Icons.camera_alt_rounded,
                        color: Colors.amber,
                      ),
                      backgroundColor: Colors.lightBlueAccent[700],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
