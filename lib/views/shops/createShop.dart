import 'dart:async';
import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/authController.dart';
import 'package:flutter_app/controllers/shopController.dart';
import 'package:flutter_app/controllers/userController.dart';
import 'package:flutter_app/models/shopCategory.dart';
import 'package:flutter_app/views/dashboard/customBottomNavigationBar.dart';
import 'package:flutter_app/views/dashboard/dashboard.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/signinScreens/createaccount.dart';
import 'package:flutter_app/views/signinScreens/forgotPassword.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:flutter_app/views/signinScreens/otp_received.dart';
import 'package:flutter_app/views/signinScreens/passwordrecovery.dart';
import 'package:flutter_app/views/user/verifieLevel.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class CreateShop extends StatefulWidget {
  @override
  _CreateShopState createState() => _CreateShopState();
}

class _CreateShopState extends State<CreateShop> {
  int _selectedItem;
  List<ShopCategory> shopCategories = [];
  String _selectedCategory;

  final _titleController = TextEditingController();
  final _addressController = TextEditingController();
  final _descriptionController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  bool loading = false;

  @override
  void initState() {
   getUserInfo().then((value) async {
    if(value['status']['code'] == 200){
      getShopCategories().then((result) {
        print(result);
        setState(() {
          for (var u in result) {
            shopCategories.add(u);
          }
        });
      });
    }else if(value['status']['code'] == 1005){
      SharedPreferences preferences = await SharedPreferences.getInstance();
      preferences.remove('connected');
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  Login()));
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return DialogScreen(
              color: Colors.amber,
              icon: Icons.pending_actions,
              title: 'Session',
              message: value['status']['message'],
              message2: '',
              buttonText: 'Close',
            );
          });
    }
   });
    super.initState();
  }

  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      bottomNavigationBar: CustomBottomNavigationBar(
        iconList: [
          Icons.apps,
          Icons.money_outlined,
          Icons.qr_code_scanner_sharp,
          Icons.wallet_membership,
          Icons.account_circle_rounded,
        ],
        onChange: (val) {
          setState(() {
            _selectedItem = 4;
          });
        },
        defaultSelectedIndex: 4,
      ),
      backgroundColor: Color.fromRGBO(234, 239, 255, 3),
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: loading == true ? Container(
          height: height,
          child: SpinKitFadingCircle(
            itemBuilder: (BuildContext context, int index) {
              return DecoratedBox(
                decoration: BoxDecoration(
                  color: index.isEven ? Colors.yellow : Colors.blue,
                ),
              );
            },
          ),
        ) : Form(
          key: _formKey,
          child: SafeArea(
            child: Column(
              children: [
                Stack(
                  alignment: Alignment.bottomCenter,
                  children: [
                    Container(
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                  margin: EdgeInsets.only(
                                      top: height / 15, left: width / 20),
                                  child: Icon(
                                    Icons.close,
                                    size: height / 35,
                                  )),
                            ],
                          ), //header
                          SizedBox(
                            height: height / 30,
                          ),
                          Container(
                            width: width,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(40),
                                  topRight: Radius.circular(40),
                                )),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                SizedBox(height: height / 20),
                                Container(
                                  child: Text(
                                    'Shop',
                                    style: TextStyle(
                                      fontFamily: 'DMSans-Bold',
                                      fontSize: height * 0.035,
                                      color: const Color(0xff172b4d),
                                    ),
                                    textHeightBehavior: TextHeightBehavior(
                                        applyHeightToFirstAscent: false),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                SizedBox(
                                  height: height / 100,
                                ),
                                Container(
                                  child: Text(
                                    'Please fill the form about your shop details',
                                    style: TextStyle(
                                      fontFamily: 'DMSans-Regular',
                                      fontSize: height * 0.023,
                                      color: const Color(0xff7a869a),
                                      letterSpacing: -0.3999999961853027,
                                      height: 1.7142857142857142,
                                    ),
                                    textHeightBehavior: TextHeightBehavior(
                                        applyHeightToFirstAscent: false),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                SizedBox(
                                  height: height / 60,
                                ),
                                Row(
                                  children: [
                                    Container(
                                      margin:
                                          EdgeInsets.only(left: width * 0.09),
                                      child: Text(
                                        "Title",
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                193, 199, 208, 3),
                                            fontSize: height / 45),
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                  width: width / 1.1,
                                  height: height / 13,
                                  margin: EdgeInsets.fromLTRB(
                                      width * 0.08, 0, 30, 0),
                                  child: TextFormField(
                                    validator: (value) {
                                      if (value == null || value.isEmpty) {
                                        return 'Please enter title';
                                      }
                                      return null;
                                    },
                                    controller: _titleController,
                                    autofocus: false,
                                    style: TextStyle(
                                      fontSize: height / 45,
                                      color: Colors.black,
                                    ),
                                    decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: 'Ooredoo',
                                      filled: true,
                                      fillColor:
                                          Color.fromRGBO(244, 245, 247, 3),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                      enabledBorder: UnderlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    Container(
                                      margin:
                                          EdgeInsets.only(left: width * 0.09),
                                      child: Text(
                                        "Address",
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                193, 199, 208, 3),
                                            fontSize: height / 45),
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                  width: width / 1.1,
                                  height: height / 13,
                                  margin: EdgeInsets.fromLTRB(
                                      width * 0.08, 0, 30, 0),
                                  child: TextFormField(
                                    validator: (value) {
                                      if (value == null || value.isEmpty) {
                                        return 'Please enter address';
                                      }
                                      return null;
                                    },
                                    controller: _addressController,
                                    autofocus: false,
                                    style: TextStyle(
                                      fontSize: height / 45,
                                      color: Colors.black,
                                    ),
                                    decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: 'Marsa, Tunisie',
                                      filled: true,
                                      fillColor:
                                          Color.fromRGBO(244, 245, 247, 3),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                      enabledBorder: UnderlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    Container(
                                      margin:
                                          EdgeInsets.only(left: width * 0.09),
                                      child: Text(
                                        "Category",
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                193, 199, 208, 3),
                                            fontSize: height / 45),
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                  child: Padding(
                                    padding: EdgeInsets.only(left: width / 15),
                                    child: Row(
                                      children: [
                                        DropdownButtonHideUnderline(
                                            child: ButtonTheme(
                                          alignedDropdown: true,
                                          child: DropdownButton(
                                            hint: Text(
                                              'Select Category',
                                              style: TextStyle(
                                                  fontSize: height / 45),
                                            ),
                                            value: _selectedCategory,
                                            onChanged: (newValue) {
                                              setState(() {
                                                print(newValue);
                                                _selectedCategory = newValue;
                                              });
                                            },
                                            items: shopCategories
                                                .map((shopCategory) {
                                              return DropdownMenuItem(
                                                value:
                                                    shopCategory.id.toString(),
                                                child: Row(
                                                  children: [
                                                    Container(
                                                      child: Text(
                                                        (shopCategory.title)
                                                            .toString(),
                                                        style: TextStyle(
                                                            fontSize:
                                                                height / 50,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w500),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              );
                                            }).toList(),
                                          ),
                                        )),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    Container(
                                      margin:
                                          EdgeInsets.only(left: width * 0.09),
                                      child: Text(
                                        "Description",
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                193, 199, 208, 3),
                                            fontSize: height / 45),
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                    width: width / 1.1,
                                    height: height / 13,
                                    margin: EdgeInsets.fromLTRB(
                                        width * 0.109, 0, 30, 0),
                                    child: TextFormField(
                                      validator: (value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Please enter description';
                                        }
                                        return null;
                                      },
                                      controller: _descriptionController,
                                      keyboardType: TextInputType.multiline,
                                      textInputAction: TextInputAction.newline,
                                      minLines: 1,
                                      maxLines: 5,
                                    )),
                                SizedBox(
                                  height: height / 9,
                                ),
                                Container(
                                  height: height / 15,
                                  width: width / 1.25,
                                  child: InkWell(
                                    onTap: () {},
                                    // ignore: deprecated_member_use
                                    child: RaisedButton(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                      onPressed: () {
                                        if (_formKey.currentState.validate() &&
                                            _selectedCategory != null) {
                                          setState(() {
                                            loading = true;
                                          });
                                          addShop(
                                                  _titleController.text,
                                                  _descriptionController.text,
                                                  _addressController.text,
                                                  _selectedCategory)
                                              .then((result) async {
                                            setState(() {
                                              loading = false;
                                            });
                                            print(result);
                                            if (result['status']['code'] !=
                                                200 && result['status']['code'] != 1005) {
                                              showDialog(
                                                  context: context,
                                                  builder:
                                                      (BuildContext context) {
                                                    return DialogScreen(
                                                      color: Colors.red,
                                                      icon: Icons.error_outline,
                                                      title: 'Error',
                                                      message:
                                                          result['message'],
                                                      message2: '',
                                                      buttonText: 'Close',
                                                    );
                                                  });
                                            }
                                            else if(result['status']['code'] == 1005){
                                              SharedPreferences preferences = await SharedPreferences.getInstance();
                                              preferences.remove('connected');
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          Login()));
                                              showDialog(
                                                  context: context,
                                                  builder: (BuildContext context) {
                                                    return DialogScreen(
                                                      color: Colors.amber,
                                                      icon: Icons.pending_actions,
                                                      title: 'Session',
                                                      message: result['status']['message'],
                                                      message2: '',
                                                      buttonText: 'Close',
                                                    );
                                                  });
                                            }
                                              else{
                                              setState(() {
                                                loading = false;
                                              });
                                              showDialog(
                                                  context: context,
                                                  builder:
                                                      (BuildContext context) {
                                                    return DialogScreen(
                                                      color: Colors.amber,
                                                      icon: Icons.error_outline,
                                                      title: 'Success',
                                                      message:
                                                          'Your request to add your shop on WinGo will be reviewed as soon as possible by one of our agents, and will notify you once approved or rejected',
                                                      message2: '',
                                                      buttonText: 'Close',
                                                    );
                                                  });
                                            }
                                          });
                                        } else if (_formKey.currentState
                                                .validate() &&
                                            _selectedCategory == null)
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return DialogScreen(
                                                  color: Colors.red,
                                                  icon: Icons.error_outline,
                                                  title: 'Error',
                                                  message:
                                                      'Please select a category',
                                                  message2: '',
                                                  buttonText: 'Close',
                                                );
                                              });
                                      },
                                      color: Colors.amber,
                                      textColor: Colors.white,
                                      child: Text("Add".toUpperCase(),
                                          style: TextStyle(
                                              fontSize: height / 46,
                                              color: Colors.white)),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    // Positioned(
                    //   top: height / 6.3,
                    //   child: Container(
                    //     width: height / 7.5,
                    //     child: Image.asset(
                    //       "assets/images/profilepic.png",
                    //       fit: BoxFit.fill,
                    //     ),
                    //   ),
                    // )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
