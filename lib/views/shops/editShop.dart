import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/authController.dart';
import 'package:flutter_app/controllers/shopController.dart';
import 'package:flutter_app/controllers/userController.dart';
import 'package:flutter_app/models/shopCategory.dart';
import 'package:flutter_app/views/dashboard/customBottomNavigationBar.dart';
import 'package:flutter_app/views/dashboard/dashboard.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/signinScreens/createaccount.dart';
import 'package:flutter_app/views/signinScreens/forgotPassword.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:flutter_app/views/signinScreens/otp_received.dart';
import 'package:flutter_app/views/user/verifieLevel.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class EditShop extends StatefulWidget {
  @override
  _EditShopState createState() => _EditShopState();
}

class _EditShopState extends State<EditShop> {
  int _selectedItem;
  List<ShopCategory> shopCategories = [];
  String _selectedCategory;

  final _titleController = TextEditingController();
  final _addressController = TextEditingController();
  final _descriptionController = TextEditingController();

  File imageFile;
  String photo = "";
  File _image;
  final picker = ImagePicker();
  bool loading = false;

  final _formKey = GlobalKey<FormState>();


  Future getImage() async {
    // ignore: deprecated_member_use
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      imageFile = File(pickedFile.path);
    });
  }

  static Future image2Base64(String path) async {
    File file = new File(path);
    List<int> imageBytes = await file.readAsBytes();
    return base64Encode(imageBytes);
  }

  Future userShop;

  @override
  void initState() {

    getUserInfo().then((info) async {
      if(info['status']['code'] == 200){
        userShop = getUserShop();
        getShopCategories().then((result) {
          // print(result);
          setState(() {
            for (var u in result) {
              shopCategories.add(u);
            }
          });
        });

        getUserShop().then((shop) {
          setState(() {
            _titleController.text = shop['data']['shop']['title'];
            _descriptionController.text = shop['data']['shop']['description'];
            _addressController.text = shop['data']['shop']['address'];
            _selectedCategory = shop['data']['shop']['hashed_category_id'];
          });
          print(shop);
        });
      }else if(info['status']['code'] == 1005){
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.remove('connected');
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    Login()));
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return DialogScreen(
                color: Colors.amber,
                icon: Icons.pending_actions,
                title: 'Session',
                message: info['status']['message'],
                message2: '',
                buttonText: 'Close',
              );
            });
      }
    });
    super.initState();
  }

  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      bottomNavigationBar: CustomBottomNavigationBar(
        iconList: [
          Icons.apps,
          Icons.money_outlined,
          Icons.qr_code_scanner_sharp,
          Icons.wallet_membership,
          Icons.account_circle_rounded,
        ],
        onChange: (val) {
          setState(() {
            _selectedItem = 4;
          });
        },
        defaultSelectedIndex: 4,
      ),
      backgroundColor: Color.fromRGBO(234, 239, 255, 3),
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
          child: FutureBuilder(
        future: userShop,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (!snapshot.hasData) {
            return Container(
              height: height,
              child: SpinKitFadingCircle(
                itemBuilder: (BuildContext context, int index) {
                  return DecoratedBox(
                    decoration: BoxDecoration(
                      color: index.isEven ? Colors.yellow : Colors.blue,
                    ),
                  );
                },
              ),
            );
          } else {
            return loading == false ? Form(
              key: _formKey,
              child: SafeArea(
                child: Column(
                  children: [
                    Stack(
                      alignment: Alignment.bottomCenter,
                      children: [
                        Container(
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  GestureDetector(
                                    onTap: (){
                                      Navigator.pop(context);
                                    },
                                    child: Container(
                                        margin: EdgeInsets.only(
                                            top: height / 15, left: width / 20),
                                        child: Icon(
                                          Icons.close,
                                          size: height / 35,
                                        )),
                                  ),
                                ],
                              ), //header
                              SizedBox(
                                height: height / 30,
                              ),
                              Container(
                                width: width,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(40),
                                      topRight: Radius.circular(40),
                                    )),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    SizedBox(height: height / 11),
                                    Container(
                                      child: Text(
                                        'Shop',
                                        style: TextStyle(
                                          fontFamily: 'DMSans-Bold',
                                          fontSize: height * 0.035,
                                          color: const Color(0xff172b4d),
                                        ),
                                        textHeightBehavior: TextHeightBehavior(
                                            applyHeightToFirstAscent: false),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    SizedBox(
                                      height: height / 100,
                                    ),
                                    Container(
                                      child: Text(
                                        'Enter Shop Information',
                                        style: TextStyle(
                                          fontFamily: 'DMSans-Regular',
                                          fontSize: height * 0.023,
                                          color: const Color(0xff7a869a),
                                          letterSpacing: -0.3999999961853027,
                                          height: 1.7142857142857142,
                                        ),
                                        textHeightBehavior: TextHeightBehavior(
                                            applyHeightToFirstAscent: false),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    SizedBox(
                                      height: height / 60,
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          margin:
                                              EdgeInsets.only(left: width * 0.09),
                                          child: Text(
                                            "Title",
                                            style: TextStyle(
                                                color: Color.fromRGBO(
                                                    193, 199, 208, 3),
                                                fontSize: height / 45),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      width: width / 1.1,
                                      height: height / 13,
                                      margin: EdgeInsets.fromLTRB(
                                          width * 0.08, 0, 30, 0),
                                      child: TextFormField(
                                        validator: (value) {
                                          if (value == null || value.isEmpty) {
                                            return 'Please enter title';
                                          }
                                          return null;
                                        },
                                        controller: _titleController,
                                        autofocus: false,
                                        style: TextStyle(
                                          fontSize: height / 45,
                                          color: Colors.black,
                                        ),
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText: 'Ooredoo',
                                          filled: true,
                                          fillColor:
                                              Color.fromRGBO(244, 245, 247, 3),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                          ),
                                          enabledBorder: UnderlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          margin:
                                              EdgeInsets.only(left: width * 0.09),
                                          child: Text(
                                            "Address",
                                            style: TextStyle(
                                                color: Color.fromRGBO(
                                                    193, 199, 208, 3),
                                                fontSize: height / 45),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      width: width / 1.1,
                                      height: height / 13,
                                      margin: EdgeInsets.fromLTRB(
                                          width * 0.08, 0, 30, 0),
                                      child: TextFormField(
                                        validator: (value) {
                                          if (value == null || value.isEmpty) {
                                            return 'Please enter address';
                                          }
                                          return null;
                                        },
                                        controller: _addressController,
                                        autofocus: false,
                                        style: TextStyle(
                                          fontSize: height / 45,
                                          color: Colors.black,
                                        ),
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText: 'Marsa, Tunisie',
                                          filled: true,
                                          fillColor:
                                              Color.fromRGBO(244, 245, 247, 3),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                          ),
                                          enabledBorder: UnderlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          margin:
                                              EdgeInsets.only(left: width * 0.09),
                                          child: Text(
                                            "Category",
                                            style: TextStyle(
                                                color: Color.fromRGBO(
                                                    193, 199, 208, 3),
                                                fontSize: height / 45),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      child: Padding(
                                        padding:
                                            EdgeInsets.only(left: width / 15),
                                        child: Row(
                                          children: [
                                            DropdownButtonHideUnderline(
                                                child: ButtonTheme(
                                              alignedDropdown: true,
                                              child: DropdownButton(
                                                hint: Text(
                                                  'Select Category',
                                                  style: TextStyle(
                                                      fontSize: height / 45),
                                                ),
                                                value: _selectedCategory,
                                                onChanged: (newValue) {
                                                  setState(() {
                                                    print(newValue);
                                                    _selectedCategory = newValue;
                                                  });
                                                },
                                                items: shopCategories
                                                    .map((shopCategory) {
                                                  return DropdownMenuItem(
                                                    value: shopCategory.id
                                                        .toString(),
                                                    child: Row(
                                                      children: [
                                                        Container(
                                                          child: Text(
                                                            (shopCategory.title)
                                                                .toString(),
                                                            style: TextStyle(
                                                                fontSize:
                                                                    height / 50,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  );
                                                }).toList(),
                                              ),
                                            )),
                                          ],
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          margin:
                                              EdgeInsets.only(left: width * 0.09),
                                          child: Text(
                                            "Description",
                                            style: TextStyle(
                                                color: Color.fromRGBO(
                                                    193, 199, 208, 3),
                                                fontSize: height / 45),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                        width: width / 1.1,
                                        height: height / 13,
                                        margin: EdgeInsets.fromLTRB(
                                            width * 0.109, 0, 30, 0),
                                        child: TextFormField(
                                          validator: (value) {
                                            if (value == null || value.isEmpty) {
                                              return 'Please enter description';
                                            }
                                            return null;
                                          },
                                          controller: _descriptionController,
                                          keyboardType: TextInputType.multiline,
                                          textInputAction:
                                              TextInputAction.newline,
                                          minLines: 1,
                                          maxLines: 5,
                                        )),
                                    SizedBox(
                                      height: height / 12,
                                    ),
                                    Container(
                                      height: height / 15,
                                      width: width / 1.25,
                                      child: InkWell(
                                        onTap: () {},
                                        // ignore: deprecated_member_use
                                        child: RaisedButton(
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                          ),
                                          onPressed: () {
                                            if (_formKey.currentState.validate()) {
                                              setState(() {
                                                loading = true;
                                              });
                                              updateShop(snapshot.data['data']['shop']['hashed_id'],
                                                  _titleController.text,
                                                  _descriptionController.text,
                                                  _addressController.text,
                                                  _selectedCategory)
                                                  .then((result) async {
                                                setState(() {
                                                  loading = false;
                                                });
                                                print(result);
                                                if (result['status']['code'] !=
                                                    200 && result['status']['code'] != 1005) {
                                                  showDialog(
                                                      context: context,
                                                      builder:
                                                          (BuildContext context) {
                                                        return DialogScreen(
                                                          color: Colors.red,
                                                          icon: Icons.error_outline,
                                                          title: 'Error',
                                                          message:
                                                          result['message'],
                                                          message2: '',
                                                          buttonText: 'Close',
                                                        );
                                                      });
                                                }
                                                else if(result['status']['code'] == 1005){
                                                  SharedPreferences preferences = await SharedPreferences.getInstance();
                                                  preferences.remove('connected');
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              Login()));
                                                  showDialog(
                                                      context: context,
                                                      builder: (BuildContext context) {
                                                        return DialogScreen(
                                                          color: Colors.amber,
                                                          icon: Icons.pending_actions,
                                                          title: 'Session',
                                                          message: result['status']['message'],
                                                          message2: '',
                                                          buttonText: 'Close',
                                                        );
                                                      });
                                                }
                                                else {
                                                  showDialog(
                                                      context: context,
                                                      builder:
                                                          (BuildContext context) {
                                                        return DialogScreen(
                                                          color: Colors.amber,
                                                          icon: Icons.error_outline,
                                                          title: 'Success',
                                                          message:
                                                          'Your shop have been updated successfully!',
                                                          message2: '',
                                                          buttonText: 'Close',
                                                        );
                                                      });
                                                }
                                              });
                                            }
                                          },
                                          color: Colors.amber,
                                          textColor: Colors.white,
                                          child: Text("Edit".toUpperCase(),
                                              style: TextStyle(
                                                  fontSize: height / 46,
                                                  color: Colors.white)),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Positioned(
                          top: height / 16.3,
                          child: InkWell(
                            onTap: () async {
                              // ignore: deprecated_member_use
                              getImage().then((file) async{
                                if (imageFile != null) {
                                  image2Base64(imageFile.path).then((value) async {
                                    print(value);
                                    uploadShopLogo("data:image/png;base64,$value")
                                        .then((result) {
                                      print(result);
                                      // setState(() {
                                      //   photo = result['data']['photoPath'];
                                      // });
                                    });
                                  });
                                }
                              });
                            },
                            child: Container(
                                 width: height / 7.5,
                             child: Image.asset(
                                  "assets/images/profilepic.png",
                                  fit: BoxFit.fill,
                                )),
                            // child: Container(
                            //   width: height / 7.5,
                            //   child: imageFile != null
                            //       ? ClipRRect(
                            //           borderRadius: BorderRadius.circular(100.0),
                            //           child: Image.file(
                            //             imageFile,
                            //             width: height / 7.5,
                            //             height: height / 7.5,
                            //           ))
                            //       : snapshot.data['data']['shop']['logo'] == null
                            //           ? Image.asset(
                            //               "assets/images/profilepic.png",
                            //               fit: BoxFit.fill,
                            //             )
                            //           : ClipRRect(
                            //               borderRadius:
                            //                   BorderRadius.circular(100.0),
                            //               child: Image.network(
                            //                   'https://wingo.tn/' +
                            //                       snapshot.data['data']['shop']
                            //                           ['logo'])),
                            // ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ) : Container(
              height: height,
              child: SpinKitFadingCircle(
                itemBuilder: (BuildContext context, int index) {
                  return DecoratedBox(
                    decoration: BoxDecoration(
                      color: index.isEven ? Colors.yellow : Colors.blue,
                    ),
                  );
                },
              ),
            );
          }
        },
      )),
    );
  }
}
