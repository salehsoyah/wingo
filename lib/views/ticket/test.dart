import 'package:flutter/material.dart';

class Test extends StatefulWidget {
  @override
  _TestState createState() => _TestState();
}

class _TestState extends State<Test> {
  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height = MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return SafeArea(
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Container(
              height: height / 1.5,
              color: Colors.red,
            ),
            Positioned(
              top: height / 4,
                child: Container(
                  color: Colors.blueAccent,
                  height: height / 6,
                  width: width / 4,

                )
            )
          ],
        )
    );
  }
}
