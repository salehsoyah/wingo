import 'dart:ui';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/authController.dart';
import 'package:flutter_app/controllers/userController.dart';
import 'package:flutter_app/views/dashboard/customBottomNavigationBar.dart';
import 'package:flutter_app/views/dashboard/dashboard.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/shops/createShop.dart';
import 'package:flutter_app/views/shops/editShop.dart';
import 'package:flutter_app/views/signinScreens/createaccount.dart';
import 'package:flutter_app/views/signinScreens/forgotPassword.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:flutter_app/views/signinScreens/otp_received.dart';
import 'package:flutter_app/views/signinScreens/passwordrecovery.dart';
import 'package:flutter_app/views/user/verifieLevel.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';

class History extends StatefulWidget {
  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  int _selectedItem;
  int transaction = 1;

  Future transfers;
  Future deposits;
  Future withdrawals;
  final _scrollController = ScrollController();

  @override
  void initState() {
    getUserInfo().then((result) async {
      // print(result);
      if(result['status']['code'] == 1005){
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.remove('connected');
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    Login()));
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return DialogScreen(
                color: Colors.amber,
                icon: Icons.pending_actions,
                title: 'Session',
                message: result['status']['message'],
                message2: '',
                buttonText: 'Close',
              );
            });
      }else if(result['status']['code'] == 200){
      setState((){
        transfers = getListUserTransfer();
        deposits = getListUserDeposits();
        withdrawals = getListUserWithdrawals();
      });
      }

    });

    super.initState();
  }

  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      bottomNavigationBar: CustomBottomNavigationBar(
        iconList: [
          Icons.apps,
          Icons.money_outlined,
          Icons.qr_code_scanner_sharp,
          Icons.wallet_membership,
          Icons.account_circle_rounded,
        ],
        onChange: (val) {
          setState(() {
            _selectedItem = 1;
          });
        },
        defaultSelectedIndex: 1,
      ),
      backgroundColor: Color.fromRGBO(234, 239, 255, 3),
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.only(top: height / 15),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(50),
                topRight: Radius.circular(50),
              )),
          child: FutureBuilder(
            future: transaction == 1
                ? transfers
                : transaction == 2
                    ? deposits
                    : withdrawals,
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.connectionState ==
                  ConnectionState.waiting) {
                return Container(
                  height: height,
                  child: SpinKitFadingCircle(
                    itemBuilder: (BuildContext context, int index) {
                      return DecoratedBox(
                        decoration: BoxDecoration(
                          color: index.isEven ? Colors.yellow : Colors.blue,
                        ),
                      );
                    },
                  ),
                );
              } else {
                return Column(
                  children: <Widget>[
                    SizedBox(
                      height: height / 16,
                    ),
                    SizedBox(height: height / 20),
                    Container(
                      child: Text(
                        'Transactions',
                        style: TextStyle(
                          fontFamily: 'DMSans-Bold',
                          fontSize: height * 0.030,
                          color: const Color(0xff172b4d),
                        ),
                        textHeightBehavior:
                            TextHeightBehavior(applyHeightToFirstAscent: false),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(
                      height: height / 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              transaction = 1;
                            });
                          },
                          child: Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: transaction == 1 ? Colors.yellow[400] : Colors.transparent
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Container(
                                    width: 75,
                                    height: 75,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15),
                                      color: Colors.yellow[200],
                                    ),
                                    child: Center(
                                        child: Icon(Icons.compare_arrows_outlined)),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text('Transfers')
                            ],
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              transaction = 2;
                            });
                          },
                          child: Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: transaction == 2 ? Colors.green[200] : Colors.transparent
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Container(
                                    width: 75,
                                    height: 75,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15),
                                      color: Colors.green[100],
                                    ),
                                    child: Center(
                                        child:
                                            Icon(Icons.arrow_circle_down_outlined)),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text('Deposits')
                            ],
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              transaction = 3;
                            });
                          },
                          child: Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: transaction == 3 ? Colors.red[200] : Colors.transparent
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Container(
                                    width: 75,
                                    height: 75,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15),
                                      color: Colors.red[100],
                                    ),
                                    child: Center(
                                        child:
                                            Icon(Icons.arrow_circle_up_outlined)),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text('Withdrawals')
                            ],
                          ),
                        )
                      ],
                    ),
                    Divider(
                      color: Colors.amber,
                      thickness: 3.0,
                      height: 40,
                      indent: 20,
                      endIndent: 20,
                    ),
                    (snapshot.data != null && snapshot.data.length > 0) ? Flexible(
                      fit: FlexFit.loose,
                      child: Scrollbar(
                        isAlwaysShown: false,
                        controller: _scrollController,
                        child: SingleChildScrollView(
                          controller: _scrollController,
                          child: Container(
                              child: ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            itemCount: snapshot.data.length,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.only(
                                    left: 20, right: 20.0, bottom: 15),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(right: 8.0),
                                          child: Image(
                                            width: 50,
                                            image: transaction == 1
                                                ? AssetImage(
                                                    "assets/images/transfer.png")
                                                : transaction == 2
                                                    ? AssetImage(
                                                        "assets/images/deposit.png")
                                                    : AssetImage(
                                                        "assets/images/withdraw.png"),
                                          ),
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(transaction == 2
                                                ? snapshot.data[index].type == 1
                                                    ? 'Card Deposit'
                                                    : 'Cash In'
                                                : transaction == 3
                                                    ? snapshot.data[index].type ==
                                                            1
                                                        ? 'Bank Withdrawal'
                                                        : 'Cash out'
                                                    : transaction == 1
                                                        ? snapshot
                                                            .data[index].toName
                                                        : ' '),
                                            Text(
                                              (snapshot.data[index].transfer_date)
                                                  .substring(0, 10),
                                              style:
                                                  TextStyle(color: Colors.grey),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    // Text("- " +
                                    //     snapshot.data[index]
                                    //         .amount
                                    //         .toString()),
                                    transaction != 3
                                        ? Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                            Text(transaction == 1
                                                ? " - " +
                                                    snapshot.data[index].amount
                                                        .toString()
                                                : " + " +
                                                    snapshot.data[index].amount
                                                        .toString()),
                                            Text(
                                              'TND',
                                              style: TextStyle(
                                                  fontSize: 8,
                                                  fontFeatures: [
                                                    FontFeature.enable(
                                                        'sups'),
                                                  ]),
                                            )
                                          ],
                                        )
                                        : Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,

                                      children: [
                                            Text(
                                                "- " +
                                                    snapshot.data[index].amount
                                                        .toString(),
                                                style: TextStyle(
                                                    color:
                                                        snapshot.data[index].status ==
                                                                0
                                                            ? Colors.amber
                                                            : snapshot.data[index]
                                                                        .status ==
                                                                    1
                                                                ? Colors.green
                                                                : Colors.red),
                                              ),
                                            Text(
                                              'TND',
                                              style: TextStyle(
                                                  fontSize: 8,
                                                  fontFeatures: [
                                                    FontFeature.enable(
                                                        'sups'),
                                                  ]),
                                            )
                                          ],
                                        ),
                                  ],
                                ),
                              );
                            },
                          )),
                        ),
                      ),
                    ) : transaction == 1 ? Text('No Transfers') : transaction == 2 ? Text('No Deposits') : Text('No Withdrawals'),
                  ],
                );
              }
            },
          ),
        ),
      ),
    );
  }
}
