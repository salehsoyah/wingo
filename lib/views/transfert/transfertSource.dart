import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/linkCardController.dart';
import 'package:flutter_app/controllers/userController.dart';
import 'package:flutter_app/models/card.dart';
import 'package:flutter_app/views/dashboard/dashboard.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/dialog/transfertSuccessDialog.dart';
import 'package:flutter_app/views/linkCard/linkCard.dart';
import 'package:flutter_app/views/linkCard/linkedCard.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:flutter_app/views/transfert/transferValidation.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TransferSource extends StatefulWidget {
  String amount;

  TransferSource({Key key, @required this.amount}) : super(key: key);

  @override
  _TransferSourceState createState() => _TransferSourceState();
}

class _TransferSourceState extends State<TransferSource> {
  final _nameController = TextEditingController();
  final _cardNbController = TextEditingController();
  final _phoneController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  String _selected;
  List<BankCard> cards;

  List<BankCard> _myJson = [];

  bool loading = false;
  int seeMore = 0;
  Future recentPayees;
  void initState() {
    getUserInfo().then((user) async {
      if(user['status']['code'] == 200){
        recentPayees = getUserRecentPayees();
        getUserRecentPayees().then((value){
          print(value);
        });
      }else if(user['status']['code'] == 1005){
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.remove('connected');
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    Login()));
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return DialogScreen(
                color: Colors.amber,
                icon: Icons.pending_actions,
                title: 'Session',
                message: user['status']['message'],
                message2: '',
                buttonText: 'Close',
              );
            });
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: SafeArea(
              child: Container(
                width: double.infinity,
                color: Color.fromRGBO(234, 239, 255, 3),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Container(
                              margin: EdgeInsets.only(
                                  top: height / 15, left: width / 20),
                              child: Icon(
                                Icons.close,
                                size: height / 35,
                              )),
                        ),
                      ],
                    ), //header

                    SizedBox(
                      height: height / 15,
                    ),
                    Column(
                      children: [
                        SingleChildScrollView(
                          child: Container(
                            width: width,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(25),
                                  topRight: Radius.circular(25),
                                  bottomLeft: Radius.circular(25),
                                  bottomRight: Radius.circular(25),
                                )),
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 30,
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Container(
                                    margin: EdgeInsets.only(left: 15),
                                    child: Text(
                                      "Recent Payees",
                                      style: TextStyle(
                                          fontSize: height / 47,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                FutureBuilder(
                                  future: recentPayees, builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                                  if (snapshot.connectionState ==
                                      ConnectionState.done) {
                                    if(snapshot.hasData && snapshot.data.length > 0){
                                      return Padding(
                                        padding: const EdgeInsets.only(left: 8.0, bottom: 17, right: 8.0),
                                        child: Column(
                                          children: [
                                            GridView.builder(
                                              shrinkWrap: true,
                                              itemCount: (snapshot.data.length ~/ 4) < 1
                                                  ? snapshot.data.length
                                                  : (4 + seeMore),
                                              gridDelegate:
                                              SliverGridDelegateWithFixedCrossAxisCount(
                                                  crossAxisCount: 4,
                                                  childAspectRatio: 1),
                                              itemBuilder: (BuildContext context, int index) {
                                                return ListView.builder(
                                                    shrinkWrap: true,
                                                    itemCount: 1,
                                                    itemBuilder:
                                                        (BuildContext ctxt, int indx) {
                                                      return Padding(
                                                        padding: const EdgeInsets.all(8.0),
                                                        child: Container(
                                                          child: InkWell(
                                                            onTap: (){
                                                            setState(() {
                                                              _phoneController.text = snapshot.data[index].phone.toString();
                                                            });
                                                            },
                                                            child: new Column(
                                                              crossAxisAlignment:
                                                              CrossAxisAlignment.center,
                                                              mainAxisAlignment:
                                                              MainAxisAlignment.center,
                                                              children: <Widget>[
                                                                Image(
                                                                  image: AssetImage(
                                                                      "assets/images/person.PNG"),
                                                                ),
                                                                SizedBox(height: 2),
                                                                Text(snapshot
                                                                    .data[index].name, style:
                                                                  TextStyle(
                                                                    fontSize: height / 60
                                                                  ),
                                                                ),
                                                                SizedBox(height: 2,)
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      );
                                                    });
                                              },
                                            ),
                                            snapshot.data.length > 4 ? Container(
                                              height: height / 13.5,
                                              width: width / 1.25,
                                              margin: EdgeInsets.only(top: height / 50),
                                              child: InkWell(
                                                onTap: () {},
                                                // ignore: deprecated_member_use
                                                child: FlatButton(
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                    BorderRadius.circular(15.0),
                                                  ),
                                                  onPressed: () {
                                                    print(seeMore);
                                                    if (snapshot.data.length -
                                                        (4 + seeMore) >=
                                                        4) {
                                                      setState(() {
                                                        seeMore = seeMore + 4;
                                                      });
                                                    } else {
                                                      setState(() {
                                                        seeMore = seeMore +
                                                            snapshot.data.length -
                                                            (4 + seeMore);
                                                      });
                                                    }
                                                  },
                                                  color:
                                                  Color.fromRGBO(244, 245, 247, 3),
                                                  textColor: Colors.white,
                                                  child: Text("See all",
                                                      style: TextStyle(
                                                          fontSize: height / 50,
                                                          color:
                                                          const Color(0xff7a869a))),
                                                ),
                                              ),
                                            ) : Container(),
                                          ],
                                        ),
                                      );
                                    }else{
                                      return Text(
                                        "No recent payees !",
                                        style: TextStyle(fontSize: height / 47),
                                      );
                                    }
                                  } else {
                                    return Center(child: CircularProgressIndicator());
                                  }
                                },
                                ),
                                // Container(
                                //   margin: EdgeInsets.only(left: width / 17, right: width / 17, top: height / 30),

                                // child: Column(
                                //   mainAxisAlignment: MainAxisAlignment.start,
                                //   crossAxisAlignment: CrossAxisAlignment.start,
                                //   children: [
                                //     Text(
                                //       "Name",
                                //       style: TextStyle(
                                //           color: Color.fromRGBO(193, 199, 208, 3),
                                //           fontSize: height / 45),
                                //     ),
                                //     Container(
                                //       child: TextField(
                                //         controller: _nameController,
                                //         autofocus: false,
                                //         style: TextStyle(
                                //             fontSize: height / 50,
                                //             color: Colors.black),
                                //         decoration: InputDecoration(
                                //           prefixIcon: Icon(
                                //             Icons.account_circle_rounded,
                                //             size: height / 25,
                                //           ),
                                //           border: InputBorder.none,
                                //           hintText: 'Stella Cobb',
                                //           filled: true,
                                //           fillColor:
                                //           Color.fromRGBO(244, 245, 247, 3),
                                //           contentPadding: const EdgeInsets.only(
                                //               left: 20.0, bottom: 6.0, top: 17.0),
                                //           focusedBorder: OutlineInputBorder(
                                //             borderRadius:
                                //             BorderRadius.circular(15.0),
                                //           ),
                                //           enabledBorder: UnderlineInputBorder(
                                //             borderRadius:
                                //             BorderRadius.circular(15.0),
                                //           ),
                                //         ),
                                //       ),
                                //     ),
                                //   ],
                                // ),
                                // ),
                                // Container(
                                //   margin: EdgeInsets.only(left: width / 17, right: width / 17, top: height / 30),
                                //   child: Column(
                                //     mainAxisAlignment: MainAxisAlignment.start,
                                //     crossAxisAlignment: CrossAxisAlignment.start,
                                //     children: [
                                //       Text(
                                //         "Card Number",
                                //         style: TextStyle(
                                //             color: Color.fromRGBO(193, 199, 208, 3),
                                //             fontSize: height / 45),
                                //       ),
                                //       Container(
                                //         child: TextField(
                                //           keyboardType: TextInputType.number,
                                //           controller: _cardNbController,
                                //           autofocus: false,
                                //           style: TextStyle(
                                //               fontSize: height / 50, color: Colors.black),
                                //           decoration: InputDecoration(
                                //             prefixIcon: Icon(
                                //               Icons.credit_card_sharp,
                                //               size: height / 25,
                                //             ),
                                //             suffixIcon: Icon(
                                //               Icons.done,
                                //               color: Colors.amber,
                                //             ),
                                //             border: InputBorder.none,
                                //             hintText: '4509 2097 9956 6997 0000',
                                //             filled: true,
                                //             fillColor: Color.fromRGBO(244, 245, 247, 3),
                                //             contentPadding: const EdgeInsets.only(
                                //                 left: 20.0, bottom: 6.0, top: 17.0),
                                //             focusedBorder: OutlineInputBorder(
                                //               borderRadius: BorderRadius.circular(15.0),
                                //             ),
                                //             enabledBorder: UnderlineInputBorder(
                                //               borderRadius: BorderRadius.circular(15.0),
                                //             ),
                                //           ),
                                //         ),
                                //       ),
                                //
                                //     ],
                                //   ),
                                // ),
                                loading == false
                                    ? Container(
                                  margin: EdgeInsets.only(
                                      left: width / 17,
                                      right: width / 17,
                                      top: height / 50),
                                  child: Column(
                                    mainAxisAlignment:
                                    MainAxisAlignment.start,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Payee's phone",
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                193, 199, 208, 3),
                                            fontSize: height / 45),
                                      ),
                                      Container(
                                        child: TextFormField(
                                          validator: (value) {
                                            if (value == null ||
                                                value.isEmpty) {
                                              return 'Please enter payee"s phone';
                                            }else if(value.length > 8 || value.length < 8){
                                              return 'Please enter payee"s phone should have 8 digits';
                                            }
                                            return null;
                                          },
                                          controller: _phoneController,
                                          keyboardType:
                                          TextInputType.number,
                                          autofocus: false,
                                          style: TextStyle(
                                              fontSize: height / 50,
                                              color: Colors.black),
                                          decoration: InputDecoration(
                                            prefixIcon: Icon(
                                              Icons.phone_android,
                                              size: height / 25,
                                            ),
                                            suffixIcon: Icon(
                                              Icons.done,
                                              color: Colors.amber,
                                            ),
                                            border: InputBorder.none,
                                            hintText: '21 111 111',
                                            filled: true,
                                            fillColor: Color.fromRGBO(
                                                244, 245, 247, 3),
                                            contentPadding:
                                            const EdgeInsets.only(
                                                left: 20.0,
                                                bottom: 6.0,
                                                top: 17.0),
                                            focusedBorder:
                                            OutlineInputBorder(
                                              borderRadius:
                                              BorderRadius.circular(
                                                  15.0),
                                            ),
                                            enabledBorder:
                                            UnderlineInputBorder(
                                              borderRadius:
                                              BorderRadius.circular(
                                                  15.0),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                                    : Container(
                                  height: height / 2,
                                  child: SpinKitFadingCircle(
                                    itemBuilder: (BuildContext context,
                                        int index) {
                                      return DecoratedBox(
                                        decoration: BoxDecoration(
                                          color: index.isEven
                                              ? Colors.yellow
                                              : Colors.blue,
                                        ),
                                      );
                                    },
                                  ),
                                ),
                                SizedBox(
                                  height: height / 3.5,
                                ),
                                loading == false
                                    ? Container(
                                  height: height / 13.5,
                                  width: width / 1.25,
                                  child: InkWell(
                                    onTap: () {},
                                    // ignore: deprecated_member_use
                                    child: RaisedButton(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                        BorderRadius.circular(15.0),
                                      ),
                                      onPressed: () async {
                                        if (_formKey.currentState
                                            .validate()) {
                                          setState(() {
                                            loading = true;
                                          });
                                          getRecipient(int.parse(
                                              _phoneController
                                                  .text))
                                              .then((result) async {
                                            setState(() {
                                              loading = false;
                                            });
                                            print(result);
                                            SharedPreferences
                                            preferences =
                                            await SharedPreferences
                                                .getInstance();
                                            print(preferences
                                                .getString('userName'));

                                            if (result['status']
                                            ['code'] ==
                                                200) {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        TransferValidation(
                                                          from: preferences
                                                              .getString(
                                                              'userName'),
                                                          amount: widget
                                                              .amount,
                                                          toName: result[
                                                          'data'][0]
                                                          [
                                                          'first_name'],
                                                          toPhone: result[
                                                          'data']
                                                          [
                                                          0]['phone'],
                                                        )),
                                              );
                                            }
                                            else if(result['status']['code'] == 1005){
                                              SharedPreferences preferences = await SharedPreferences.getInstance();
                                              preferences.remove('connected');
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          Login()));
                                              showDialog(
                                                  context: context,
                                                  builder: (BuildContext context) {
                                                    return DialogScreen(
                                                      color: Colors.amber,
                                                      icon: Icons.pending_actions,
                                                      title: 'Session',
                                                      message: result['status']['message'],
                                                      message2: '',
                                                      buttonText: 'Close',
                                                    );
                                                  });
                                            }
                                            else {
                                              showDialog(
                                                  context: context,
                                                  builder: (BuildContext
                                                  context) {
                                                    return DialogScreen(
                                                      color: Colors.red,
                                                      icon: Icons
                                                          .error_outline,
                                                      title: 'Error',
                                                      message: result[
                                                      'status']
                                                      ['message'],
                                                      message2: '',
                                                      buttonText:
                                                      'Close',
                                                    );
                                                  });
                                            }
                                          });
                                        }
                                      },
                                      color: Colors.amber,
                                      textColor: Colors.white,
                                      child: Text("next".toUpperCase(),
                                          style: TextStyle(
                                              fontSize: height / 47,
                                              color: Colors.white)),
                                    ),
                                  ),
                                )
                                    : Container(),
                                SizedBox(
                                  height: 10,
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              )
          ),
        ),
      ),
    );
  }
}
