import 'package:flutter/material.dart';
import 'package:flutter_app/views/dashboard/dashboard.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/transfert/transferValidationShop.dart';
import 'package:flutter_app/views/transfert/transfertSource.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TransferShop extends StatefulWidget {
  @override
  _TransferShopState createState() => _TransferShopState();

  String shopId;
  String shopName;

  TransferShop({Key key, @required this.shopId, @required this.shopName})
      : super(key: key);
}

class _TransferShopState extends State<TransferShop> {
  final _amountController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: SafeArea(
            child: Container(
              width: double.infinity,
              color: Color.fromRGBO(234, 239, 255, 3),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).pushReplacement(
                            MaterialPageRoute(
                                builder: (context) => Dashboard()),
                          );

                        },
                        child: Container(
                            margin: EdgeInsets.only(
                                top: height / 15, left: width / 20),
                            child: Icon(
                              Icons.close,
                              size: height / 35,
                            )),
                      ),
                    ],
                  ), //header

                  SizedBox(
                    height: height / 15,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40),
                          topRight: Radius.circular(40),
                        )),
                    child: Column(
                      children: [
                        SizedBox(height: height / 12),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: Text(
                              'Amount of money that must be paid',
                              style: TextStyle(
                                fontFamily: 'DMSans-Bold',
                                fontSize: height * 0.035,
                                color: const Color(0xff172b4d),
                              ),
                              textHeightBehavior: TextHeightBehavior(
                                  applyHeightToFirstAscent: false),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: height / 8.5),
                          child: Text(
                            'TND',
                            style: TextStyle(
                              fontFamily: 'DMSans-Regular',
                              fontSize: height / 45,
                              color: const Color(0xff7a869a),
                              letterSpacing: -0.3999999961853027,
                              height: 1.7142857142857142,
                            ),
                            textHeightBehavior: TextHeightBehavior(
                                applyHeightToFirstAscent: false),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(30, 10, 30, 0),
                          child: TextFormField(
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter amount';
                              }
                              return null;
                            },
                            controller: _amountController,
                            textAlign: TextAlign.center,
                            keyboardType: TextInputType.number,
                            autofocus: false,
                            style: TextStyle(
                                fontSize: height / 40, color: Colors.black),
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Enter the amount',
                              hintStyle: TextStyle(
                                  color: Colors.grey,
                                  fontSize: height / 30,
                                  fontWeight: FontWeight.w300),
                              filled: true,
                              fillColor: Color.fromRGBO(244, 245, 247, 3),
                            ),
                          ),
                        ),
                        Container(
                          height: height / 14.5,
                          width: width / 1.25,
                          margin: EdgeInsets.only(top: height / 3),
                          child: InkWell(
                            onTap: () {},
                            // ignore: deprecated_member_use
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              onPressed: () async {
                                SharedPreferences preferences =
                                    await SharedPreferences.getInstance();
                                print(preferences.getString('userName'));
                                if (_formKey.currentState.validate()) {
                                  if (_amountController.text != "" &&
                                      int.parse(_amountController.text) <
                                          99999.99) {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              TransferValidationShop(
                                                amount: _amountController.text,
                                                shopId: widget.shopId,
                                                shopName: widget.shopName,
                                                from: preferences
                                                    .getString('userName'),
                                              )),
                                    );
                                  } else {
                                    showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return DialogScreen(
                                            color: Colors.red,
                                            icon: Icons.error_outline,
                                            title: 'Error',
                                            message:
                                                'Please enter a valid amount',
                                            message2: '',
                                            buttonText: 'Close',
                                          );
                                        });
                                  }
                                }
                              },
                              color: Colors.amber,
                              textColor: Colors.white,
                              child: Text("Next".toUpperCase(),
                                  style: TextStyle(
                                      fontSize: height / 40,
                                      color: Colors.white)),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
