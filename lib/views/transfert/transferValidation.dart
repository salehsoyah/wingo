import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/linkCardController.dart';
import 'package:flutter_app/controllers/transferController.dart';
import 'package:flutter_app/controllers/userController.dart';
import 'package:flutter_app/models/card.dart';
import 'package:flutter_app/views/dashboard/dashboard.dart';
import 'package:flutter_app/views/dialog/checkcredentials.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/dialog/transfertSuccessDialog.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TransferValidation extends StatefulWidget {
  String amount;
  String from;
  String toName;
  String toPhone;

  static var routeName = 'transferValidation';

  TransferValidation(
      {Key key,
      @required this.amount,
      @required this.from,
      @required this.toName,
      @required this.toPhone})
      : super(key: key);

  @override
  _TransferValidationState createState() => _TransferValidationState();
}

class _TransferValidationState extends State<TransferValidation> {
  String _selected;
  List<BankCard> cards;

  List<BankCard> _myJson = [];
  String from_name;
  String testRec;

  Future userInfo;

  void initState() {
    getUserInfo().then((result) async {
      if(result['status']['code'] == 200){
        userInfo = getUserInfo();
      }else if(result['status']['code'] == 1005){
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.remove('connected');
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    Login()));
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return DialogScreen(
                color: Colors.amber,
                icon: Icons.pending_actions,
                title: 'Session',
                message: result['status']['message'],
                message2: '',
                buttonText: 'Close',
              );
            });
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: SafeArea(
          child: FutureBuilder(
            future: userInfo,
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (!snapshot.hasData) {
                return Container(
                  height: height,
                  child: SpinKitFadingCircle(
                    itemBuilder: (BuildContext context, int index) {
                      return DecoratedBox(
                        decoration: BoxDecoration(
                          color: index.isEven ? Colors.yellow : Colors.blue,
                        ),
                      );
                    },
                  ),
                );
              } else {
                return Container(
                  width: double.infinity,
                  color: Color.fromRGBO(234, 239, 255, 3),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.of(context).pop();

                            },
                            child: Container(
                                margin: EdgeInsets.only(
                                    top: height / 15, left: width / 20),
                                child: Icon(
                                  Icons.close,
                                  size: height / 35,
                                )),
                          ),
                        ],
                      ), //header

                      SizedBox(
                        height: MediaQuery.of(context).size.width / 7,
                      ),
                      Column(
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(25),
                                  topRight: Radius.circular(25),
                                  bottomLeft: Radius.circular(25),
                                  bottomRight: Radius.circular(25),
                                )),
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 30,
                                ),
                                // Container(
                                //   width: MediaQuery.of(context).size.width * 0.90,
                                //   child: DropdownButtonHideUnderline(child: ButtonTheme(
                                //     alignedDropdown: true,
                                //     child: DropdownButton(
                                //       hint: Text('Select bank',
                                //         style: TextStyle(
                                //             fontSize: height / 50
                                //         ),
                                //       ),
                                //       value: _myJson.contains(widget.cardNb.toString()) ? widget.cardNb.toString() : _myJson.first.card_number.toString(),
                                //       onChanged: (newValue){
                                //         setState(() {
                                //           _selected = newValue;
                                //           print(_selected);
                                //         });
                                //       },
                                //       items: _myJson.map((bankItem){
                                //         return DropdownMenuItem(
                                //           value: bankItem.card_number.toString(),
                                //           child: Row(
                                //             crossAxisAlignment: CrossAxisAlignment.start,
                                //             children: [
                                //               Image.asset('assets/images/' + bankItem.bank_name.toString() + '.png', width: width / 9),
                                //               Column(
                                //                 crossAxisAlignment: CrossAxisAlignment.start,
                                //                 children: [
                                //                   Container(
                                //                     margin: EdgeInsets.only(left: 10),
                                //                     child: Text((bankItem.bank_name).toString(),
                                //                       style: TextStyle(
                                //                           fontSize: height / 48,
                                //                           fontWeight: FontWeight.w500
                                //                       ),),
                                //                   ),
                                //                   Container(
                                //                     margin: EdgeInsets.only(left: 10),
                                //                     child: Text('****' + (bankItem.card_number).toString().substring(16, 20),
                                //                       style: TextStyle(
                                //                           color: Colors.grey,
                                //                           fontSize: height / 50,
                                //                           fontWeight: FontWeight.w500
                                //                       ),
                                //                     ),
                                //                   ),
                                //                 ],
                                //               )
                                //             ],
                                //           ),
                                //         );
                                //       }).toList(),
                                //     ),
                                //   )),
                                // ),

                                // Divider(
                                //     height: 20,
                                //     thickness: 2,
                                //     indent: 20,
                                //     endIndent: 20),
                                SizedBox(
                                  height: 15,
                                ),
                                Row(
                                  children: [
                                    SizedBox(
                                      width: 15,
                                    ),
                                    Text(
                                      "Transaction Details",
                                      style: TextStyle(
                                          fontSize: height / 47,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 20, right: 20),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    color: Colors.grey[100],
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(20.0),
                                    child: Column(
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "To",
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontSize: height / 47),
                                            ),
                                            Text(
                                              widget.toName,
                                              style: TextStyle(
                                                  fontSize: height / 47),
                                            )
                                          ],
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Divider(
                                          height: 20,
                                          thickness: 2,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text("Amount",
                                                style: TextStyle(
                                                    color: Colors.grey,
                                                    fontSize: height / 47)),
                                            Row(
                                              children: [
                                                Text(
                                                  widget.amount.toString(),
                                                  style: TextStyle(
                                                      fontSize: height / 47),
                                                ),
                                                Text(
                                                  'TND',
                                                  style: TextStyle(
                                                      fontSize: 10,
                                                      fontFeatures: [
                                                        FontFeature.enable(
                                                            'sups'),
                                                      ]),
                                                )
                                              ],
                                            )
                                          ],
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Divider(
                                          height: 20,
                                          thickness: 2,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text("Transaction fee",
                                                style: TextStyle(
                                                    color: Colors.grey,
                                                    fontSize: height / 47)),
                                            Row(
                                              children: [
                                                Text(
                                                  (((snapshot.data['data']
                                                                      ['user']
                                                                  ['level'][
                                                              'transfer_fee']) *
                                                          double.parse(
                                                              widget.amount) /
                                                          100))
                                                      .toString(),
                                                  style: TextStyle(
                                                      fontSize: height / 47),
                                                ),
                                                Text(
                                                  'TND',
                                                  style: TextStyle(
                                                      fontSize: 10,
                                                      fontFeatures: [
                                                        FontFeature.enable(
                                                            'sups'),
                                                      ]),
                                                )
                                              ],
                                            )
                                          ],
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Divider(
                                          height: 20,
                                          thickness: 2,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text("Net amount",
                                                style: TextStyle(
                                                    color: Colors.grey,
                                                    fontSize: height / 47)),
                                            Row(
                                              children: [
                                                Text(
                                                  (double.parse(widget.amount) -
                                                          (((snapshot.data['data']
                                                                              [
                                                                              'user']
                                                                          [
                                                                          'level']
                                                                      [
                                                                      'transfer_fee']) *
                                                                  double.parse(
                                                                      widget
                                                                          .amount)) /
                                                              100))
                                                      .toString(),
                                                  style: TextStyle(
                                                      fontSize: height / 47),
                                                ),
                                                Text(
                                                  'TND',
                                                  style: TextStyle(
                                                      fontSize: 10,
                                                      fontFeatures: [
                                                        FontFeature.enable(
                                                            'sups'),
                                                      ]),
                                                )
                                              ],
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: height / 5,
                                ),
                                Container(
                                  height: height / 13.5,
                                  width: width / 1.25,
                                  child: InkWell(
                                    onTap: () {},
                                    // ignore: deprecated_member_use
                                    child: RaisedButton(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                      onPressed: () async {
                                        SharedPreferences preferences =
                                            await SharedPreferences
                                                .getInstance();
                                        print(widget.toPhone);
                                        showDialog(
                                            context: context,
                                            builder: (context) {
                                              return CheckCredentials();
                                            }).then((checkCode) {
                                          print(checkCode);
                                          if (checkCode == 200) {
                                            storeTransfer(
                                                    preferences
                                                        .getString('idUser'),
                                                    widget.toPhone,
                                                    double.parse(widget.amount))
                                                .then((result) async {
                                              if (result['status']['code'] ==
                                                  200) {
                                                print(result);
                                                showDialog(
                                                    context: context,
                                                    builder:
                                                        (BuildContext context) {
                                                      return TransferSuccess(
                                                        amount: (result['data']
                                                                    ['transfer']
                                                                ['amount'])
                                                            .toString(),
                                                        name: widget.toName,
                                                        fromName: widget.from,
                                                        fee: (result['data']
                                                                    ['transfer']
                                                                ['loyalty'])
                                                            .toString(),
                                                        title: 'Transfer',
                                                      );
                                                    });
                                              }
                                              else if(result['status']['code'] == 1005){
                                                SharedPreferences preferences = await SharedPreferences.getInstance();
                                                preferences.remove('connected');
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            Login()));
                                                showDialog(
                                                    context: context,
                                                    builder: (BuildContext context) {
                                                      return DialogScreen(
                                                        color: Colors.amber,
                                                        icon: Icons.pending_actions,
                                                        title: 'Session',
                                                        message: result['status']['message'],
                                                        message2: '',
                                                        buttonText: 'Close',
                                                      );
                                                    });
                                              }
                                              else {
                                                showDialog(
                                                    context: context,
                                                    builder:
                                                        (BuildContext context) {
                                                      return DialogScreen(
                                                        color: Colors.red,
                                                        icon:
                                                            Icons.error_outline,
                                                        title: 'Error',
                                                        message:
                                                            result['message'],
                                                        message2: '',
                                                        buttonText: 'Close',
                                                      );
                                                    });
                                              }
                                            });
                                          } else {
                                            showDialog(
                                                context: context,
                                                builder:
                                                    (BuildContext context) {
                                                  return DialogScreen(
                                                    color: Colors.red,
                                                    icon: Icons.error_outline,
                                                    title: 'Error',
                                                    message:
                                                        'Unauthorized , Invalid credentials',
                                                    message2: '',
                                                    buttonText: 'Close',
                                                  );
                                                });
                                          }
                                        });
                                      },
                                      color: Colors.amber,
                                      textColor: Colors.white,
                                      child: Text(
                                          "press & hold to transfer"
                                              .toUpperCase(),
                                          style: TextStyle(
                                              fontSize: height / 47,
                                              color: Colors.white)),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                )
                              ],
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                );
              }
            },
          ),
        ),
      ),
    );
  }
}
