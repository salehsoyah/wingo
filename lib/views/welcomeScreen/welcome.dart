import 'dart:async';

import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:flutter_svg/flutter_svg.dart';

class welcome extends StatelessWidget {
  final VoidCallback w_logo;

  welcome({
    Key key,
    this.w_logo,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Timer(Duration(seconds: 1), () async {

      Navigator.of(context).pushReplacement(
          MaterialPageRoute(
              builder: (BuildContext context) =>
                  Login())); //It will redirect  after 3 seconds
    });
    return Scaffold(
        backgroundColor: const Color(0xff0e4dfb),
        body: Container(
          alignment: Alignment.center,
          child: SafeArea(
            child: Container(
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.white.withOpacity(0.4),
                    spreadRadius: 10,
                    blurRadius: 30,
                    offset: Offset(0, 20), // changes position of shadow
                  ),
                ],
              ),
              child: Image(
                width: MediaQuery.of(context).size.width / 3,
                image: AssetImage("assets/images/logo.PNG",
                 ),
              )
            ),
          ),
        ));
  }
}
