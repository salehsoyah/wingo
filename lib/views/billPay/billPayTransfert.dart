import 'package:flutter/material.dart';
import 'package:flutter_app/views/dashboard/customBottomNavigationBar.dart';
import 'package:flutter_app/views/dashboard/homeTopTabs.dart';
import 'package:flutter_app/views/topUpScreens/topUpDeposit.dart';
import 'package:flutter_app/views/transfert/transfert.dart';

class BillPayTransfert extends StatefulWidget {
  @override
  _BillPayTransfert createState() => _BillPayTransfert();
}

class _BillPayTransfert extends State<BillPayTransfert> {

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 1,
      initialIndex: 0,
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height * 1.7,
            width: double.infinity,
            color: Colors.white,
            child: Stack(
              children: [
                SafeArea(
                  child: SizedBox(

                    height: MediaQuery.of(context).size.height * 2,
                    child: Column(
                      children: <Widget>[
                        Center(
                          child: Container(
                            margin:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(40),
                                  topRight: Radius.circular(40),
                                )),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(width: 7,),

                                Expanded(
                                  flex: 0,
                                  child: Row(),
                                )
                              ],
                            ),
                          ),
                        ),
                        Center(
                          child: Container(
                              margin: EdgeInsets.only(
                                top: MediaQuery.of(context).size.width / 30,
                              ),
                              child: Text(
                                "Bill Pay",
                                style: TextStyle(fontSize: 30),
                              )),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.width / 7,
                        ),
                        SizedBox(
                          height: 50,
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(top: MediaQuery.of(context).size.height/100),
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(40),
                                  topRight: Radius.circular(40),
                                )),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(left: 20, top: 15),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 25),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Column(
                                      children: [
                                        SizedBox.fromSize(
                                          size: Size(75, 60),
                                          // button width and height
                                          child: ClipRRect(
                                            borderRadius: BorderRadius.all(Radius.circular(15.0)),
                                            child: Material(
                                              child: InkWell(
                                                splashColor: Colors.lightBlue, // splash color
                                                onTap: () {}, // button pressed
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    Icon(Icons.lightbulb,color: Colors.amber,size: 30,),
                                                    // icon
                                                    // text
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: 8),
                                        Text("Electricite")
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        SizedBox.fromSize(
                                          size: Size(75, 60),
                                          // button width and height
                                          child: ClipRRect(
                                            borderRadius: BorderRadius.all(Radius.circular(15.0)),
                                            child: Material(
                                              child: InkWell(
                                                splashColor: Colors.lightBlue, // splash color
                                                onTap: () {}, // button pressed
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    Icon(Icons.account_circle,color: Colors.blue,size: 30,),
                                                    // icon
                                                    // text
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: 8),
                                        Text("Sonede")
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        SizedBox.fromSize(
                                          size: Size(75, 60),
                                          // button width and height
                                          child: ClipRRect(
                                            borderRadius: BorderRadius.all(Radius.circular(15.0)),
                                            child: Material(
                                              child: InkWell(
                                                splashColor: Colors.lightBlue, // splash color
                                                onTap: () {}, // button pressed
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    Icon(Icons.wifi,color: Colors.black,size: 30,),
                                                    // icon
                                                    // text
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: 8),
                                        Text("Internet")
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        SizedBox.fromSize(
                                          size: Size(75, 60),
                                          // button width and height
                                          child: ClipRRect(
                                            borderRadius: BorderRadius.all(Radius.circular(15.0)),
                                            child: Material(
                                              child: InkWell(
                                                splashColor: Colors.lightBlue, // splash color
                                                onTap: () {}, // button pressed
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    Icon(Icons.tv_outlined,color: Colors.grey,size: 30,),
                                                    // icon
                                                    // text
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: 8),
                                        Text("Television")
                                      ],
                                    ),
                                  ],
                                ),
                                SizedBox(height: 20,),
                                Divider(
                                  height: 20,
                                  thickness: 0.05,
                                  color: Colors.black,
                                ),
                                Row(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(left: 20,top:15),
                                      child: Text("This month",style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600
                                      ),),
                                    )
                                  ],
                                ),
                                SizedBox(height: 20,),
                                Divider(
                                  height: 20,
                                  thickness: 0.05,
                                  color: Colors.black,
                                ),

                                SizedBox(height: 15,),
                                Container(
                                  margin: EdgeInsets.only(left: 5,bottom: 5),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          SizedBox(
                                            height: MediaQuery.of(context).size.height/20,
                                            width: 380,
                                            child: ListTile(
                                              leading: IconButton(
                                                  icon: const Icon(Icons.lightbulb,
                                                    color: Colors.amber,
                                                    size: 25,
                                                  ),
                                                  onPressed: null
                                              ),
                                              title: Text('Electricity',style: TextStyle(fontWeight: FontWeight.w200),),
                                              subtitle: Text('CP-6567136'),
                                              trailing: Text("- 10.00",style: TextStyle(color: Colors.red,),),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(height: 25,),
                                      Divider(
                                        height: 20,
                                        thickness: 0.05,
                                        color: Colors.black,
                                      ),
                                      Row(
                                        children: [
                                          SizedBox(
                                            height: MediaQuery.of(context).size.height/20,
                                            width: 380,
                                            child: ListTile(
                                              leading: IconButton(
                                                  icon: const Icon(Icons.account_circle,
                                                    color: Colors.blue,
                                                    size: 25,
                                                  ),
                                                  onPressed: null
                                              ),
                                              title: Text('Sonede',style: TextStyle(fontWeight: FontWeight.w200),),

                                              subtitle: Text('CP-6567136'),
                                              trailing: Text("- 10.00",style: TextStyle(color: Colors.red,),),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(height: 15,),
                                      Divider(
                                        height: 20,
                                        thickness: 0.05,
                                        color: Colors.black,
                                      ),
                                      Row(
                                        children: [
                                          SizedBox(
                                            height: MediaQuery.of(context).size.height/20,
                                            width: 380,
                                            child: ListTile(
                                              leading: IconButton(
                                                  icon: const Icon(Icons.tv_outlined,
                                                    color: Colors.grey,
                                                    size: 25,
                                                  ),
                                                  onPressed: null
                                              ),
                                              title: Text('Television',style: TextStyle(fontWeight: FontWeight.w200),),
                                              subtitle: Text('CP-6567136'),
                                              trailing: Text("+ 10.00",style: TextStyle(color: Colors.green,),),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 30,
                                      ),

                                    ],
                                  ),
                                ),
                                SizedBox(height: 20,),
                                Divider(
                                  height: 20,
                                  thickness: 0.05,
                                  color: Colors.black,
                                ),
                                Row(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(left: 20, top: 15),
                                      child: Text("Last Month", style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500
                                      ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 25,
                                ),
                                Divider(
                                  height: 20,
                                  thickness: 0.05,
                                  color: Colors.black,
                                ),
                                Row(
                                  children: [
                                    SizedBox(
                                      height: MediaQuery.of(context).size.height/20,
                                      width: 380,
                                      child: ListTile(
                                        leading: IconButton(
                                            icon: const Icon(Icons.tv_outlined,
                                              color: Colors.grey,
                                              size: 25,
                                            ),
                                            onPressed: null
                                        ),
                                        title: Text('Television',style: TextStyle(fontWeight: FontWeight.w200),),
                                        subtitle: Text('CP-6567136'),
                                        trailing: Text("+ 20.00",style: TextStyle(color: Colors.green,),),
                                      ),
                                    ),
                                  ],
                                ),

                              ],
                            ),
                          ),
                        ),

                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

