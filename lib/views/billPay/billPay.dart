import 'package:flutter/material.dart';
import 'package:flutter_app/views/dialog/dialog.dart';

class BillPay extends StatefulWidget {
  @override
  _BillPay createState() => _BillPay();
}

class _BillPay extends State<BillPay> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          color: Color.fromRGBO(234, 239, 255, 3),
          child: Stack(
            children: [
              SafeArea(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: MediaQuery.of(context).size.width / 5,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(40),
                            topRight: Radius.circular(40),
                          )),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(
                                top: MediaQuery.of(context).size.width / 4.5),
                            child: Text(
                              'Bill Pay',
                              style: TextStyle(
                                fontFamily: 'DMSans-Regular',
                                fontSize: 26,
                                color: const Color(0xff7a869a),
                                letterSpacing: 0.3999999961853027,
                                height: 1.7142857142857142,
                              ),
                              textHeightBehavior: TextHeightBehavior(
                                  applyHeightToFirstAscent: false),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.width / 7,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Column(
                                children: [
                                  SizedBox.fromSize(
                                    size: Size(75, 60),
                                    // button width and height
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.all(Radius.circular(15.0)),
                                      child: Material(
                                        child: InkWell(
                                          splashColor: Colors.lightBlue, // splash color
                                          onTap: () {}, // button pressed
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Icon(Icons.lightbulb,color: Colors.amber,size: 30,),
                                              // icon
                                              // text
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 8),
                                  Text("Electricite")
                                ],
                              ),
                              Column(
                                children: [
                                  SizedBox.fromSize(
                                    size: Size(75, 60),
                                    // button width and height
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.all(Radius.circular(15.0)),
                                      child: Material(
                                        child: InkWell(
                                          splashColor: Colors.lightBlue, // splash color
                                          onTap: () {}, // button pressed
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Icon(Icons.add,color: Colors.blue,size: 30,),
                                              // icon
                                              // text
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 8),
                                  Text("Sonede")
                                ],
                              ),
                              Column(
                                children: [
                                  SizedBox.fromSize(
                                    size: Size(75, 60),
                                    // button width and height
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.all(Radius.circular(15.0)),
                                      child: Material(
                                        child: InkWell(
                                          splashColor: Colors.lightBlue, // splash color
                                          onTap: () {}, // button pressed
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Icon(Icons.wifi,color: Colors.black,size: 30,),
                                              // icon
                                              // text
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 8),
                                  Text("Internet")
                                ],
                              ),
                              Column(
                                children: [
                                  SizedBox.fromSize(
                                    size: Size(75, 60),
                                    // button width and height
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.all(Radius.circular(15.0)),
                                      child: Material(
                                        child: InkWell(
                                          splashColor: Colors.lightBlue, // splash color
                                          onTap: () {}, // button pressed
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Icon(Icons.tv_outlined,color: Colors.grey,size: 30,),
                                              // icon
                                              // text
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 8),
                                  Text("Television")
                                ],
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 55,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.width * 1.299,
                            decoration: BoxDecoration(
                                color: Colors.grey[200],
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(40),
                                  topRight: Radius.circular(40),
                                )),
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 35,
                                ),
                                Container(
                                  width:
                                  MediaQuery.of(context).size.width / 1.2,
                                  height: MediaQuery.of(context).size.width /0.9,

                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(25),
                                        topRight: Radius.circular(25),
                                        bottomLeft: Radius.circular(25),
                                        bottomRight: Radius.circular(25),
                                      )),
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.stretch,
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                    children: [
                                      SizedBox(
                                        height: 15,
                                      ),

                                      Padding(
                                        padding: const EdgeInsets.only(bottom: 0.0),
                                        child: ListTile(
                                          leading: Image(
                                            image: AssetImage(
                                                "assets/images/bank of.png"),
                                          ),
                                          title: Padding(
                                            padding: const EdgeInsets.only(top:1.0,bottom: 10),
                                            child: Text(
                                              'Company',
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w300),
                                            ),
                                          ),
                                          subtitle: Text('****9999'),
                                          trailing: Icon(Icons.keyboard_arrow_down_sharp),
                                        ),
                                      ),
                                      Divider(
                                          height: 20,
                                          thickness: 2,
                                          indent: 20,
                                          endIndent: 20
                                      ),

                                      Container(
                                        margin: EdgeInsets.only(
                                            left:
                                            MediaQuery.of(context).size.width / 12,
                                          bottom: MediaQuery.of(context).size.width/15
                                            ),
                                        child: Text(
                                          "Custumor's code",
                                          style: TextStyle(
                                              color: Color.fromRGBO(193, 199, 208, 3)),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(bottom: 35,left: 25,right: 25,top: 0),
                                        child: TextField(
                                          autofocus: false,
                                          style: TextStyle(
                                              fontSize: 15.0,
                                              color: Color.fromRGBO(193, 199, 208, 3)),
                                          decoration: InputDecoration(
                                            prefixIcon: Icon(Icons.lightbulb),
                                            suffixIcon: Icon(
                                              Icons.done,
                                              color: Colors.amber,
                                            ),
                                            border: InputBorder.none,
                                            hintText: 'Enter your code',
                                            filled: true,
                                            fillColor: Color.fromRGBO(244, 245, 247, 3),
                                            contentPadding: const EdgeInsets.only(
                                                left: 20.0, bottom: 6.0, top: 15.0),
                                            focusedBorder: OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(15.0),
                                            ),
                                            enabledBorder: UnderlineInputBorder(
                                              borderRadius: BorderRadius.circular(15.0),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        height:
                                        MediaQuery.of(context).size.width / 8.5,
                                        width:
                                        MediaQuery.of(context).size.width / 1.25,
                                        margin: EdgeInsets.fromLTRB(25, 20, 25, 45),
                                        child: InkWell(
                                          onTap: () {},
                                          // ignore: deprecated_member_use
                                          child: RaisedButton(
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                              BorderRadius.circular(15.0),
                                            ),
                                            onPressed: () {
                                              showDialog(
                                                  context: context,
                                                  builder: (BuildContext context) {
                                                    return DialogScreen(
                                                      color: Colors.amber,
                                                      icon: Icons.money_outlined,
                                                      title: 'Successful deposit into wallet',
                                                      message: 'Now you can comfortably shop, eat and drink.',
                                                      message2 :'Enjoy it',
                                                      buttonText: 'GO BACK TO HOME',
                                                    );
                                                  });
                                            },
                                            color: Colors.amber,
                                            textColor: Colors.white,
                                            child: Text("Next".toUpperCase(),
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    color: Colors.white)),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),

                                ),



                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
