import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/linkCardController.dart';
import 'package:flutter_app/controllers/transferController.dart';
import 'package:flutter_app/controllers/userController.dart';
import 'package:flutter_app/views/dialog/checkcredentials.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/dialog/enterCard.dart';
import 'package:flutter_app/views/dialog/transaction.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_app/models/card.dart';

class TopUpDepositSource extends StatefulWidget {
  double amount;

  TopUpDepositSource({Key key, @required this.amount}) : super(key: key);

  @override
  _TopUpDepositSourceState createState() => _TopUpDepositSourceState();
}

class _TopUpDepositSourceState extends State<TopUpDepositSource> {
  String _selected;
  List<BankCard> cards;

  List<BankCard> _myJson = [];

  void initState() {
    getUserInfo().then((value) async {
      if(value['status']['code'] == 200){
        indexCards().then((result) {
          print(result);
          setState(() {
            for (var u in result) {
              _myJson.add(u);
            }
          });
        });
      }else if(value['status']['code'] == 1005){
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.remove('connected');
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    Login()));
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return DialogScreen(
                color: Colors.amber,
                icon: Icons.pending_actions,
                title: 'Session',
                message: value['status']['message'],
                message2: '',
                buttonText: 'Close',
              );
            });
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            width: double.infinity,
            color: Color.fromRGBO(234, 239, 255, 3),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();

                      },
                      child: Container(
                          margin: EdgeInsets.only(
                              top: height / 15, left: width / 20),
                          child: Icon(
                            Icons.close,
                            size: height / 35,
                          )),
                    ),
                  ],
                ), //header

                SizedBox(
                  height: height / 14,
                ),
                Container(
                  width: width,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40),
                      )),
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: height / 8.5),
                        child: Text(
                          'TND',
                          style: TextStyle(
                            fontFamily: 'DMSans-Regular',
                            fontSize: height / 40,
                            color: const Color(0xff7a869a),
                            letterSpacing: -0.3999999961853027,
                            height: 1.7142857142857142,
                          ),
                          textHeightBehavior: TextHeightBehavior(
                              applyHeightToFirstAscent: false),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.fromLTRB(30, 10, 30, 0),
                          child: Text(
                            widget.amount.toString(),
                            style: TextStyle(
                                fontSize: height / 25, color: Colors.black),
                          )),
                      SizedBox(
                        height: 55,
                      ),
                      Container(
                        width: width,
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(40),
                              topRight: Radius.circular(40),
                            )),
                        child: Column(
                          children: [
                            SizedBox(
                              height: 35,
                            ),
                            Container(
                              width: width / 1.09,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(25),
                                    topRight: Radius.circular(25),
                                    bottomLeft: Radius.circular(25),
                                    bottomRight: Radius.circular(25),
                                  )),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 14.0, bottom: 12.0, left: 15),
                                    child: Text(
                                      "Money Source",
                                      style: TextStyle(
                                          fontSize: height / 45,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),

                                  Divider(
                                      height: 4,
                                      thickness: 2,
                                      indent: 20,
                                      endIndent: 20),
                                 SizedBox(height: 5,),
                                  DropdownButtonHideUnderline(
                                      child: ButtonTheme(
                                    alignedDropdown: true,
                                    child: DropdownButton(
                                      hint: Text(
                                        'Select bank',
                                        style: TextStyle(fontSize: height / 45),
                                      ),
                                      value: _selected,
                                      onChanged: (newValue) {
                                        setState(() {
                                          print(newValue);
                                          _selected = newValue;
                                        });
                                      },
                                      items: _myJson.map((bankItem) {
                                        return DropdownMenuItem(
                                          value: bankItem.id.toString(),
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Image.asset(
                                                  'assets/images/' +
                                                      bankItem.bank_name
                                                          .toString() +
                                                      '.png',
                                                  width: 25),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        left: 10),
                                                    child: Text(
                                                      (bankItem.bank_name)
                                                          .toString(),
                                                      style: TextStyle(
                                                          fontSize: height / 50,
                                                          fontWeight:
                                                              FontWeight.w500),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        left: 10),
                                                    child: Text(
                                                      '****' +
                                                          (bankItem.card_number)
                                                              .toString()
                                                              .substring(
                                                                  16, 20),
                                                      style: TextStyle(
                                                          color: Colors.grey,
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.w500),
                                                    ),
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                        );
                                      }).toList(),
                                    ),
                                  ))
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 25,
                            ),
                            Container(
                              width: width / 1.09,
                              height: height / 4.8,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(25),
                                    topRight: Radius.circular(25),
                                    bottomLeft: Radius.circular(25),
                                    bottomRight: Radius.circular(25),
                                  )),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Row(
                                    children: [
                                      SizedBox(
                                        width: 15,
                                      ),
                                      Text(
                                        "Information",
                                        style: TextStyle(
                                            fontSize: height / 45,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Divider(
                                      height: 20,
                                      thickness: 2,
                                      indent: 20,
                                      endIndent: 20),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 16, right: 14),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "Amount",
                                          style:
                                              TextStyle(fontSize: height / 50),
                                        ),
                                        Text(
                                          widget.amount.toString(),
                                          style:
                                              TextStyle(fontSize: height / 50),
                                        )
                                      ],
                                    ),
                                  ),
                                  Divider(
                                      height: 20,
                                      thickness: 2,
                                      indent: 20,
                                      endIndent: 20),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 16, right: 14),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "Fee",
                                          style:
                                              TextStyle(fontSize: height / 50),
                                        ),
                                        Text(
                                          "Free",
                                          style: TextStyle(
                                              color: Colors.amber,
                                              fontSize: height / 50),
                                        )
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 7,
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 25,
                            ),
                            Container(
                              height: height / 13.5,
                              width: width / 1.25,
                              child: InkWell(
                                onTap: () {},
                                // ignore: deprecated_member_use
                                child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  onPressed: () async {
                                    SharedPreferences preferences =
                                        await SharedPreferences.getInstance();

                                    if(_selected != null){
                                      showDialog(context: context, builder: (context){
                                        return CheckCredentials();
                                      }).then((checkCode){
                                        print(checkCode);
                                        if(checkCode == 200){
                                          storeDeposit(preferences.getString('idUser'), widget.amount, _selected)
                                              .then((result) async {
                                            print(result);
                                            if (result['status']['code'] == 200) {
                                              showDialog(
                                                  context: context,
                                                  builder: (BuildContext context) {
                                                    return Transaction(icon: Icons.arrow_circle_down_outlined, title: 'Successful deposit into wallet', message: 'Now you can comfortably shop, eat and drink. Enjoy it! ');
                                                  });
                                            }else if(result['status']['code'] == 1005){
                                              SharedPreferences preferences = await SharedPreferences.getInstance();
                                              preferences.remove('connected');
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          Login()));
                                              showDialog(
                                                  context: context,
                                                  builder: (BuildContext context) {
                                                    return DialogScreen(
                                                      color: Colors.amber,
                                                      icon: Icons.pending_actions,
                                                      title: 'Session',
                                                      message: result['status']['message'],
                                                      message2: '',
                                                      buttonText: 'Close',
                                                    );
                                                  });
                                            }
                                            else{
                                              showDialog(
                                                  context: context,
                                                  builder: (BuildContext context) {
                                                    return DialogScreen(
                                                      color: Colors.red,
                                                      icon: Icons.error_outline,
                                                      title: 'Error',
                                                      message: result['message'],
                                                      message2: '',
                                                      buttonText: 'Close',
                                                    );
                                                  });
                                            }
                                          });
                                        }else{
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return DialogScreen(
                                                  color: Colors.red,
                                                  icon: Icons.error_outline,
                                                  title: 'Error',
                                                  message: 'Unauthorized , Invalid credentials',
                                                  message2: '',
                                                  buttonText: 'Close',
                                                );
                                              });
                                        }
                                      });
                                    }else{
                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return EnterCard();
                                          });
                                    }


                                  },
                                  color: Colors.amber,
                                  textColor: Colors.white,
                                  child: Text("Deposit".toUpperCase(),
                                      style: TextStyle(
                                          fontSize: height / 45, color: Colors.white)),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
