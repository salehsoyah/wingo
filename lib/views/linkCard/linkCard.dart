import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/authController.dart';
import 'package:flutter_app/controllers/linkCardController.dart';
import 'package:flutter_app/views/dialog/checkcredentials.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/linkCard/saveLinkedCard.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:flutter_app/views/signinScreens/otp_received.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LinkCard extends StatefulWidget {
  @override
  _LinkCardState createState() => _LinkCardState();
}

class _LinkCardState extends State<LinkCard> {
  final _cardholderNameController = TextEditingController();
  final _cardNumberController = TextEditingController();
  final _monthController = TextEditingController();
  final _yearController = TextEditingController();


  String dropdownValue = 'UBCI';
  bool loading = false;

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Color.fromRGBO(234, 239, 255, 3),
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              SizedBox(
                height: height / 35,
              ),
              Expanded(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40),
                      )),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(top: height / 15),
                          child: Image(
                            image: AssetImage("assets/images/visa_card.png"),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(top: height / 16),
                                    child: Text(
                                      "Cardholder Name",
                                      style: TextStyle(
                                          color:
                                              Color.fromRGBO(193, 199, 208, 3),
                                          fontSize: height / 50),
                                    ),
                                  ),
                                  Container(
                                    width: width / 2.5,
                                    child: TextFormField(
                                      validator: (value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Please enter cardholder name';
                                        }
                                        return null;
                                      },
                                      controller: _cardholderNameController,
                                      autofocus: false,
                                      style: TextStyle(
                                          fontSize: height / 50,
                                          color: Colors.black),
                                      decoration: InputDecoration(
                                        prefixIcon: Icon(Icons.account_circle),
                                        border: InputBorder.none,
                                        hintText: 'foulen ben foulen',
                                        filled: true,
                                        fillColor:
                                            Color.fromRGBO(244, 245, 247, 3),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                        ),
                                        enabledBorder: UnderlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(top: height / 16),
                                    child: Text(
                                      "Bank Name",
                                      style: TextStyle(
                                          color:
                                              Color.fromRGBO(193, 199, 208, 3),
                                          fontSize: height / 50),
                                    ),
                                  ),
                                  Container(
                                    width: width / 2.5,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20),
                                      color: Color.fromRGBO(244, 245, 247, 3),
                                    ),
                                    child: DropdownButtonFormField<String>(
                                      style: TextStyle(
                                          fontSize: 13, color: Colors.black),
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                        ),
                                        enabledBorder: UnderlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                        ),
                                        errorBorder: InputBorder.none,
                                        disabledBorder: InputBorder.none,
                                        filled: true,
                                        hintStyle:
                                            TextStyle(color: Colors.grey[800]),
                                        hintText: "Name",
                                      ),
                                      value: dropdownValue,
                                      items: <String>[
                                        'Amen Bank',
                                        'UBCI',
                                        'BIAT',
                                      ].map<DropdownMenuItem<String>>(
                                          (String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value),
                                        );
                                      }).toList(),
                                      onChanged: (String newValue) {
                                        setState(() {
                                          dropdownValue = newValue;
                                        });
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(
                                    top:
                                        MediaQuery.of(context).size.width / 20),
                                child: Text(
                                  "Card Number",
                                  style: TextStyle(
                                      color: Color.fromRGBO(193, 199, 208, 3),
                                      fontSize: height / 50),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Container(
                            child: TextFormField(
                              keyboardType: TextInputType.number,
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Please enter card number';
                                }else if(value.length < 20 || value.length > 20){
                                  return 'Card should have 20 characters';
                                }
                                return null;
                              },
                              controller: _cardNumberController,
                              autofocus: false,
                              style: TextStyle(
                                  fontSize: height / 50, color: Colors.black),
                              decoration: InputDecoration(
                                prefixIcon: Icon(Icons.apps),
                                suffixIcon: Icon(
                                  Icons.done,
                                  color: Colors.amber,
                                ),
                                border: InputBorder.none,
                                hintText: '4509 2097 9956 6997 6589',
                                filled: true,
                                fillColor: Color.fromRGBO(244, 245, 247, 3),
                                contentPadding: const EdgeInsets.only(
                                    left: 20.0, bottom: 6.0, top: 15.0),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                enabledBorder: UnderlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                      top: height / 26,
                                    ),
                                    child: Text(
                                      "Month",
                                      style: TextStyle(
                                          color:
                                              Color.fromRGBO(193, 199, 208, 3),
                                          fontSize: height / 50),
                                    ),
                                  ),
                                  Container(
                                    width: height / 5,
                                    child: TextFormField(
                                      keyboardType: TextInputType.number,
                                      validator: (value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Please enter month';
                                        }else if(value.length < 2 || value.length > 2){
                                          return 'Min 2 characters';
                                        }
                                        return null;
                                      },
                                      controller: _monthController,
                                      obscureText: false,
                                      autofocus: false,
                                      style: TextStyle(
                                          fontSize: height / 50,
                                          color: Colors.black),
                                      decoration: InputDecoration(
                                        prefixIcon: Icon(Icons.calendar_today),
                                        border: InputBorder.none,
                                        hintText: '05',
                                        filled: true,
                                        fillColor:
                                            Color.fromRGBO(244, 245, 247, 3),
                                        contentPadding: const EdgeInsets.only(
                                            left: 20.0, bottom: 6.0, top: 15.0),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                        ),
                                        enabledBorder: UnderlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(
                                          top: height / 26,
                                        ),
                                        child: Text(
                                          "Year",
                                          style: TextStyle(
                                              color: Color.fromRGBO(
                                                  193, 199, 208, 3),
                                              fontSize: height / 50),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Container(
                                    width: height / 5,
                                    child: TextFormField(
                                      keyboardType: TextInputType.number,
                                      validator: (value) {
                                        if (value == null || value.isEmpty ) {
                                          return 'Please enter year';
                                        }else if(value.length < 4 || value.length > 4){
                                          return 'Min 4 characters';
                                        }
                                        return null;
                                      },
                                      controller: _yearController,
                                      obscureText: false,
                                      autofocus: false,
                                      style: TextStyle(
                                          fontSize: height / 50,
                                          color: Colors.black),
                                      decoration: InputDecoration(
                                        prefixIcon: Icon(Icons.calendar_today),
                                        border: InputBorder.none,
                                        hintText: '2021',
                                        filled: true,
                                        fillColor:
                                            Color.fromRGBO(244, 245, 247, 3),
                                        contentPadding: const EdgeInsets.only(
                                            left: 20.0, bottom: 6.0, top: 15.0),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                        ),
                                        enabledBorder: UnderlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        // Container(
                        //   margin: EdgeInsets.only(left: 10, top: height / 40),
                        //   child: Row(
                        //     children: [
                        //       Checkbox(
                        //         value: checkboxValue,
                        //         onChanged: (val) {
                        //           setState(() {
                        //             checkboxValue = val;
                        //           });
                        //           print(checkboxValue);
                        //         },
                        //       ),
                        //       Container(
                        //           height: height / 35,
                        //           width: width / 1.25,
                        //           child: Text(
                        //             "Remember this card details.",
                        //             style: TextStyle(fontSize: height / 45),
                        //           )),
                        //     ],
                        //   ),
                        // ),
                        Container(
                          height: height / 14,
                          width: width / 1.25,
                          margin: EdgeInsets.only(top: height / 20),
                          child: InkWell(
                            onTap: () {},
                            // ignore: deprecated_member_use
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              onPressed: () async {
                                if (_formKey.currentState.validate()) {
                                  setState(() {
                                    loading = true;
                                  });
                                  SharedPreferences preferences =
                                      await SharedPreferences.getInstance();

                                  showDialog(
                                      context: context,
                                      builder: (context) {
                                        return CheckCredentials();
                                      }).then((checkCode) {
                                    print(checkCode);
                                    if (checkCode == 200) {
                                      storeCard(
                                              preferences.getString('idUser'),
                                              _cardholderNameController.text,
                                              _cardNumberController.text,
                                              dropdownValue,
                                              _monthController.text,
                                              _yearController.text)
                                          .then((result) async {
                                        setState(() {
                                          loading = false;
                                        });
                                        if (result['status']['code'] == 402) {
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return DialogScreen(
                                                  color: Colors.red,
                                                  icon: Icons.error_outline,
                                                  title: 'Error',
                                                  message: result['message'],
                                                  message2:
                                                      'Please enter a valid card number',
                                                  buttonText: 'Close',
                                                );
                                              });
                                        } else if (result['status']['code'] ==
                                            404) {
                                          String messageBody = "";
                                          if (result['message']
                                              .containsKey('cardholder_name')) {
                                            messageBody += result['message']
                                                    ['cardholder_name'][0] +
                                                "\n";
                                          }
                                          if (result['message']
                                              .containsKey('card_number')) {
                                            messageBody += result['message']
                                                    ['card_number'][0] +
                                                "\n";
                                          }
                                          if (result['message']
                                              .containsKey('bank_name')) {
                                            messageBody += result['message']
                                                    ['bank_name'][0] +
                                                "\n";
                                          }
                                          if (result['message']
                                              .containsKey('month')) {
                                            messageBody += result['message']
                                                    ['month'][0] +
                                                "\n";
                                          }
                                          if (result['message']
                                              .containsKey('year')) {
                                            messageBody += result['message']
                                                    ['year'][0] +
                                                "\n";
                                          }
                                          print(result['message']);
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return DialogScreen(
                                                  color: Colors.red,
                                                  icon: Icons.error_outline,
                                                  title: 'Error',
                                                  message: messageBody,
                                                  message2: '',
                                                  buttonText: 'Close',
                                                );
                                              });
                                        } else if ((result['status']['code'] ==
                                                200) ) {
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return SaveLinkedCard();
                                              });
                                        }else if(result['status']['code'] == 1005){
                                          SharedPreferences preferences = await SharedPreferences.getInstance();
                                          preferences.remove('connected');
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      Login()));
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return DialogScreen(
                                                  color: Colors.amber,
                                                  icon: Icons.pending_actions,
                                                  title: 'Session',
                                                  message: result['status']['message'],
                                                  message2: '',
                                                  buttonText: 'Close',
                                                );
                                              });
                                        }
                                      });
                                    } else {
                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return DialogScreen(
                                              color: Colors.red,
                                              icon: Icons.error_outline,
                                              title: 'Error',
                                              message:
                                                  'Unauthorized , Invalid credentials',
                                              message2: '',
                                              buttonText: 'Close',
                                            );
                                          });
                                    }
                                  });
                                }
                              },
                              color: Colors.amber,
                              textColor: Colors.white,
                              child: Text("next".toUpperCase(),
                                  style: TextStyle(
                                      fontSize: height / 49,
                                      color: Colors.white)),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 100,
                        )
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
