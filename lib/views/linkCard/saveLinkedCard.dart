import 'package:flutter/material.dart';
import 'package:flutter_app/views/linkCard/linkedCard.dart';

class SaveLinkedCard extends StatefulWidget {
  @override
  _SaveLinkedCardState createState() => _SaveLinkedCardState();
}

class _SaveLinkedCardState extends State<SaveLinkedCard> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
      child: Stack(
        overflow: Overflow.visible,
        alignment: Alignment.topCenter,
        children: [
          Container(
            height: 210,
            width: 500,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 65, 10, 10),
              child: Column(
                children: [
                  Text(
                    'Card Added Succefully',
                    style: TextStyle(fontWeight: FontWeight.w500, fontSize: 18),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    'Now you can freely pay and shop , enjoy',
                    style: TextStyle(fontSize: 14, color: Colors.grey),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  FlatButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => LinkedCard()));
                    },
                    color: Colors.transparent,
                    child: Text(
                      'CLICK & CONTINUE',
                      style: TextStyle(color: Colors.amber, fontSize: 16),
                    ),
                  )
                ],
              ),
            ),
          ),
          Positioned(
              top: -52,
              child: Container(
                width: 100,
                height: 100,
                decoration: BoxDecoration(
                  color: Colors.amber,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Icon(
                  Icons.credit_card,
                  //color: Colors.white,
                  size: 45,
                ),
              )),
        ],
      ),
    );
  }
}
