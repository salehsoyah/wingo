import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/getXcontrollers/linkCardControllerX.dart';
import 'package:flutter_app/controllers/linkCardController.dart';
import 'package:flutter_app/controllers/userController.dart';
import 'package:flutter_app/models/card.dart';
import 'package:flutter_app/views/dashboard/customBottomNavigationBar.dart';
import 'package:flutter_app/views/dashboard/dashboard.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/linkCard/linkCard.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:flutter_app/views/transfert/transfertSource.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LinkedCard extends StatefulWidget {
  @override
  _LinkedCardState createState() => _LinkedCardState();
}

class _LinkedCardState extends State<LinkedCard> {
  int _selectedItem;
  Future cardList;
  int seeMore = 0;
  int rest;
  bool loading = false;
  @override
  void initState() {
    getUserInfo().then((result) async {
      print(result['status']['code']);
      if(result['status']['code'] == 1005){
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.remove('connected');
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    Login()));
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return DialogScreen(
                color: Colors.amber,
                icon: Icons.pending_actions,
                title: 'Session',
                message: result['status']['message'],
                message2: '',
                buttonText: 'Close',
              );
            });
      }else if(result['status']['code'] == 200){
        setState(() {
          cardList = indexCards();
        });
      }
    });
    // indexCards().then((cards){
    //
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Color.fromRGBO(234, 239, 255, 3),
        bottomNavigationBar: CustomBottomNavigationBar(
          iconList: [
            Icons.apps,
            Icons.money_outlined,
            Icons.qr_code_scanner_sharp,
            Icons.wallet_membership,
            Icons.account_circle_rounded,
          ],
          onChange: (val) {
            setState(() {
              _selectedItem = 3;
            });
          },
          defaultSelectedIndex: 3,
        ),
        resizeToAvoidBottomInset: true,
        body: SingleChildScrollView(
          child: SafeArea(
            child: Container(
              width: double.infinity,
              color: Color.fromRGBO(234, 239, 255, 3),
              child: Column(
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          margin: EdgeInsets.only(top: height / 14.5),
                          child: Text(
                            "Link Card",
                            style: TextStyle(fontSize: height / 45),
                          )),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        left: 25, right: 25, bottom: 0, top: 20),
                    width: width,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(25))),
                    child: Column(
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                                margin: EdgeInsets.only(
                                    left: 15,
                                    top: height / 35,
                                    bottom: height / 100),
                                child: Text(
                                  "Bank Account",
                                  style: TextStyle(fontSize: height / 45),
                                )),
                            GestureDetector(
                              onTap: () {
                                Navigator.of(context).pushReplacement(
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            LinkCard()));
                              },
                              child: Container(
                                margin: EdgeInsets.only(right: 10),
                                child: Image(
                                  image: AssetImage(
                                    "assets/images/add.png",
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Divider(
                          height: 20,
                          thickness: 0.1,
                          color: Colors.black,
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        FutureBuilder(
                          future: cardList,
                          builder: (BuildContext context,
                              AsyncSnapshot<dynamic> snapshot) {
                            if (snapshot.connectionState ==
                                ConnectionState.done) {
                              if (snapshot.hasData &&
                                  snapshot.data.length > 0) {
                                return Column(
                                  children: [
                                  GridView.builder(
                                      shrinkWrap: true,
                                      itemCount: (snapshot.data.length ~/ 4) < 1
                                          ? snapshot.data.length
                                          : (4 + seeMore),
                                      gridDelegate:
                                          SliverGridDelegateWithFixedCrossAxisCount(
                                              crossAxisCount: 4,
                                              childAspectRatio: 1),
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return loading == false ? ListView.builder(
                                            shrinkWrap: true,
                                            itemCount: 1,
                                            itemBuilder:
                                                (BuildContext ctxt, int indx) {
                                              return Stack(
                                                children: [
                                                  Padding(
                                                    padding: const EdgeInsets.all(12.0),
                                                    child: new Column(
                                                      crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                      mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                      children: <Widget>[
                                                        Image.asset(
                                                          "assets/images/${snapshot.data[index].bank_name}.png",
                                                          width: width / 10,
                                                          height: width / 11,
                                                        ),
                                                        Text(
                                                          "${snapshot.data[index].bank_name}"
                                                              .capitalize,
                                                          style: TextStyle(
                                                              fontSize: height / 49),
                                                          textAlign: TextAlign.center,
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Positioned(
                                                    top: height / 350,
                                                    left: width / 9.5,
                                                    child: GestureDetector(
                                                      onTap: (){
                                                        setState(() {
                                                          loading = true;
                                                        });
                                                        print(snapshot.data[index].id);

                                                        deleteCard(snapshot.data[index].id).then((card){
                                                          setState(() {
                                                            loading = false;
                                                          });
                                                          Navigator.of(context).pushReplacement(
                                                              MaterialPageRoute(
                                                                  builder: (BuildContext context) =>
                                                                      LinkedCard()));
                                                          showDialog(
                                                              context: context,
                                                              builder: (BuildContext context) {
                                                                return DialogScreen(
                                                                  color: Colors.amber,
                                                                  icon: Icons.error_outline,
                                                                  title: 'Success',
                                                                  message: 'Card deleted successfully',
                                                                  message2:
                                                                  '',
                                                                  buttonText: 'Close',
                                                                );
                                                              });
                                                        });
                                                      },
                                                      child: Container(
                                                        width: height / 37.5,
                                                        child: Image.asset(
                                                          "assets/images/delete.png",
                                                          fit: BoxFit.fill,
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              );
                                            }) : Padding(
                                          padding: const EdgeInsets.only(bottom: 12.0),
                                          child:
                                          Center(child: CircularProgressIndicator()),
                                        );
                                      },
                                    ) ,
                                    snapshot.data.length > 4 ? Container(
                                      height: height / 13.5,
                                      width: width / 1.25,
                                      margin: EdgeInsets.only(top: height / 50),
                                      child: InkWell(
                                        onTap: () {},
                                        // ignore: deprecated_member_use
                                        child: FlatButton(
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                          ),
                                          onPressed: () {
                                            print(seeMore);
                                            if (snapshot.data.length -
                                                    (4 + seeMore) >=
                                                4) {
                                              setState(() {
                                                seeMore = seeMore + 4;
                                              });
                                            } else {
                                              setState(() {
                                                seeMore = seeMore +
                                                    snapshot.data.length -
                                                    (4 + seeMore);
                                              });
                                            }
                                          },
                                          color:
                                              Color.fromRGBO(244, 245, 247, 3),
                                          textColor: Colors.white,
                                          child: Text("See all",
                                              style: TextStyle(
                                                  fontSize: height / 50,
                                                  color:
                                                      const Color(0xff7a869a))),
                                        ),
                                      ),
                                    ) : Container(),
                                  ],
                                );
                              } else {
                                return Padding(
                                  padding: const EdgeInsets.only(bottom: 12.0),
                                  child: Text(
                                    "No cards found !",
                                    style: TextStyle(fontSize: height / 47),
                                  ),
                                );
                              }
                            } else {
                              return Padding(
                                padding: const EdgeInsets.only(bottom: 12.0),
                                child:
                                    Center(child: CircularProgressIndicator()),
                              );
                            }
                          },
                        ),
                        SizedBox(
                          height: 5,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: height / 15,
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        left: 25, right: 25, bottom: 35, top: 20),
                    width: width,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(25))),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                                margin: EdgeInsets.only(
                                    left: 15,
                                    top: height / 35,
                                    bottom: height / 100),
                                child: Text(
                                  "International Card",
                                  style: TextStyle(fontSize: height / 40),
                                )),
                          ],
                        ),
                        Divider(
                          height: 20,
                          thickness: 0.1,
                          color: Colors.black,
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Column(
                              children: [
                                Image(
                                  width: width / 6,
                                  image: AssetImage(
                                    "assets/images/paypal.png",
                                  ),
                                ),
                                SizedBox(height: 8),
                                Text(
                                  "Paypal",
                                  style: TextStyle(fontSize: height / 55),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                Image(
                                  width: width / 6,
                                  image: AssetImage("assets/images/visa.png"),
                                ),
                                SizedBox(height: 8),
                                Text(
                                  "Citibank",
                                  style: TextStyle(fontSize: height / 55),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                Image(
                                  width: width / 6,
                                  image: AssetImage(
                                      "assets/images/mastercard.png"),
                                ),
                                SizedBox(height: 8),
                                Text(
                                  "Bank of ...",
                                  style: TextStyle(fontSize: height / 55),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                Image(
                                  width: width / 6,
                                  image:
                                      AssetImage("assets/images/payoneer.png"),
                                ),
                                SizedBox(height: height / 80),
                                Text(
                                  "Deutsche",
                                  style: TextStyle(fontSize: height / 55),
                                )
                              ],
                            ),
                          ],
                        ),
                        SizedBox(
                          height: height / 35,
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
