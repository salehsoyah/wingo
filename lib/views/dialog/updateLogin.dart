import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/authController.dart';
import 'package:flutter_app/controllers/userController.dart';
import 'package:flutter_app/views/dashboard/dashboard.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/signinScreens/opt_received_social.dart';
import 'package:flutter_app/views/signinScreens/otp_received.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UpdateLogin extends StatefulWidget {
  @override
  _UpdateLoginState createState() => _UpdateLoginState();

  String first_name;
  String last_name;
  String email;
  DateTime birthday;
  int gender;
  bool loading = false;

  UpdateLogin({
    Key key,
    @required this.first_name,
    @required this.last_name,
    @required this.email,
    @required this.birthday,
    @required this.gender,
  }) : super(key: key);
}

class _UpdateLoginState extends State<UpdateLogin> {
  @override
  final _phoneController = TextEditingController();
  final _passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  String tet = '555';
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
        child: SafeArea(
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Stack(
                overflow: Overflow.visible,
                alignment: Alignment.topCenter,
                children: [
                  Container(
                    height: height / 1.6,
                    width: width / 1.1,
                    child: loading == true
                        ? Container(
                            height: height,
                            child: SpinKitFadingCircle(
                              itemBuilder: (BuildContext context, int index) {
                                return DecoratedBox(
                                  decoration: BoxDecoration(
                                    color: index.isEven
                                        ? Colors.yellow
                                        : Colors.blue,
                                  ),
                                );
                              },
                            ),
                          )
                        : Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(height: height / 15,),
                              Text(
                                'Complete profile',
                                style: TextStyle(
                                    fontWeight: FontWeight.w500, fontSize: height / 40),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                height: height / 20,
                              ),
                              Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(left: width * 0.09),
                                    child: Text(
                                      "Phone Number",
                                      style: TextStyle(
                                          color: Color.fromRGBO(193, 199, 208, 3),
                                          fontSize: height / 45),
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                width: width / 1,
                                margin: EdgeInsets.fromLTRB(
                                    width * 0.05, 0, width * 0.05, 0),
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Please enter phone';
                                    } else if (value.length > 8 ||
                                        value.length < 8) {
                                      return 'Phone should have 8 digits';
                                    }
                                    return null;
                                  },
                                  keyboardType: TextInputType.number,
                                  controller: _phoneController,
                                  autofocus: false,
                                  style: TextStyle(
                                    fontSize: height / 45,
                                    color: Colors.black,
                                  ),
                                  decoration: InputDecoration(
                                    errorStyle: TextStyle(
                                      fontSize: height / 55,
                                    ),
                                    prefixIcon: Icon(
                                      Icons.phone_android,
                                      size: height / 25,
                                    ),
                                    border: InputBorder.none,
                                    hintText: '21 111 111',
                                    filled: true,
                                    fillColor: Color.fromRGBO(244, 245, 247, 3),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: height / 30,
                              ),
                              Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(left: width * 0.09),
                                    child: Text(
                                      "Password",
                                      style: TextStyle(
                                          color: Color.fromRGBO(193, 199, 208, 3),
                                          fontSize: height / 45),
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                width: width / 1,
                                margin:
                                    EdgeInsets.fromLTRB(width * 0.05, 0, width * 0.05, 0),
                                child: TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Please enter password';
                                    }
                                    return null;
                                  },
                                  controller: _passwordController,
                                  obscureText: true,
                                  autofocus: false,
                                  style: TextStyle(

                                      fontSize: height / 45, color: Colors.black),
                                  decoration: InputDecoration(
                                    errorStyle: TextStyle(
                                      fontSize: height / 55,
                                    ),
                                    prefixIcon: Icon(
                                      Icons.admin_panel_settings_sharp,
                                      size: height / 25,
                                    ),
                                    border: InputBorder.none,
                                    hintText: '••••••••••••• ',
                                    filled: true,
                                    fillColor: Color.fromRGBO(244, 245, 247, 3),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: height / 50,
                              ),
                              FlatButton(
                                onPressed: () {
                                  if (_formKey.currentState.validate()) {
                                    setState(() {
                                      loading = true;
                                    });
                                    completeLogin(
                                            widget.email,
                                            int.parse(_phoneController.text),
                                            _passwordController.text
                                    )
                                        .then((result) async {
                                          print(result);
                                      setState(() {
                                        loading = false;
                                      });
                                      if (result['status']['code'] == 200) {

                                        SharedPreferences preferences =
                                            await SharedPreferences.getInstance();

                                        preferences.setString('idSession', result['user']['session_id']);

                                        // preferences.setString(
                                        //     'idUser',
                                        //     (result['user']['hashed_id'])
                                        //         .toString());
                                        //
                                        // preferences.setString(
                                        //     'userName',
                                        //     result['user']['first_name']
                                        //         .toString());
                                        //
                                        // preferences.setBool('connected', true);
                                        resendVerificationPhone(widget.email, _phoneController.text, result['user']['first_name']).then((sent){
                                         if(sent['status']['code'] == 200){
                                           Navigator.push(
                                             context,
                                             MaterialPageRoute(
                                                 builder: (context) => OTPReceivedSocial(id: result['user']['hashed_id'], name: result['user']['first_name'], email: widget.email, phone: _phoneController.text)),
                                           );
                                         }else{
                                           showDialog(
                                               context: context,
                                               builder: (BuildContext context) {
                                                 return DialogScreen(
                                                   color: Colors.red,
                                                   icon: Icons.error_outline,
                                                   title: 'Error',
                                                   message: result['message'],
                                                   message2: '',
                                                   buttonText: 'Close',
                                                 );
                                               });
                                         }
                                        });

                                      } else {
                                        Navigator.pop(context);
                                        showDialog(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return DialogScreen(
                                                color: Colors.red,
                                                icon: Icons.error_outline,
                                                title: 'Error',
                                                message: result['status']['message'],
                                                message2: '',
                                                buttonText: 'Close',
                                              );
                                            });
                                      }
                                      // Navigator.pop(context, result['status']['code']);
                                    });
                                  }

                                  // not sure if this will work, could you try?
                                },
                                color: Colors.amber,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                minWidth: width / 1.7,
                                child: Text(
                                  'Confirm',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                              )
                            ],
                          ),
                  ),
                  Positioned(
                      top: - (height / 15),
                      child: Container(
                        width: width / 4,
                        height: height / 7,
                        decoration: BoxDecoration(
                          color: Colors.amber,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Icon(
                          Icons.security,
                          color: Colors.white,
                          size: height / 15,
                        ),
                      )),
                ],
              ),
            ),
          ),
        ));
  }
}
