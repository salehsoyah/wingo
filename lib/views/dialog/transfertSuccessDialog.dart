import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_app/views/dashboard/dashboard.dart';
import 'package:intl/intl.dart';

class TransferSuccess extends StatelessWidget {
  String amount;
  String name;
  String fromName;
  String fee;
  String title;

  TransferSuccess({
    Key key,
    @required this.amount,
    @required this.name,
    @required this.fromName,
    @required this.fee,
    @required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
        child: Stack(
          overflow: Overflow.visible,
          alignment: Alignment.topCenter,
          children: [
            Container(
              height: 500,
              width: 500,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 65, 10, 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      '$title money was successful',
                      style:
                          TextStyle(fontWeight: FontWeight.w500, fontSize: 18),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      'You Earned $fee points',
                      style: TextStyle(fontSize: 14, color: Colors.grey),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: 250,
                      height: 100,
                      decoration: BoxDecoration(
                        color: Colors.grey[100],
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Column(
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                amount,
                                style: TextStyle(
                                  fontSize: 30,
                                  color: Colors.amber,
                                ),
                              ),
                              Text(
                                'TND',
                                style: TextStyle(fontSize: 12, fontFeatures: [
                                  FontFeature.enable('sups'),
                                ]),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            "Sent Amount",
                            style: TextStyle(fontSize: 16, color: Colors.grey),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Expanded(
                      child: ListView(
                        children: [
                          ListTile(
                            leading: SizedBox(
                              width: 80,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        'From',
                                        style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            trailing: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  fromName == null ? ' ' : fromName,
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w500),
                                ),
                                Text(
                                  '****9999',
                                  style: TextStyle(color: Colors.grey),
                                )
                              ],
                            ),
                          ),
                          ListTile(
                            leading: SizedBox(
                              width: 80,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        'To',
                                        style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            trailing: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  name == null ? ' ' : name,
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w500),
                                ),
                                Text(
                                  '****9999',
                                  style: TextStyle(color: Colors.grey),
                                )
                              ],
                            ),
                          ),
                          ListTile(
                            leading: SizedBox(
                              width: 80,
                              child: Row(
                                children: [
                                  Column(
                                    children: [
                                      Text(
                                        'Date',
                                        style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      Spacer()
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            trailing: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  DateFormat("yyyy-MM-dd")
                                      .format(DateTime.now()),
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w500),
                                ),
                                Text(
                                  DateFormat("H:mm:ss").format(DateTime.now()),
                                  style: TextStyle(color: Colors.grey),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    FlatButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Dashboard()),
                        );
                      },
                      color: Colors.transparent,
                      child: Text(
                        'GO BACK TO HOME',
                        style: TextStyle(color: Colors.amber, fontSize: 16),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Positioned(
                top: -52,
                child: Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    color: Colors.amber,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Icon(
                    Icons.credit_card_sharp,
                    color: Colors.white,
                    size: 50,
                  ),
                )),
          ],
        ));
  }
}
