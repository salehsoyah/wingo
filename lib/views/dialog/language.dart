import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/userController.dart';
import 'package:flutter_app/models/item.dart';

class Language extends StatefulWidget {
  @override
  _LanguageState createState() => _LanguageState();

  // String from;
  // String toName;
  // int toPhone;
  Language({
    Key key,
    // @required this.from,
    // @required this.toName,
    // @required this.toPhone
  }) : super(key: key);
}

class _LanguageState extends State<Language> {
  @override
  final _phoneController = TextEditingController();
  final _passwordController = TextEditingController();
  int _groupValue;
  bool evaluate = false;

  List<Item> users = <Item>[
    const Item(
        'Français',
        Icon(
          Icons.android,
          color: const Color(0xFF167F67),
        )),
    const Item(
        'English',
        Icon(
          Icons.flag,
          color: const Color(0xFF167F67),
        )),
  ];
  Item selectedUser;

  String tet = '555';

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
        child: Stack(
          overflow: Overflow.visible,
          alignment: Alignment.topCenter,
          children: [
            Container(
              height: 340,
              width: 500,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 20, 10, 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Choose Language',
                      style:
                          TextStyle(fontWeight: FontWeight.w500, fontSize: 18),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: width * 0.06),
                          child: Text(
                            "Languages",
                            style: TextStyle(
                                color: Color.fromRGBO(193, 199, 208, 3),
                                fontSize: height / 45),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      width: width / 1,
                      height: height / 20,
                      margin:
                          EdgeInsets.fromLTRB(0, 0, width * 0.05, 0),
                      child: RadioListTile(
                        value: 1,
                        groupValue: 1,
                        onChanged: (newValue) =>
                            setState(() => _groupValue = newValue),
                        title: Text("English"),
                      ),
                    ),
                    Container(
                      width: width / 1,
                      height: height / 20,
                      margin:
                      EdgeInsets.fromLTRB(0, 0, width * 0.05, 0),
                      child: RadioListTile(
                          value: 1,
                          groupValue: 0,
                        onChanged: evaluate ? null : (value) => value = 0,
                        title: Text("Français (Soon)",
                        style: TextStyle(
                          color: Colors.grey[400]
                        ),),
                        ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      color: Colors.amber,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      minWidth: width / 1.7,
                      child: Text(
                        'Confirm',
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      ),
                    ),
                    // FlatButton(
                    //   onPressed: () {
                    //     Navigator.pop(context);
                    //   },
                    //   color: Colors.red,
                    //   shape: RoundedRectangleBorder(
                    //       borderRadius: BorderRadius.circular(10)),
                    //   minWidth: width / 1.7,
                    //   child: Text(
                    //     'Exit',
                    //     style: TextStyle(color: Colors.white, fontSize: 16),
                    //   ),
                    // )
                  ],
                ),
              ),
            ),
            Positioned(
                top: -52,
                child: Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    color: Colors.amber,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Icon(
                    Icons.language_rounded,
                    color: Colors.white,
                    size: 50,
                  ),
                )),
          ],
        ));
  }

}
