import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/userController.dart';
import 'package:flutter_app/models/item.dart';
import 'package:qr_flutter/qr_flutter.dart';

class QrCode extends StatefulWidget {
  @override
  _QrCodeState createState() => _QrCodeState();

  String data;
  // String toName;
  // int toPhone;
  QrCode({
    Key key,
    @required this.data,
    // @required this.toName,
    // @required this.toPhone
  }) : super(key: key);
}

class _QrCodeState extends State<QrCode> {
  @override
  bool evaluate = false;


  Item selectedUser;

  String tet = '555';

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
        child: Stack(
          overflow: Overflow.visible,
          alignment: Alignment.topCenter,
          children: [
            Container(
              height: 480,
              width: 500,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 20, 10, 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'My QR Code',
                      style:
                      TextStyle(fontWeight: FontWeight.w500, fontSize: 18),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        QrImage(data: widget.data, size: width / 1.8,),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      color: Colors.amber,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      minWidth: width / 1.7,
                      child: Text(
                        'Exit',
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      ),
                    ),
                    // FlatButton(
                    //   onPressed: () {
                    //     Navigator.pop(context);
                    //   },
                    //   color: Colors.red,
                    //   shape: RoundedRectangleBorder(
                    //       borderRadius: BorderRadius.circular(10)),
                    //   minWidth: width / 1.7,
                    //   child: Text(
                    //     'Exit',
                    //     style: TextStyle(color: Colors.white, fontSize: 16),
                    //   ),
                    // )
                  ],
                ),
              ),
            ),
            Positioned(
                top: -52,
                child: Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    color: Colors.amber,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Icon(
                    Icons.qr_code_scanner_sharp,
                    color: Colors.white,
                    size: 50,
                  ),
                )),
          ],
        ));
  }

}
