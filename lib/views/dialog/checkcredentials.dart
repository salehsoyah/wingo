import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/localAuthController.dart';
import 'package:flutter_app/controllers/userController.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CheckCredentials extends StatefulWidget {
  @override
  _CheckCredentialsState createState() => _CheckCredentialsState();

  // String from;
  // String toName;
  // int toPhone;
  CheckCredentials({
    Key key,
    // @required this.from,
    // @required this.toName,
    // @required this.toPhone
  }) : super(key: key);
}

class _CheckCredentialsState extends State<CheckCredentials> {
  @override
  final _phoneController = TextEditingController();
  final _passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  String tet = '555';
  bool loading = false;

  getUserId() async {
    SharedPreferences preferences =
    await SharedPreferences
        .getInstance();

    return preferences.getString('idUser');
  }

  String userId;

  void initState() {

    getUserId().then((idUser){
      setState(() {
        userId = idUser;
      });
    });

    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
        child: SafeArea(
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Stack(
                overflow: Overflow.visible,
                alignment: Alignment.topCenter,
                children: [
                  Container(
                    height: height / 1.4,
                    width: width / 1.1,
                    child: Padding(
                      padding:  EdgeInsets.fromLTRB(10, height / 8, 10, height / 20),
                      child: loading == true
                          ? Container(
                              height: height,
                              child: SpinKitFadingCircle(
                                itemBuilder: (BuildContext context, int index) {
                                  return DecoratedBox(
                                    decoration: BoxDecoration(
                                      color: index.isEven
                                          ? Colors.yellow
                                          : Colors.blue,
                                    ),
                                  );
                                },
                              ),
                            )
                          : Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Check credentials',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500, fontSize: height / 40),
                                  textAlign: TextAlign.center,
                                ),
                                SizedBox(
                                  height: height / 20,
                                ),
                                Row(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(left: width * 0.09),
                                      child: Text(
                                        "Phone Number",
                                        style: TextStyle(
                                            color: Color.fromRGBO(193, 199, 208, 3),
                                            fontSize: height / 45),
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                  width: width / 1,
                                  margin: EdgeInsets.fromLTRB(
                                      width * 0.05, 0, width * 0.05, 0),
                                  child: TextFormField(
                                    validator: (value) {
                                      if (value == null || value.isEmpty) {
                                        return 'Please enter phone';
                                      } else if (value.length > 8 ||
                                          value.length < 8) {
                                        return 'Phone should have 8 digits';
                                      }
                                      return null;
                                    },
                                    keyboardType: TextInputType.number,
                                    controller: _phoneController,
                                    autofocus: false,
                                    style: TextStyle(
                                      fontSize: height / 45,
                                      color: Colors.black,
                                    ),
                                    decoration: InputDecoration(
                                      errorStyle: TextStyle(
                                        fontSize: height / 55,
                                      ),
                                      prefixIcon: Icon(
                                        Icons.phone_android,
                                        size: height / 25,
                                      ),
                                      border: InputBorder.none,
                                      hintText: '21 111 111',
                                      filled: true,
                                      fillColor: Color.fromRGBO(244, 245, 247, 3),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(15.0),
                                      ),
                                      enabledBorder: UnderlineInputBorder(
                                        borderRadius: BorderRadius.circular(15.0),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: height / 30,
                                ),
                                Row(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(left: width * 0.09),
                                      child: Text(
                                        "Password",
                                        style: TextStyle(
                                            color: Color.fromRGBO(193, 199, 208, 3),
                                            fontSize: height / 45),
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                  width: width / 1.1,
                                  margin:
                                      EdgeInsets.fromLTRB(width * 0.05, 0, width * 0.05, 0),
                                  child: TextFormField(
                                    validator: (value) {
                                      if (value == null || value.isEmpty) {
                                        return 'Please enter password';
                                      }
                                      return null;
                                    },
                                    controller: _passwordController,
                                    obscureText: true,
                                    autofocus: false,
                                    style: TextStyle(
                                        fontSize: height / 45, color: Colors.black),
                                    decoration: InputDecoration(
                                      errorStyle: TextStyle(
                                        fontSize: height / 55,
                                      ),
                                      prefixIcon: Icon(
                                        Icons.admin_panel_settings_sharp,
                                        size: height / 25,
                                      ),
                                      border: InputBorder.none,
                                      hintText: '••••••••••••• ',
                                      filled: true,
                                      fillColor: Color.fromRGBO(244, 245, 247, 3),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(15.0),
                                      ),
                                      enabledBorder: UnderlineInputBorder(
                                        borderRadius: BorderRadius.circular(15.0),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(height: height / 50,),
                                userId != null ? Container(
                                  child: InkWell(
                                    onTap: () async {
                                      final isAuthenticated = await LocalAuthApi.authenticate();

                                      if (isAuthenticated) {
                                        Navigator.pop(context, 200);
                                      }else{
                                        Navigator.pop(context, 404);
                                      }
                                    },
                                    child: Image(
                                      width: width / 5.8,
                                      image: AssetImage("assets/images/finger.png",),
                                    ),
                                  ),
                                ) : Container(),
                                SizedBox(
                                  height: height / 30,
                                ),
                                FlatButton(
                                  onPressed: () {
                                    if (_formKey.currentState.validate()) {
                                      setState(() {
                                        loading = true;
                                      });
                                      checkUserCredentials(_phoneController.text, _passwordController.text).then((result) async {
                                        setState(() {
                                          loading = false;
                                        });
                                        print(result);
                                        print(_phoneController.text);
                                        print(_passwordController.text);
                                        SharedPreferences preferences = await SharedPreferences.getInstance();
                                        print(preferences.getString('idSession'));
                                        if(result['status']['code'] == 200){
                                          Navigator.pop(context, result['status']['code']);
                                        }
                                        else if(result['status']['code'] == 1005){
                                          SharedPreferences preferences = await SharedPreferences.getInstance();
                                          preferences.remove('connected');
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      Login()));
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return DialogScreen(
                                                  color: Colors.amber,
                                                  icon: Icons.pending_actions,
                                                  title: 'Session',
                                                  message: result['status']['message'],
                                                  message2: '',
                                                  buttonText: 'Close',
                                                );
                                              });
                                        }
                                        else{
                                          Navigator.pop(context, result['status']['code']);
                                        }
                                      });
                                    }

                                  },
                                  color: Colors.amber,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  minWidth: width / 1.7,
                                  child: Text(
                                    'Confirm',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: height / 45),
                                  ),
                                )
                              ],
                            ),
                    ),
                  ),
                  Positioned(
                      top: - (height / 15),
                      child: Container(
                        width: width / 4,
                        height: height / 7,
                        decoration: BoxDecoration(
                          color: Colors.amber,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Icon(
                          Icons.security,
                          color: Colors.white,
                          size: height / 15,
                        ),
                      )),
                ],
              ),
            ),
          ),
        ));
  }
}
