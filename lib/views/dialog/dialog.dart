import 'package:flutter/material.dart';
import 'package:flutter_app/views/dashboard/dashboard.dart';

class DialogScreen extends StatelessWidget {
  final Color color;
  final IconData icon;
  final String title;
  final String message;
  final String message2;
  final String buttonText;
  final String home;

  DialogScreen(
      {Key key,
      @required this.color,
      this.icon,
      this.title,
      this.message,
      this.message2,
      this.buttonText,
      this.home})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
        child: Stack(
          overflow: Overflow.visible,
          alignment: Alignment.center,
          children: [
            Container(
              height: 270,
              width: 500,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 65, 10, 0),
                child: Column(
                  children: [
                    Text(
                      title,
                      style:
                          TextStyle(fontWeight: FontWeight.w500, fontSize: 20),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 7,
                    ),
                    Text(
                      message,
                      style: TextStyle(fontSize: 14.5, color: Colors.grey),
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      message2,
                      style: TextStyle(fontSize: 14.5, color: Colors.grey),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 13,
                    ),
                    FlatButton(
                      minWidth: width / 1.7,

                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)
                      ),
                      onPressed: () {
                        if (home == "yes") {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Dashboard()),
                          );
                        } else {
                          Navigator.pop(context);
                        }
                      },
                      color: Colors.amber,
                      child: Text(
                        buttonText,
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Positioned(
                top: -52,
                child: Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    color: color,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Icon(
                    icon,
                    color: Colors.white,
                    size: 50,
                  ),
                )),
          ],
        ));
  }
}
