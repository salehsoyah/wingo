import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/userController.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/dom.dart' as dom;

class Privacy extends StatefulWidget {
  @override
  _PrivacyState createState() => _PrivacyState();

  String terms;
  Privacy(
      {Key key,
        @required this.terms
      })
      : super(key: key);
}

class _PrivacyState extends State<Privacy> {
  @override
  bool checkboxValue = false;
  final _phoneController = TextEditingController();
  final _passwordController = TextEditingController();

  String tet = '555';
  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
        child: Stack(
          overflow: Overflow.visible,
          alignment: Alignment.topCenter,
          children: [
            Container(
              height: 600,
              width: 400,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(height: 60,),
                    Text(
                      'Privacy Policy',
                      style:
                      TextStyle(fontWeight: FontWeight.w500, fontSize: 18),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Html(
                        data: """
                          ${widget.terms}
                  """,
                        padding: EdgeInsets.all(8.0),
                        onLinkTap: (url) {
                          print("Opening $url...");
                        },
                      ),
                    ),
                    FlatButton(
                      onPressed: () {

                        Navigator.pop(context);
                      },
                      color: Colors.amber,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      minWidth: width / 1.7,
                      child: Text(
                        'Exit',
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Positioned(
                top: -52,
                child: Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    color: Colors.amber,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Icon(
                    Icons.privacy_tip,
                    color: Colors.white,
                    size: 50,
                  ),
                )),
          ],
        ));
  }
}

