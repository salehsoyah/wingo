import 'package:flutter/material.dart';
import 'package:flutter_app/views/dashboard/dashboard.dart';
import 'package:flutter_app/views/linkCard/linkedCard.dart';
import 'package:flutter_app/views/signinScreens/login.dart';

class Transaction extends StatefulWidget {
  @override
  _TransactionState createState() => _TransactionState();

  final IconData icon;
  final String title;
  final String message;
  final int type;
  Transaction({
    Key key,
    @required this.icon,
    @required this.title,
    @required this.message,
    this.type
  }) : super(key: key);
}

class _TransactionState extends State<Transaction> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
      child: Stack(
        overflow: Overflow.visible,
        alignment: Alignment.topCenter,
        children: [
          Container(
            height: 250,
            width: 500,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 65, 10, 10),
              child: Column(
                children: [
                  Text(
                    widget.title,
                    style: TextStyle(fontWeight: FontWeight.w500, fontSize: 18),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    widget.message,
                    style: TextStyle(fontSize: 14, color: Colors.grey),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  FlatButton(
                    onPressed: () {
                      widget.type == null ? Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Dashboard())) : Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Login()));
                    },
                    color: Colors.transparent,
                    child: Text(
                      'CLICK & CONTINUE',
                      style: TextStyle(color: Colors.amber, fontSize: 16),
                    ),
                  )
                ],
              ),
            ),
          ),
          Positioned(
              top: -52,
              child: Container(
                width: 100,
                height: 100,
                decoration: BoxDecoration(
                  color: Colors.amber,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Icon(
                  widget.icon,
                  //color: Colors.white,
                  size: 45,
                ),
              )),
        ],
      ),
    );
  }
}
