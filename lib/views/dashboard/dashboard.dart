import 'dart:ui';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/controllers/userController.dart';
import 'package:flutter_app/main.dart';
import 'package:flutter_app/views/dashboard/customBottomNavigationBar.dart';
import 'package:flutter_app/views/dashboard/homeTopTabs.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/history/history.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:flutter_app/views/topUpScreens/topUpDeposit.dart';
import 'package:flutter_app/views/transfert/transfert.dart';
import 'package:flutter_app/views/withdraw/withdrawAmount.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  int _selectedItem = 0;
  double balance = 0;
  double loyalties = 0;
  Future withdrawals;
  Future transfers;
  Future deposits;

  void initState() {

    getUserBalance().then((result) async {

      if(result['status']['code'] == 1005){
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.remove('connected');
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    Login()));
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return DialogScreen(
                color: Colors.amber,
                icon: Icons.pending_actions,
                title: 'Session',
                message: result['status']['message'],
                message2: '',
                buttonText: 'Close',
              );
            });
      }
      if (result['status']['code'] == 200) {
        setState(() {
          balance = (result['data']['balance']).toDouble();
          loyalties = result['data']['loyalties'].toDouble();
        });
        setState((){
          withdrawals = getListUserWithdrawals();
          transfers = getListUserTransfer();
          deposits = getListUserDeposits();
        });
      }
    });



    // getListUserWithdrawals().then((result) {
    //
    // });

    super.initState();


    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channel.description,
                color: Colors.blue,
                playSound: true,
                icon: '@mipmap/ic_launcher',
              ),
            ));
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('A new onMessageOpenedApp event was published!');
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;
      if (notification != null && android != null) {
        showDialog(
            context: context,
            builder: (_) {
              return AlertDialog(
                title: Text(notification.title),
                content: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(notification.body),
                    ],
                  ),
                ),
              );
            });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return DefaultTabController(
      length: 1,
      initialIndex: 0,
      child: Scaffold(
        bottomNavigationBar: CustomBottomNavigationBar(
          iconList: [
            Icons.apps,
            Icons.money_outlined,
            Icons.qr_code_scanner_sharp,
            Icons.wallet_membership,
            Icons.account_circle_rounded,
          ],
          onChange: (val) {
            setState(() {
              _selectedItem = 3;
            });
          },
          defaultSelectedIndex: 0,
        ),
        body: SingleChildScrollView(
          child: Container(
            width: double.infinity,
            color: Color.fromRGBO(234, 239, 255, 3),
            child: SingleChildScrollView(
              child: SafeArea(
                  child: FutureBuilder(
                future: withdrawals,
                builder:
                    (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                  if (!snapshot.hasData) {
                    return Container(
                      height: height,
                      child: SpinKitFadingCircle(
                        itemBuilder: (BuildContext context, int index) {
                          return DecoratedBox(
                            decoration: BoxDecoration(
                              color: index.isEven ? Colors.yellow : Colors.blue,
                            ),
                          );
                        },
                      ),
                    );
                  } else {
                    return Container(
                      child: Column(
                        children: <Widget>[
                          Center(
                            child: Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 20),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15.0))),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    width: 7,
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: TextFormField(
                                      decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText: "Search on Wingo",
                                          hintStyle: TextStyle(
                                            color: Colors.grey,
                                          ),
                                          icon: Icon(
                                            Icons.search,
                                            color: Colors.grey,
                                          )),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 0,
                                    child: Row(),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Column(
                                children: [
                                  Center(
                                    child: Container(
                                        margin: EdgeInsets.only(
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              30,
                                        ),
                                        child: Row(
                                          children: [
                                            Text(
                                              balance.toString(),
                                              style: TextStyle(fontSize: 30),
                                            ),
                                            Text(
                                              'TND',
                                              style: TextStyle(fontFeatures: [
                                                FontFeature.enable('sups'),
                                              ]),
                                            )
                                          ],
                                        )),
                                  ),
                                  Center(
                                    child: Container(
                                        margin: EdgeInsets.only(
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              100,
                                        ),
                                        child: Text(
                                          "Wingo Balance",
                                          style: TextStyle(
                                              fontSize: 15, color: Colors.grey),
                                        )),
                                  ),
                                ],
                              ),
                              Column(
                                children: [
                                  Center(
                                    child: Container(
                                        margin: EdgeInsets.only(
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              30,
                                        ),
                                        child: Row(
                                          children: [
                                            Text(
                                              loyalties.toString(),
                                              style: TextStyle(fontSize: 30),
                                            ),
                                            Text(
                                              'WP',
                                              style: TextStyle(fontFeatures: [
                                                FontFeature.enable('sups'),
                                              ]),
                                            )
                                          ],
                                        )),
                                  ),
                                  Center(
                                    child: Container(
                                        margin: EdgeInsets.only(
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              100,
                                        ),
                                        child: Text(
                                          "Wingo Loyalties",
                                          style: TextStyle(
                                              fontSize: 15, color: Colors.grey),
                                        )),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.width / 7,
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                width: width / 4,
                                height: height / 10,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(12.0))),
                                child: ClipRRect(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15.0)),
                                  child: Material(
                                    // button color
                                    child: InkWell(
                                      splashColor: Colors.amber,
                                      // splash color
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    TopUpDeposit()));
                                      },
                                      // button pressed
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            Icons.money,
                                            color: Colors.amber,
                                            size: 25,
                                          ),
                                          // icon
                                          Text("Deposit"), // text
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Container(
                                width: width / 4,
                                height: height / 10,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(12.0))),
                                child: ClipRRect(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15.0)),
                                  child: Material(
                                    // button color
                                    child: InkWell(
                                      splashColor: Colors.amber,
                                      // splash color
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    Transfer()));
                                      },
                                      // button pressed
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            Icons.money,
                                            color: Colors.amber,
                                            size: 25,
                                          ),
                                          // icon
                                          Text("Transfer"), // text
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Container(
                                width: width / 4,
                                height: height / 10,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(12.0))),
                                child: ClipRRect(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15.0)),
                                  child: Material(
                                    // button color
                                    child: InkWell(
                                      splashColor: Colors.amber,
                                      // splash color
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    WithdrawAmount()));
                                      },
                                      // a revoir le screen
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            Icons.money,
                                            color: Colors.amber,
                                            size: 25,
                                          ),
                                          // icon
                                          Text("Withdraw"), // text
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 50,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(40),
                                  topRight: Radius.circular(40),
                                )),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Container(
                                        margin:
                                            EdgeInsets.only(left: 20, top: 15),
                                        child: Text(
                                          "Services",
                                          style: TextStyle(
                                              fontSize: height / 45,
                                              fontWeight: FontWeight.w500),
                                        )),
                                  ],
                                ),
                                SizedBox(height: 25),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Column(
                                      children: [
                                        Image(
                                          width: 57,
                                          height: 57,
                                          image: AssetImage(
                                              "assets/images/travel.PNG"),
                                        ),
                                        SizedBox(height: 8),
                                        Text(
                                          "Travelling",
                                          style:
                                              TextStyle(fontSize: height / 47),
                                        )
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        Image(
                                          width: 57,
                                          height: 57,
                                          image: AssetImage(
                                              "assets/images/ticket.PNG"),
                                        ),
                                        SizedBox(height: 8),
                                        Text(
                                          "Ticket",
                                          style:
                                              TextStyle(fontSize: height / 47),
                                        )
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        Image(
                                          width: 57,
                                          height: 57,
                                          image: AssetImage(
                                              "assets/images/shopping.PNG"),
                                        ),
                                        SizedBox(height: 8),
                                        Text(
                                          "Shopping",
                                          style:
                                              TextStyle(fontSize: height / 47),
                                        )
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        Image(
                                          width: 57,
                                          height: 57,
                                          image: AssetImage(
                                              "assets/images/voucher.PNG"),
                                        ),
                                        SizedBox(height: 8),
                                        Text(
                                          "Voucher",
                                          style:
                                              TextStyle(fontSize: height / 47),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Column(
                                      children: [
                                        Image(
                                          width: 57,
                                          height: 57,
                                          image: AssetImage(
                                              "assets/images/topup.PNG"),
                                        ),
                                        SizedBox(height: 8),
                                        Text(
                                          "Top up",
                                          style:
                                              TextStyle(fontSize: height / 47),
                                        )
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        Image(
                                          width: 57,
                                          height: 57,
                                          image: AssetImage(
                                              "assets/images/billpay.PNG"),
                                        ),
                                        SizedBox(height: 8),
                                        Text(
                                          "Bill Pay",
                                          style:
                                              TextStyle(fontSize: height / 47),
                                        )
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        Image(
                                          width: 57,
                                          height: 57,
                                          image: AssetImage(
                                              "assets/images/rewards.PNG"),
                                        ),
                                        SizedBox(height: 8),
                                        Text(
                                          "Rewards",
                                          style:
                                              TextStyle(fontSize: height / 47),
                                        )
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        Image(
                                          width: 57,
                                          height: 57,
                                          image: AssetImage(
                                              "assets/images/more.PNG"),
                                        ),
                                        SizedBox(height: 8),
                                        Text(
                                          "More",
                                          style:
                                              TextStyle(fontSize: height / 47),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Divider(
                                  height: 20,
                                  thickness: 0.05,
                                  color: Colors.black,
                                ),
                                Row(
                                  children: [
                                    Container(
                                      margin:
                                          EdgeInsets.only(left: 20, top: 15),
                                      child: Text(
                                        "Last Transactions",
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: Column(
                                    children: [
                                      FutureBuilder(
                                        future: transfers,
                                        builder: (BuildContext context,
                                            AsyncSnapshot snapshot) {
                                          if (snapshot.hasData && snapshot.data.length > 0)
                                            return ListView.builder(
                                              physics:
                                                  NeverScrollableScrollPhysics(),
                                              scrollDirection: Axis.vertical,
                                              shrinkWrap: true,
                                              itemCount: snapshot.data.length < 2 ? 1 : 2,
                                              itemBuilder: (context, index) {
                                                return Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 10,
                                                          right: 15.0,
                                                          bottom: 10),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Row(
                                                        children: [
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    right: 8.0),
                                                            child: Image(
                                                              width: 50,
                                                              image: AssetImage(
                                                                  "assets/images/transfer.png"),
                                                            ),
                                                          ),
                                                          Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Text(snapshot
                                                                  .data[index]
                                                                  .toName),
                                                              Text(
                                                                (snapshot
                                                                        .data[
                                                                            index]
                                                                        .transfer_date)
                                                                    .substring(
                                                                        0, 10),
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .grey),
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                      Row(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [
                                                          Text("- " +
                                                              snapshot.data[index]
                                                                  .amount
                                                                  .toString()),
                                                          Text(
                                                            'TND',
                                                            style: TextStyle(
                                                                fontSize: 8,
                                                                fontFeatures: [
                                                                  FontFeature.enable(
                                                                      'sups'),
                                                                ]),
                                                          )
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                );
                                              },
                                            );
                                          else {
                                            return Container(
                                              margin: EdgeInsets.only(left: 20),
                                            );
                                          }
                                        },
                                      ),
                                      FutureBuilder(
                                        future: deposits,
                                        // async work
                                        builder: (BuildContext context,
                                            AsyncSnapshot snapshot) {
                                          if (snapshot.hasData && snapshot.data.length > 0)
                                            return ListView.builder(
                                              physics:
                                                  NeverScrollableScrollPhysics(),
                                              scrollDirection: Axis.vertical,
                                              shrinkWrap: true,
                                              itemCount: snapshot.data.length < 2 ? 1 : 2,
                                              itemBuilder: (context, index) {
                                                return Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 10,
                                                          right: 15.0,
                                                          bottom: 10),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Row(
                                                        children: [
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    right: 8.0),
                                                            child: Image(
                                                              width: 50,
                                                              image: AssetImage(
                                                                  "assets/images/deposit.png"),
                                                            ),
                                                          ),
                                                          Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Text(snapshot
                                                                          .data[
                                                                              index]
                                                                          .type ==
                                                                      1
                                                                  ? 'Card Deposit'
                                                                  : 'Cash In'),
                                                              Text(
                                                                (snapshot
                                                                        .data[
                                                                            index]
                                                                        .transfer_date)
                                                                    .substring(
                                                                        0, 10),
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .grey),
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                      Row(
                                                        crossAxisAlignment: CrossAxisAlignment.start,

                                                        children: [
                                                          Text("+ " +
                                                              snapshot.data[index]
                                                                  .amount
                                                                  .toString()),
                                                          Text(
                                                            'TND',
                                                            style: TextStyle(
                                                                fontSize: 8,
                                                                fontFeatures: [
                                                                  FontFeature.enable(
                                                                      'sups'),
                                                                ]),
                                                          )
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                );
                                              },
                                            ); else{
                                            return Container(
                                              margin: EdgeInsets.only(left: 20),
                                            );
                                          }
                                        },
                                      ),
                                      FutureBuilder(
                                        future: withdrawals,
                                        // async work
                                        builder: (BuildContext context,
                                            AsyncSnapshot snapshot) {
                                          if (snapshot.hasData && snapshot.data.length > 0)
                                            return ListView.builder(
                                              physics:
                                                  NeverScrollableScrollPhysics(),
                                              scrollDirection: Axis.vertical,
                                              shrinkWrap: true,
                                              itemCount: 1,
                                              itemBuilder: (context, index) {
                                                return Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 10,
                                                          right: 15.0,
                                                          bottom: 10),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Row(
                                                        children: [
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    right: 8.0),
                                                            child: Image(
                                                              width: 50,
                                                              image: AssetImage(
                                                                  "assets/images/withdraw.png"),
                                                            ),
                                                          ),
                                                          Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Text(snapshot
                                                                          .data[
                                                                              index]
                                                                          .type ==
                                                                      1
                                                                  ? 'Bank Withdrawal'
                                                                  : 'Cash out'),
                                                              Text(
                                                                (snapshot
                                                                        .data[
                                                                            index]
                                                                        .transfer_date)
                                                                    .substring(
                                                                        0, 10),
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .grey),
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                      Row(
                                                        crossAxisAlignment: CrossAxisAlignment.start,

                                                        children: [
                                                          Text(
                                                            "- " +
                                                                snapshot.data[index]
                                                                    .amount
                                                                    .toString(),
                                                            style: TextStyle(
                                                                color: snapshot
                                                                            .data[
                                                                                index]
                                                                            .status ==
                                                                        0
                                                                    ? Colors.amber
                                                                    : snapshot
                                                                                .data[
                                                                                    index]
                                                                                .status ==
                                                                            1
                                                                        ? Colors
                                                                            .green
                                                                        : Colors
                                                                            .red),
                                                          ),
                                                          Text(
                                                            'TND',
                                                            style: TextStyle(
                                                                fontSize: 8,
                                                                fontFeatures: [
                                                                  FontFeature.enable(
                                                                      'sups'),
                                                                ]),
                                                          )
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                );
                                              },
                                            ); else{
                                            return Container(
                                              margin: EdgeInsets.only(left: 20),
                                            );
                                          }
                                        },
                                      ),
                                      Container(
                                        height:
                                            MediaQuery.of(context).size.width /
                                                8.5,
                                        width:
                                            MediaQuery.of(context).size.width /
                                                1.25,
                                        margin: EdgeInsets.only(
                                            top: MediaQuery.of(context)
                                                    .size
                                                    .height /
                                                60),
                                        child: InkWell(
                                          onTap: () {},
                                          // ignore: deprecated_member_use
                                          child: FlatButton(
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(15.0),
                                            ),
                                            onPressed: () {
                                              Navigator.of(context)
                                                  .pushReplacement(
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        History()),
                                              );
                                            },
                                            color: Color.fromRGBO(
                                                244, 245, 247, 3),
                                            textColor: Colors.white,
                                            child: Text("See all",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: const Color(
                                                        0xff7a869a))),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                // Divider(
                                //   height: 20,
                                //   thickness: 0.05,
                                //   color: Colors.black,
                                // ),
                                // Row(
                                //   children: [
                                //     Container(
                                //       margin: EdgeInsets.only(left: 20, top: 15),
                                //       child: Text("Monthly Budget", style: TextStyle(
                                //           fontSize: 16,
                                //           fontWeight: FontWeight.w500
                                //       ),
                                //       ),
                                //     ),
                                //   ],
                                // ),
                                // Expanded(
                                //   child: TabBarView(
                                //     children: [
                                //       HomeTopTabs(Colors.amber),
                                //     ],
                                //   ),
                                // ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                  }
                },
              )),
            ),
          ),
        ),
      ),
    );
  }
}
