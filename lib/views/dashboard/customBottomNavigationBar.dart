import 'package:flutter/material.dart';
import 'package:flutter_app/views/dashboard/dashboard.dart';
import 'package:flutter_app/views/history/history.dart';
import 'package:flutter_app/views/linkCard/linkedCard.dart';
import 'package:flutter_app/views/shops/scanShop.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:flutter_app/views/user/profile.dart';
import 'package:flutter_app/views/user/profileOptions.dart';

class CustomBottomNavigationBar extends StatefulWidget {
  final int defaultSelectedIndex;
  final Function(int) onChange;
  final List<IconData> iconList;

  CustomBottomNavigationBar(
      {@required this.defaultSelectedIndex,
        @required this.iconList,
        @required this.onChange});

  @override
  _CustomBottomNavigationBarState createState() =>
      _CustomBottomNavigationBarState();
}

class _CustomBottomNavigationBarState extends State<CustomBottomNavigationBar> {
  int _selectedIndex = 0;
  List<IconData> _iconList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _selectedIndex = widget.defaultSelectedIndex;
    _iconList = widget.iconList;
  }

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    List<Widget> _navBarItemList = [];

    for (var i = 0; i < _iconList.length; i++) {
      _navBarItemList.add(buildNavBarItem(_iconList[i], i));
    }

    return Row(
      children: _navBarItemList,
    );
  }

  Widget buildNavBarItem(IconData icon, int index) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () {
        switch(index){
          case 0:
            Navigator.push(context, MaterialPageRoute(builder: (context)
            =>Dashboard()));
            break;
          case 1:
            Navigator.push(context, MaterialPageRoute(builder: (context)
            =>History()));
            break;
          case 2:
            Navigator.push(context, MaterialPageRoute(builder: (context)
            =>ScanShop()));
            break;
          case 3:
            Navigator.push(context, MaterialPageRoute(builder: (context)
            =>LinkedCard()));
            break;
          case 4:
            Navigator.push(context, MaterialPageRoute(builder: (context)
            =>ProfileOptions()));
            break;

        }
        print('index = '  + index.toString());
        widget.onChange(index);
        setState(() {
          _selectedIndex = index;
        });
        print('selected = ' + _selectedIndex.toString());

      },
      child: Container(
        height: 55,
        width: MediaQuery.of(context).size.width / _iconList.length,
        decoration: index == _selectedIndex
            ? BoxDecoration(
            border: Border(
              bottom: BorderSide(width: 4, color: Colors.amber),
            ),
            gradient: LinearGradient(colors: [
              Colors.amber,
              Colors.amber,
            ], begin: Alignment.bottomCenter, end: Alignment.topCenter)
          // color: index == _selectedItemIndex ? Colors.green : Colors.white,
        )
            : BoxDecoration(),
        child: Icon(
          icon,
          color: index == _selectedIndex ? Colors.white : Colors.grey,
        ),
      ),
    );
  }
}