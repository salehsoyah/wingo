import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/views/dashboard/customBottomNavigationBar.dart';
import 'package:flutter_app/views/dialog/transfertSuccessDialog.dart';
import 'package:flutter_app/views/linkCard/saveLinkedCard.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MyWallet extends StatefulWidget {
  @override
  _MyWalletState createState() => _MyWalletState();
}

class _MyWalletState extends State<MyWallet> {
  int _selectedItem = 0;
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 1,
        child: Scaffold(

          bottomNavigationBar: CustomBottomNavigationBar(
            iconList: [
              Icons.apps,
              Icons.money_outlined,
              Icons.qr_code_scanner_sharp,
              Icons.wallet_membership,
              Icons.account_circle_rounded,
            ],

            onChange: (val) {
              setState(() {
                _selectedItem = val;
              });


            },
            defaultSelectedIndex: 1,
          ),
          body: SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height * 1.7,
              width: double.infinity,
              color: Color.fromRGBO(234, 239, 255, 3),
              child: Stack(
                children: [
                  SafeArea(
                      child: SizedBox(
                    height: MediaQuery.of(context).size.height * 2,
                    child: Column(
                      children: <Widget>[
                        Center(
                          child: Container(
                            margin: EdgeInsets.only(
                              top: MediaQuery.of(context).size.width / 3.5,
                            ),
                            child: Text(
                              "16,003.00",
                              style: TextStyle(fontSize: 35),
                            ),
                          ),
                        ),
                        Center(
                          child: Container(
                              margin: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.width / 100),
                              child: Text(
                                "CaPay Balance",
                                style:
                                    TextStyle(fontSize: 15, color: Colors.grey),
                              )),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.width / 7,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              width: 100,
                              height: 80,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15.0))),
                              child: Column(
                                children: [
                                  SizedBox(
                                    height: 12,
                                  ),
                                  Icon(
                                    Icons.money,
                                    color: Colors.amber,
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text("top up"),

                                ],
                              ),

                            ),

                            SizedBox(
                              width: 10,
                            ),
                            Container(
                              width: 100,
                              height: 80,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15.0))),
                              child: Column(
                                children: [
                                  SizedBox(
                                    height: 12,
                                  ),
                                  Icon(
                                    Icons.money,
                                    color: Colors.amber,
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),


                                  Text("Transfert")

                                ],

                              ),

                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Container(
                              width: 100,
                              height: 80,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15.0))),
                              child: Column(
                                children: [
                                  SizedBox(
                                    height: 12,
                                  ),
                                  Icon(
                                    Icons.money,
                                    color: Colors.amber,

                                  ),

                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text("Withdraw")
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 50,
                        ),
                        Expanded(
                            child: Container(
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(40),
                                topRight: Radius.circular(40),
                              )),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Container(
                                      margin:
                                          EdgeInsets.only(left: 25, top: 20),
                                      child: Text(
                                        "Group Fund",
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500),
                                      )),
                                ],
                              ),
                              SizedBox(height: 25),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Column(
                                    children: [
                                      Image(
                                          image: AssetImage(
                                              "assets/images/hsbc.png")),
                                      SizedBox(height: 8),
                                      Text("Add new")
                                    ],
                                  ),
                                  Column(
                                    children: [

                                      Image(

                                          image: AssetImage(
                                              "assets/images/hsbc.png")),
                                      SizedBox(height: 8),
                                      Text("Family"),

                                    ],
                                  ),
                                  Column(
                                    children: [
                                      Image(
                                          image: AssetImage(
                                              "assets/images/hsbc.png")),
                                      SizedBox(height: 8),
                                      Text("Coffee")
                                    ],
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [

                                  Column(

                                    children: [

                                      Image(
                                        image: AssetImage(
                                            "assets/images/deutsche.png"),
                                      ),

                                      SizedBox(
                                        height: 8,
                                      ),

                                      Text("Friend"),

                                    ],
                                  ),
                                  Column(
                                    children: [
                                      Image(
                                          image: AssetImage(
                                              "assets/images/deutsche.png")),
                                      SizedBox(height: 8),
                                      Text("Company")
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      Image(
                                        image: AssetImage(
                                            "assets/images/deutsche.png"),
                                      ),
                                      SizedBox(
                                        height: 8,
                                      ),
                                      Text("Shopping")
                                    ],
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Divider(
                                height: 20,
                                thickness: 0.05,
                                color: Colors.black,
                              ),
                              Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(left: 25, top: 20),
                                    child: Text(
                                      "Spending",
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500),
                                    ),

                                  ),

                                ],

                              ),

                              SizedBox(
                                height: 15,
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        SizedBox(
                                          height: 40,
                                          width: 300,
                                          child: ListTile(
                                            leading: Image(
                                              image: AssetImage(
                                                  "assets/images/mastercard.png"),
                                            ),
                                            title: Text('Alan Santos'),
                                            subtitle: Text('884-563-1430'),
                                            trailing: Text(
                                              "+ 1,220.00",
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    SizedBox(
                                      height: 15,
                                    ),
                                    Row(
                                      children: [
                                        SizedBox(
                                          height: 40,
                                          width: 300,
                                          child: ListTile(
                                            leading: Image(
                                              image: AssetImage(
                                                  "assets/images/mastercard.png"),
                                            ),
                                            title: Text('Metro Ticket'),
                                            subtitle: Text('21 December 2020'),
                                            trailing: Text("- 0.00"),
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 15,),
                                    Row(
                                      children: [
                                        SizedBox(
                                          height: 40,
                                          width: 300,
                                          child: ListTile(
                                            leading: Image(
                                              image: AssetImage("assets/images/mastercard.png"),
                                            ),

                                            title: Text('Internet Bill'),
                                            subtitle: Text('21 December 2020'),
                                            trailing: Text("- 20.00"),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ))
                      ],
                    ),
                  ))
                ],
              ),
            ),

          ),



        ),

    );
  }
}
