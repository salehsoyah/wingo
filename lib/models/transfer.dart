class Transfer{
  final int id;
  final String toName;
  final double amount;
  final String transfer_date;
  final int status;
  final int type;



  Transfer(this.id, this.toName, this.amount, this.transfer_date, this.status, this.type);
}