class ShopCategory{
  final String id;
  final String title;

  ShopCategory(this.id, this.title);
}