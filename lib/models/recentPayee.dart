class RecentPayee{
  final int id;
  final String name;
  final double amount;
  final String transfer_date;
  final String type;
  final int phone;


  RecentPayee(this.id, this.name, this.amount, this.transfer_date, this.type, this.phone);
}