class Deposit{
  final int id;
  final String toName;
  final int amount;
  final String transfer_date;
  final int status;
  final int type;


  Deposit(this.id, this.toName, this.amount, this.transfer_date, this.status, this.type);
}