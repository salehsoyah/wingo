import 'dart:convert';
import 'dart:io';
import 'package:flutter_app/models/shopCategory.dart';
import 'package:flutter_app/urls/urls.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';


getShop(String code, String user_id) async {
  var queryParameters = {
    'code': '$code',
    'user_id': '$user_id',
  };

  SharedPreferences preferences = await SharedPreferences.getInstance();
  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };
  Response response = await http.post(Uri.parse('$urlProd/shop/scan'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}

addShop(String title, String description, String address,
    String category_id) async {

  SharedPreferences preferences = await SharedPreferences.getInstance();
  String userId = preferences.getString('idUser');
  String id = userId;

  var queryParameters = {
    'title': '$title',
    'description': '$description',
    'longitude': '36.8891',
    'latitude': '10.3223',
    'address': '$address',
    'category_id': '$category_id',
    'user_id': id,
  };

  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };

  Response response = await http.post(Uri.parse('$urlProd/shop/add'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}

getShopCategories() async {

  SharedPreferences preferences = await SharedPreferences.getInstance();
  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };
  Response response = await http.get(
    Uri.parse('$urlProd/shop/categories'),
    // Send authorization headers to the backend.
    headers: headers,
  );

  var jsonData = json.decode(response.body);
  List<ShopCategory> shopCategories = [];
  for (var u in jsonData['data']['categories']) {
    ShopCategory shopCategory = ShopCategory(u['hashed_id'], u['title']);
    shopCategories.add(shopCategory);
  }
  return shopCategories;
}

getUserShop() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  String userId = preferences.getString('idUser');
  String id = userId;

  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };
  Response response = await http.get(
    Uri.parse('$urlProd/shop/get/$userId'),
    // Send authorization headers to the backend.
    headers: headers,
  );

  return jsonDecode(response.body);
}

uploadShopLogo(String logo) async {

  var queryParameters = {
    'logo': '$logo',
  };

  SharedPreferences preferences = await SharedPreferences.getInstance();
  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };

  Response response = await http.post(Uri.parse('$urlProd/shop/update/logo'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}

updateShop(String shopId, String title, String description, String address,
    String category_id) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  String userId = preferences.getString('idUser');
  String id = userId;

  var queryParameters = {
    'title': '$title',
    'description': '$description',
    'longitude': '36.8891',
    'latitude': '10.3223',
    'address': '$address',
    'category_id': '$category_id',
    'user_id': id,
    'shop_id': '$shopId'
  };

  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };

  Response response = await http.post(Uri.parse('$urlProd/shop/update'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}
