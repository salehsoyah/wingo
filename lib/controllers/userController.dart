import 'dart:convert';
import 'dart:io';
import 'package:flutter_app/models/deposit.dart';
import 'package:flutter_app/models/document.dart';
import 'package:flutter_app/models/recentPayee.dart';
import 'package:flutter_app/models/transfer.dart';
import 'package:flutter_app/models/withdrawal.dart';
import 'package:flutter_app/urls/urls.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

getUserBalance() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  String userId = preferences.getString('idUser');
  String id = userId;

  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };
  Response response = await http.get(
    Uri.parse('$urlProd/user/get/balance/$id'),
    // Send authorization headers to the backend.
    headers: headers,
  );

  return jsonDecode(response.body);
}

getUserRecentPayees() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  String userId = preferences.getString('idUser');
  String id = userId;

  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };
  Response response = await http.get(
    Uri.parse('$urlProd/user/get/transfers/$id'),
    // Send authorization headers to the backend.
    headers: headers,
  );

  var jsonData = json.decode(response.body);
  List<RecentPayee> payees = [];
  for (var u in jsonData['data'][0]) {
    RecentPayee payee = RecentPayee(u['id'], u['first_name'],
        u['amount'].toDouble(), u['transfer_date'], u['type'], u['phone']);
    payees.add(payee);
  }
  return payees;
}

getUserInfo() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  String userId = preferences.getString('idUser');
  String id = userId;

  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };
  Response response = await http.get(
    Uri.parse('$urlProd/user/get/$id'),
    // Send authorization headers to the backend.
    headers: headers,
  );

  var jsonData = json.decode(response.body);

  return jsonData;
}

getRecipient(int phone) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();

  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };
  Response response = await http.get(
    Uri.parse('$urlProd/recipient/get/$phone'),
    // Send authorization headers to the backend.
    headers: headers,
  );

  var jsonData = json.decode(response.body);

  return jsonData;
}

getListUserTransfer() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  String userId = preferences.getString('idUser');
  String id = userId;

  var queryParameters = {
    'user_id': '$id',
  };

  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };
  Response response =
      await http.post(Uri.parse('$urlProd/transactions/list/transfer'),
          // Send authorization headers to the backend.
          headers: headers,
          body: queryParameters);

  var jsonData = json.decode(response.body);

  List<Transfer> transfers = [];

  for (var u in jsonData['data']['transfers']) {
    Transfer transfer = Transfer(
        u['id'], u['toName'], u['amount'].toDouble(), u['created_at'], 1, 1);
    transfers.add(transfer);
  }

  return transfers;
}

getListUserDeposits() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  String userId = preferences.getString('idUser');
  String id = userId;

  var queryParameters = {
    'user_id': '$id',
  };

  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };
  Response response =
      await http.post(Uri.parse('$urlProd/transactions/list/deposit'),
          // Send authorization headers to the backend.
          headers: headers,
          body: queryParameters);

  var jsonData = json.decode(response.body);

  List<Deposit> deposits = [];

  for (var u in jsonData['data']['deposits']) {
    Deposit deposit = Deposit(
        u['id'], u['toName'], u['amount'], u['created_at'], 1, u['type']);
    deposits.add(deposit);
  }

  return deposits;
}

getListUserWithdrawals() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  String userId = preferences.getString('idUser');
  String id = userId;

  var queryParameters = {
    'user_id': '$id',
  };

  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };

  Response response =
      await http.post(Uri.parse('$urlProd/transactions/list/withdrawal'),
          // Send authorization headers to the backend.
          headers: headers,
          body: queryParameters);

  var jsonData = json.decode(response.body);

  List<Withdrawal> withdrawals = [];

  for (var u in jsonData['data']['withdrawals']) {
    Withdrawal withdrawal = Withdrawal(u['id'], u['bank_details']['bank'],
        u['amount'], u['created_at'], u['status'], u['type']);
    withdrawals.add(withdrawal);
  }

  return withdrawals;
}

getUserLevelRequest() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  String userId = preferences.getString('idUser');
  String id = userId;

  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };
  Response response = await http.get(
    Uri.parse('$urlProd/level/request/$id'),
    // Send authorization headers to the backend.
    headers: headers,
  );

  var jsonData = json.decode(response.body);

  return jsonData;
}

getLevelDocuments(String levelId) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };
  Response response = await http.get(
    Uri.parse('$urlProd/level/documents/$levelId'),
    // Send authorization headers to the backend.
    headers: headers,
  );

  var jsonData = json.decode(response.body);

  List<Document> documents = [];

  for (var u in jsonData['data']['level']['documents']) {
    Document document = Document(u['hashed_id'], u['title']);
    documents.add(document);
  }

  return documents;
}

addLevelRequest(String level_id) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  String userId = preferences.getString('idUser');
  String id = userId;

  var queryParameters = {'user_id': '$id', 'level_id': '$level_id'};

  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };
  Response response = await http.post(Uri.parse('$urlProd/level/request'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  var jsonData = json.decode(response.body);

  return jsonData;
}

uploadVerification(
    String id, String photo, String user_id, String document_id) async {
  var queryParameters = {
    'photo': '$photo',
    'id': '$id',
    'user_id': '$user_id',
    'document_id': '$document_id'
  };

  SharedPreferences preferences = await SharedPreferences.getInstance();
  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };

  Response response =
      await http.post(Uri.parse('$urlProd/user/upload/verification'),
          // Send authorization headers to the backend.
          headers: headers,
          body: queryParameters);

  var jsonData = json.decode(response.body);

  return jsonData;
}

checkUserCredentials(String phone, String password) async {
  var queryParameters = {
    'phone': '$phone',
    'password': '$password',
  };

  SharedPreferences preferences = await SharedPreferences.getInstance();
  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };
  Response response = await http.post(Uri.parse('$urlProd/user/attempt'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  var jsonData = json.decode(response.body);
  print(idSession);
  return jsonData;
}

updateNotifications(String user, int wingo, int support, int level,
    int transfer, int deposit, int withdrawal, int all) async {

  var queryParameters = {
    'user': '$user',
    'wingo': '$wingo',
    'all': '$all',
    'support': '$support',
    'level': '$level',
    'transfer': '$transfer',
    'deposit': '$deposit',
    'withdrawal': '$withdrawal',
  };

  SharedPreferences preferences = await SharedPreferences.getInstance();
  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };
  Response response = await http.post(Uri.parse('$urlProd/user/notifications'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  var jsonData = json.decode(response.body);

  return jsonData;
}

getTerms() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };
  Response response = await http.get(
    Uri.parse('$urlProd/general/terms'),
    // Send authorization headers to the backend.
    headers: headers,
  );

  var jsonData = json.decode(response.body);

  return jsonData;
}

getPrivacy() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };
  Response response = await http.get(
    Uri.parse('$urlProd/general/privacy'),
    // Send authorization headers to the backend.
    headers: headers,
  );

  var jsonData = json.decode(response.body);

  return jsonData;
}

deleteUser(String user_id) async {
  var queryParameters = {
    'user_id': '$user_id',
  };

  Uri uri = Uri.parse("$urlProd/user/delete");
  final uiFinal = uri.replace(queryParameters: queryParameters);

  SharedPreferences preferences = await SharedPreferences.getInstance();
  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };

  Response response = await http.get(uiFinal, headers: headers);
  // Send authorization headers to the backend.

  var jsonData = json.decode(response.body);

  return jsonData;
}

updateUser(String first_name, String last_name, String email, String phone,
    DateTime birthday, int gender, String password) async {
  var queryParameters = {
    'first_name': '$first_name',
    'last_name': '$last_name',
    'email': '$email',
    'phone': '$phone',
    'birthday': '$birthday',
    'gender': '$gender',
    'password': '$password',
  };

  SharedPreferences preferences = await SharedPreferences.getInstance();
  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };

  Response response = await http.post(Uri.parse('$urlProd/user/edit'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  var jsonData = json.decode(response.body);

  return jsonData;
}

getQrCode(String code, String user_id) async {
  var queryParameters = {
    'code': '$code',
    'user_id': '$user_id',
  };

  SharedPreferences preferences = await SharedPreferences.getInstance();
  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };

  Response response = await http.post(Uri.parse('$urlProd/user/qr/scan'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}

completeLogin(String email, int phone, String password) async {
  var queryParameters = {
    'email': '$email',
    'phone': '$phone',
    'password': '$password',
  };

  Response response = await http.post(Uri.parse('$urlProd/user/complete'),
      // Send authorization headers to the backend.
      headers: {HttpHeaders.authorizationHeader: "x-www-form-urlencoded"},
      body: queryParameters);

  return jsonDecode(response.body);
}
