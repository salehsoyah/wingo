import 'dart:convert';
import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/urls/urls.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

// Future authFacebook() async {
//   FacebookLogin facebookSignIn = new FacebookLogin();
//
//   final FacebookLoginResult result = await facebookSignIn.logIn([' email ']);
//
//   switch (result.status) {
//     case FacebookLoginStatus.loggedIn:
//       final FacebookAccessToken accessToken = result.accessToken;
//
//       final graphResponse = await http.get(Uri.parse(
//           'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture&access_token=${accessToken.token}'));
//       final profile = json.decode(graphResponse.body);
//       print('''
//          Logged in!
//
//          Token: ${accessToken.token}
//          User id: ${accessToken.userId}
//          Expires: ${accessToken.expires}
//          Permissions: ${accessToken.permissions}
//          Declined permissions: ${accessToken.declinedPermissions}
//          ''');
//
//       return profile;
//       break;
//     case FacebookLoginStatus.cancelledByUser:
//       print(' Login cancelled by the user. ');
//       break;
//     case FacebookLoginStatus.error:
//       print(' Something went wrong with the login process.\n'
//           'Here\'s the error Facebook gave us : ${result.errorMessage}');
//       break;
//   }
// }
social_registration_login(
    String provider,
    String email,
    String first_name,
    String last_name,
    String uuid,
    String platform,
    int os_version,
    String fcm_token,
    String model) async {
  var queryParameters = {
    'email': '$email',
    'first_name': '$first_name',
    'last_name': '$last_name',
    'platform': '$platform',
    'os_version': '$os_version',
    'model': '$model',
    'fcm_token': '$fcm_token',
    'uuid': '$uuid',
    //  'avatar' : '$avatar',
  };

  Response response =
      await http.post(Uri.parse("$urlProd/user/social/$provider"),
          // Send authorization headers to the backend.
          headers: {HttpHeaders.authorizationHeader: "x-www-form-urlencoded"},
          body: queryParameters);

  return jsonDecode(response.body);
}

signIn(String phone, String password, String uuid, String platform,
    int os_version, String fcm_token, String model) async {
  var queryParameters = {
    'phone': '$phone',
    'password': '$password',
    'platform': '$platform',
    'os_version': '$os_version',
    'model': '$model',
    'fcm_token': '$fcm_token',
    'uuid': '$uuid',
  };

  Response response = await http.post(Uri.parse("$urlProd/user/login"),
      // Send  authorization headers to the backend.
      headers: {HttpHeaders.authorizationHeader: "x-www-form-urlencoded"},
      body: queryParameters);
  return jsonDecode(response.body);
}

registration(
    String first_name,
    String email,
    String phone,
    String password,
    String uuid,
    String platform,
    int os_version,
    String fcm_token,
    String model) async {
  var queryParameters = {
    'first_name': '$first_name',
    'phone': '$phone',
    'email': '$email',
    'password': '$password',
    'platform': '$platform',
    'os_version': '$os_version',
    'model': '$model',
    'fcm_token': '$fcm_token',
    'uuid': '$uuid',
  };
  Response response = await http.post(Uri.parse("$urlProd/user/register"),
      // Send authorization headers to the backend.
      headers: {HttpHeaders.authorizationHeader: "x-www-form-urlencoded"},
      body: queryParameters);
  print(queryParameters.toString());
  print(response.body);

  return jsonDecode(response.body);
}

resetPassword(String email, String old_password, String new_password,
    String new_confirm) async {
  var queryParameters = {
    'old': '$old_password',
    'new': '$new_password',
    'new_confirm': '$new_confirm',
  };

  Response response =
      await http.put(Uri.parse('$urlProd/user/password/reset/$email'),
          // Send authorization headers to the backend.
          headers: {HttpHeaders.authorizationHeader: "x-www-form-urlencoded"},
          body: queryParameters);

  return jsonDecode(response.body);
}

forgotPasswordMail(String email) async {
  var queryParameters = {
    'email': '$email',
  };
  Response response =
      await http.post(Uri.parse('$urlProd/user/password/forgot/mail'),
          // Send authorization headers to the backend.
          headers: {HttpHeaders.authorizationHeader: "x-www-form-urlencoded"},
          body: queryParameters);

  return jsonDecode(response.body);
}

uploadLogo(String photo) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");

  var queryParameters = {
    'photo': '$photo',
  };

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.post(Uri.parse('$urlProd/user/uploadImage'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}

editUser(
    String first_name,
    String last_name,
    String email,
    String phone,
    DateTime birthday,
    int gender,
    String password,
    ) async {

  var queryParameters = {
    'first_name': '$first_name',
    'last_name': '$last_name',
    'email': '$email',
    'phone': '$phone',
    'birthday': '$birthday',
    'gender': '$gender',
    'password': '$password'
  };

  Response response = await http.post(Uri.parse('$urlProd/user/edit'),
      // Send authorization headers to the backend.
      headers: {HttpHeaders.authorizationHeader: "x-www-form-urlencoded"},
      body: queryParameters);

  var jsonData = json.decode(response.body);

  return jsonData;
}

getUserDetail(String email) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  print("hi");
  Response response = await http.get(
    Uri.parse("$urlProd/user/get/details/$email"),
    // Send authorization headers to the backend.
    headers: headers,
  );
  print("hi11");

  return jsonDecode(response.body);
}

forgetPassword(String token, String new_password, String new_confirm) async {
  var queryParameters = {
    'new': '$new_password',
    'new_confirm': '$new_confirm',
  };

  Response response =
      await http.post(Uri.parse('$urlProd/user/password/forgot/$token'),
          // Send authorization headers to the backend.
          headers: {HttpHeaders.authorizationHeader: "x-www-form-urlencoded"},
          body: queryParameters
      );

  return jsonDecode(response.body);
}

verifyingIdentityPhone(String token) async {

  var queryParameters = {
    'token': '$token',
  };

  SharedPreferences preferences = await SharedPreferences.getInstance();
  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };

  Response response = await http.post(Uri.parse('$urlProd/user/phone/verify'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}

resendVerificationMail(String email) async {
  var queryParameters = {
    'email': '$email',
  };

  Response response = await http.post(Uri.parse('$urlProd/user/verify/resend'),
      // Send authorization headers to the backend.
      headers: {HttpHeaders.authorizationHeader: "x-www-form-urlencoded"},
      body: queryParameters);

  print(response.body);
  return jsonDecode(response.body);
}

Future authGoogle() async {
  Firebase.initializeApp();
  final googleSignIn = GoogleSignIn();
  bool isSigningIn = false;
  final user = await googleSignIn.signIn();
  if (user == null) {
    return isSigningIn;
  } else {
    final googleAuth = await user.authentication;
    final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken, idToken: googleAuth.idToken);
    await FirebaseAuth.instance.signInWithCredential(credential);
    final person = FirebaseAuth.instance.currentUser;

    return person;
  }
}

resendVerificationPhone(String email, String phone, String name) async {
  var queryParameters = {
    'email': '$email',
    'phone': '$phone',
    'first_name': '$name',
  };

  Response response = await http.post(Uri.parse('$urlProd/user/verify/phone/resend'),
      // Send authorization headers to the backend.
      headers: {HttpHeaders.authorizationHeader: "x-www-form-urlencoded"},
      body: queryParameters);

  print(response.body);
  return jsonDecode(response.body);
}
