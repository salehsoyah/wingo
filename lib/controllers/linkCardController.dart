import 'dart:convert';
import 'dart:io';
import 'package:flutter_app/models/card.dart';
import 'package:flutter_app/urls/urls.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

storeCard(String user_id, String cardholder_name, String card_number,
    String bank_name, String month, String year) async {
  var queryParameters = {
    'user_id': '$user_id',
    'cardholder_name': '$cardholder_name',
    'card_number': '$card_number',
    'bank_name': '$bank_name',
    'month': '$month',
    'year': '$year',
  };

  SharedPreferences preferences = await SharedPreferences.getInstance();
  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };

  Response response = await http.post(Uri.parse('$urlProd/card/add'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}

Future<List<BankCard>> indexCards() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  String userId = preferences.getString('idUser');
  String id = userId;
  print(id);

  var queryParameters = {
    'user_id': '$id',
  };

  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };
  Response response = await http.post(Uri.parse('$urlProd/card/list'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  var jsonData = json.decode(response.body);
  List<BankCard> cards = [];
  for (var u in jsonData["data"][0]) {
    BankCard card = BankCard(u['id'], u['user_id'], u['cardholder_name'],
        u['card_number'], u['bank_name'], u['month'], u['year']);
    cards.add(card);
  }
  return cards;
}

deleteCard(int card_id) async {

  SharedPreferences preferences = await SharedPreferences.getInstance();
  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };

  Response response = await http.get(Uri.parse('$urlProd/card/delete/$card_id'),
      // Send authorization headers to the backend.
      headers: headers,
  );

  return jsonDecode(response.body);
}
