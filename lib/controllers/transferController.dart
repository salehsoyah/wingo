import 'dart:convert';
import 'dart:io';
import 'package:flutter_app/urls/urls.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

storeTransfer(String from_id, String to_phone, double amount) async {
  var queryParameters = {
    'from_id': '$from_id',
    'to_phone': '$to_phone',
    'amount': '$amount',
  };

  SharedPreferences preferences = await SharedPreferences.getInstance();
  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };

  Response response = await http.post(Uri.parse('$urlProd/transactions/add/transfer'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}

storeDeposit(String user_id, double amount, String card_id) async {
  var queryParameters = {
    'user_id': '$user_id',
    'card_id': '$card_id',
    'amount': '$amount',
  };

  SharedPreferences preferences = await SharedPreferences.getInstance();
  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };

  Response response = await http.post(Uri.parse('$urlProd/transactions/add/deposit'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}

storeWithdrawal(String user_id, double amount, String rib, String bank) async {
  var queryParameters = {
    'user_id': '$user_id',
    'amount': '$amount',
    'rib': '$rib',
    'bank': '$bank',
  };

  SharedPreferences preferences = await SharedPreferences.getInstance();
  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };

  Response response = await http.post(Uri.parse('$urlProd/transactions/request/withdrawal'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}

storeTransferShop(String from_id, String shop_id, double amount) async {
  var queryParameters = {
    'from_id': '$from_id',
    'shop_id': '$shop_id',
    'amount': '$amount',
  };

  SharedPreferences preferences = await SharedPreferences.getInstance();
  String idSession = preferences.getString('idSession');
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$idSession"
  };

  Response response = await http.post(Uri.parse('$urlProd/transactions/add/payment'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}