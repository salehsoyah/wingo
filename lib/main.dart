import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/views/billPay/billPay.dart';
import 'package:flutter_app/views/billPay/billPayTransfert.dart';
import 'package:flutter_app/views/billPay/BillPayFiche.dart';
import 'package:flutter_app/views/dashboard/customBottomNavigationBar.dart';
import 'package:flutter_app/views/dashboard/dashboard.dart';
import 'package:flutter_app/views/dashboard/homeTopTabs.dart';
import 'package:flutter_app/views/dialog/checkcredentials.dart';
import 'package:flutter_app/views/dialog/dialog.dart';
import 'package:flutter_app/views/dialog/terms.dart';
import 'package:flutter_app/views/dialog/transfertSuccessDialog.dart';
import 'package:flutter_app/views/dialog/updateLogin.dart';
import 'package:flutter_app/views/history/history.dart';
import 'package:flutter_app/views/linkCard/linkCard.dart';
import 'package:flutter_app/views/linkCard/linkedCard.dart';
import 'package:flutter_app/views/linkCard/saveLinkedCard.dart';
import 'package:flutter_app/views/onboardingscreen/OnboardingScreen.dart';
import 'package:flutter_app/views/onboardingscreen/pages.dart';
import 'package:flutter_app/views/shops/createShop.dart';
import 'package:flutter_app/views/shops/editShop.dart';
import 'package:flutter_app/views/shops/scanShop.dart';
import 'package:flutter_app/views/signinScreens/createaccount.dart';
import 'package:flutter_app/views/signinScreens/fingerprint.dart';
import 'package:flutter_app/views/signinScreens/forgotPassword.dart';
import 'package:flutter_app/views/signinScreens/login.dart';
import 'package:flutter_app/views/signinScreens/otp_received.dart';
import 'package:flutter_app/views/signinScreens/passwordrecovery.dart';
import 'package:flutter_app/views/signinScreens/resetpassword.dart';
import 'package:flutter_app/views/signinScreens/verifyForgotPassword.dart';
import 'package:flutter_app/views/ticket/selectTicket.dart';
import 'package:flutter_app/views/ticket/test.dart';
import 'package:flutter_app/views/topUpScreens/topUpDeposit.dart';
import 'package:flutter_app/views/topUpScreens/topUpDepositSource.dart';
import 'package:flutter_app/views/transfert/transferShop.dart';
import 'package:flutter_app/views/transfert/transferValidation.dart';
import 'package:flutter_app/views/transfert/transfert.dart';
import 'package:flutter_app/views/transfert/transfertSource.dart';
import 'package:flutter_app/views/user/editProfile.dart';
import 'package:flutter_app/views/user/notifications.dart';
import 'package:flutter_app/views/user/profile.dart';
import 'package:flutter_app/views/user/profileOptions.dart';
import 'package:flutter_app/views/user/verification.dart';
import 'package:flutter_app/views/user/verifieLevel.dart';
import 'package:flutter_app/views/wallet/myWallet.dart';
import 'package:flutter_app/views/welcomeScreen/welcome.dart';
import 'package:flutter_app/views/withdraw/withdrawAmount.dart';
import 'package:flutter_app/views/withdraw/withdrawCard.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:shared_preferences/shared_preferences.dart';

const AndroidNotificationChannel channel = AndroidNotificationChannel(
    'high_importance_channel',
    'High Importance Notifications',
    'This channel is used for important notifications',
    importance: Importance.high,
    playSound: true);

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
FlutterLocalNotificationsPlugin();

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print('A bg message just showed up : ${message.messageId}');
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  await flutterLocalNotificationsPlugin
  .resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()
  ?.createNotificationChannel(channel);

  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );

  SharedPreferences preferences = await SharedPreferences.getInstance();
  var idSession = preferences.getBool('connected');

  runApp(MaterialApp(

    debugShowCheckedModeBanner: false,
    home: idSession == null ? welcome() : Dashboard()
  ));

}
